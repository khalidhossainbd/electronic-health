@extends('layouts.main')

@section('title')
    Electronic Health Appointment System
@endsection

@section('content')

    
    <section>
        <div class="container-fluid mt-2">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="main-img-container-one">
                                    <div class="text-container">
                                        <a href="#" class="text-decoration-none">
                                            <h3 class="d-block px-3 mt-2 text-white">Covid-19</h3>
                                            <h4 class="d-block px-3 text-white">Prevent, Protect and Prevail</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="main-img-container-two">
                                    <div class="text-container">
                                        <a href="#" class="text-decoration-none">
                                            <h3 class="d-block p-3 text-white">Our Activities</h3>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 p-0">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="{{ asset('front/dist/images/slider/slide_1.jpg') }}" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('front/dist/images/slider/slide_2.jpg') }}" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('front/dist/images/slider/slide_3.jpg') }}" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="top-main-tab">
                        <ul class="nav nav-pills nav-justified" id="myTab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#oneHome" role="tab" aria-controls="home" aria-selected="true">
                                <i class="fab fa-accessible-icon"></i>
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#twoHome" role="tab" aria-controls="profile" aria-selected="false">
                                <i class="fas fa-baby"></i>
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="contact-tab" data-toggle="tab" href="#threeHome" role="tab" aria-controls="contact" aria-selected="false">
                                <i class="fas fa-balance-scale-left"></i>
                              </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#fourHome" role="tab" aria-controls="contact" aria-selected="false">
                                    <i class="fas fa-bed"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#fiveHome" role="tab" aria-controls="contact" aria-selected="false">
                                    <i class="fas fa-blind"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#sixHome" role="tab" aria-controls="contact" aria-selected="false">
                                    <i class="fas fa-chalkboard-teacher"></i>
                                </a>
                            </li>
                          </ul>
                          <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="oneHome" role="tabpanel" aria-labelledby="home-tab">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis ab eligendi repellat, iste quisquam quia.</p>
                            </div>
                            <div class="tab-pane fade" id="twoHome" role="tabpanel" aria-labelledby="profile-tab">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis ab eligendi repellat, iste quisquam quia.</p>
                            </div>
                            <div class="tab-pane fade" id="threeHome" role="tabpanel" aria-labelledby="contact-tab">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis ab eligendi repellat, iste quisquam quia.</p>
                            </div>
                            <div class="tab-pane fade" id="fourHome" role="tabpanel" aria-labelledby="contact-tab">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis ab eligendi repellat, iste quisquam quia.</p>
                            </div>
                            <div class="tab-pane fade" id="fiveHome" role="tabpanel" aria-labelledby="contact-tab">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis ab eligendi repellat, iste quisquam quia.</p>
                            </div>
                            <div class="tab-pane fade" id="sixHome" role="tabpanel" aria-labelledby="contact-tab">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis ab eligendi repellat, iste quisquam quia.</p>
                            </div>
                          </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="top-main-content">
                        <h1>Welcome to <span>EHAS</span> </h1>
                        <p>
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id aperiam pariatur impedit debitis voluptate. Voluptate voluptas sequi hic. Perspiciatis fuga animi odio aliquid voluptates ut eligendi quas ipsam commodi est?
                            <br> <br>
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id aperiam pariatur impedit debitis voluptate. Voluptate voluptas sequi hic. Perspiciatis fuga animi odio aliquid voluptates ut eligendi quas ipsam commodi est?
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ===== Doctor Slider start ======= -->

    <section class="my-5">
        <div class="conntainer">
            <div class="row">
                <div class="col-12">
                    
                </div>
            </div>
        </div>
        <div id="carousel-4" class="carousel slide" data-ride="carousel">

            <!-- <ol class="carousel-indicators">
                <li data-target="#carousel-4" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-4" data-slide-to="1"></li>
                <li data-target="#carousel-4" data-slide-to="2"></li>
                <li data-target="#carousel-4" data-slide-to="3"></li>
            </ol> -->

            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/s62.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/c172.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/c94.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                        </div>

                    </div>
                    <!--e container -->

                </div><!-- e carousel-item -->

                <div class="carousel-item">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/c31.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/c41.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/s62.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                        </div>

                    </div>
                    <!--e container -->

                </div><!-- e carousel-item -->

                <div class="carousel-item">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/c94.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/c172.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/s72.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                        </div>

                    </div>
                    <!--e container -->

                </div><!-- e carousel-item -->

                <div class="carousel-item">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/c172.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/s62.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="doctor-slider-container">
                                    <img class="img-fluid" src="{{ asset('front/dist/images/img/s83.jpg') }}">
                                </div>
                                <div class="doctor-slider-text">
                                    <h3 class="h4 text-center mt-5">MD Khalid Hossain</h3>
                                    <p class="text-center">Child Spelishest</p>
                                    <p class="text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Aperiam, beatae distinctio? Exercitationem assumenda velit quo!</p>
                                </div>
                                <a class="btn btn-success slider-book-btn" href="">Book Now</a>
                            </div>
                        </div>

                    </div>
                    <!--e container -->

                </div><!-- e carousel-item -->

            </div><!-- e carousel-inner -->

            <a class="carousel-control-prev" href="#carousel-4" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon bg-dark" aria-hidden="true"></span>
                <span class="sr-only">prev</span>
            </a>

            <a class="carousel-control-next" href="#carousel-4" role="button" data-slide="next">
                <span class="carousel-control-next-icon bg-dark" aria-hidden="true"></span>
                <span class="sr-only">next</span>
            </a>

        </div><!-- e carousel -->
    </section>
    <!-- ===== Doctor Slider end ======= -->

    <div class="bg-gradient py-5">
        <div class="container">
            <div class="row">
                <div class="four col-md-3">
                    <div class="counter-box colored"><i class="fas fa-people-arrows"></i> <span class="counter">2147</span>
                        <p>Happy Customers</p>
                    </div>
                </div>
                <div class="four col-md-3">
                    <div class="counter-box"><i class="fas fa-user-md"></i><span class="counter">63</span>
                        <p>Registered Doctors</p>
                    </div>
                </div>
                <div class="four col-md-3">
                    <div class="counter-box"> <i class="fas fa-atom"></i> <span class="counter">9</span>
                        <p>Available Services</p>
                    </div>
                </div>
                <div class="four col-md-3">
                    <div class="counter-box"><i class="fas fa-user-shield"></i><span class="counter">563</span>
                        <p>Registered counselor</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 mx-auto">
                    <div class="p-0 p-sm-5 bg-white">
                        <!-- Bootstrap carousel-->
                        <div class="carousel slide" id="carouselExampleIndicators" data-ride="carousel">
                            <!-- Bootstrap carousel indicators [nav] -->
                            <ol class="carousel-indicators mb-0">
                                <li class="active" data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                            </ol>


                            <!-- Bootstrap inner [slides]-->
                            <div class="carousel-inner px-5 pb-4">
                                <!-- Carousel slide-->
                                <div class="carousel-item active">
                                    <div class="media">
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <img class="rounded-circle img-fluid d-block mx-auto my-2" src="{{ asset('front/dist/images/doctor.png') }}" alt="" >
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="media-body mr-2 mr-sm-5" style="min-height:380px;">
                                                    <blockquote class="blockquote border-0 p-0">
                                                        <h4 class="media-title">Lorem ipsum dolor sit amet.</h4>
                                                        <p class="font-italic lead text-justify"> <i class="fa fa-quote-left mr-3 text-success"></i>
                                                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptate repudiandae ut minima vel id, neque itaque quaerat in, nulla natus voluptatibus pariatur iure ipsa omnis deleniti, deserunt ratione doloremque modi!
                                                        </p>
                                                        <test-footer class="">Md Khalid Hossain <br>
                                                            <small title="Source Title"> Head of IT </small>
                                                        </test-footer>
                                                    </blockquote>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Carousel slide-->
                                <div class="carousel-item">
                                    <div class="media">
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <img class="rounded-circle img-fluid d-block mx-auto my-2" src="{{ asset('front/dist/images/doctor-2.png') }}" alt="" >
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="media-body mr-2 mr-sm-5" style="min-height:380px;">
                                                    <blockquote class="blockquote border-0 p-0">
                                                        <h4 class="media-title">Lorem ipsum dolor sit amet.</h4>
                                                        <p class="font-italic lead text-justify"> <i class="fa fa-quote-left mr-3 text-success"></i>
                                                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptate repudiandae ut minima vel id, neque itaque quaerat in, nulla natus voluptatibus pariatur iure ipsa omnis deleniti, deserunt ratione doloremque modi!
                                                        </p>
                                                        <test-footer class="">Md Mamunur Rahman <br>
                                                            <small title="Source Title"> Head of IT </small>
                                                        </test-footer>
                                                    </blockquote>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Carousel slide-->
                                <div class="carousel-item">
                                    <div class="media">
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <img class="rounded-circle img-fluid d-block mx-auto my-2" src="{{ asset('front/dist/images/doctor.png') }}" alt="" >
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="media-body mr-2 mr-sm-5" style="min-height:380px;">
                                                    <blockquote class="blockquote border-0 p-0">
                                                        <h4 class="media-title">Lorem ipsum dolor sit amet.</h4>
                                                        <p class="font-italic lead text-justify"> <i class="fa fa-quote-left mr-3 text-success"></i>
                                                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptate repudiandae ut minima vel id, neque itaque quaerat in, nulla natus voluptatibus pariatur iure ipsa omnis deleniti, deserunt ratione doloremque modi!
                                                        </p>
                                                        <test-footer class="">Tofail Ahmed<br>
                                                            <small title="Source Title"> Head of IT </small>
                                                        </test-footer>
                                                    </blockquote>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Carousel slide-->
                                <div class="carousel-item">
                                    <div class="media">
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <img class="rounded-circle img-fluid d-block mx-auto my-2" src="{{ asset('front/dist/images/doctor-2.png') }}" alt="" >
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="media-body mr-2 mr-sm-5" style="min-height:380px;">
                                                    <blockquote class="blockquote border-0 p-0">
                                                        <h4 class="media-title">Lorem ipsum dolor sit amet.</h4>
                                                        <p class="font-italic lead text-justify"> <i class="fa fa-quote-left mr-3 text-success"></i>
                                                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptate repudiandae ut minima vel id, neque itaque quaerat in, nulla natus voluptatibus pariatur iure ipsa omnis deleniti, deserunt ratione doloremque modi!
                                                        </p>
                                                        <test-footer class="">Md Khalid Hossain <br>
                                                            <small title="Source Title"> Head of IT </small>
                                                        </test-footer>
                                                    </blockquote>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- Bootstrap controls [dots]-->
                            <a class="carousel-control-prev width-auto" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <i class="fa fa-angle-left text-dark text-lg"></i>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next width-auto" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <i class="fa fa-angle-right text-dark text-lg"></i>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection