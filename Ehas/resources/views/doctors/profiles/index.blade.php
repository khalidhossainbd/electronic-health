@extends('layouts.doctor-app')

@section('title')
    Doctor Dashboard
@endsection

@section('content')

    <!-- BREADCRUMB-->
    <section class="au-breadcrumb m-t-75">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="au-breadcrumb-content">
                            <div class="au-breadcrumb-left">
                                <h4 class="name">Doctor's Profile</h4>
                            </div>
                            @if(Auth::user()->docprofile)
                            <a href="{{ route('doctor.profile.edit', Auth::user()->docprofile->id) }}" class="au-btn au-btn-icon au-btn--blue">
                                Edit Information
                            </a>
                            @else
                            <a href="{{ route('doctor.profile.create') }}" class="au-btn au-btn-icon au-btn--green">
                                Add Information
                            </a>
                            @endif
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->


    <section class="statistic">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card p-5">
                            @if(Auth::user()->docprofile)
                            <table class="table table-bordered">
                                <tr>
                                    <th>Home Phone</th>
                                    <td>{{ Auth::user()->docprofile->home_phone }}</td>
                                </tr>
                                <tr>
                                    <th>Gender</th>

                                    <td>
                                        {{ Auth::user()->docprofile->gender == 1 ? 'Male' : '' }}
                                        {{ Auth::user()->docprofile->gender == 2 ? 'Female' : '' }}
                                        {{ Auth::user()->docprofile->gender == 3 ? 'Others' : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>NI Number</th>
                                    <td>{{ Auth::user()->docprofile->nid }}</td>
                                </tr>
                                <tr>
                                    <th>Social Media Link</th>
                                    <td>{{ Auth::user()->docprofile->social_media }}</td>
                                </tr>
                                <tr>
                                    <th>Street Address</th>
                                    <td>{{ Auth::user()->docprofile->address }}</td>
                                </tr>
                                <tr>
                                    <th>Area</th>
                                    <td>{{ Auth::user()->docprofile->area }}</td>
                                </tr>
                                <tr>
                                    <th>State</th>
                                    <td>{{ Auth::user()->docprofile->state }}</td>
                                </tr>
                                <tr>
                                    <th>Short Bio</th>
                                    <td>{!! Auth::user()->docprofile->shot_bio !!}</td>
                                </tr>
                            </table>
                            @else
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
