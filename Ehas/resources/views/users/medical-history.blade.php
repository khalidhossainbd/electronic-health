@extends('layouts.user-app')

@section('title')
    Patient Profile
@endsection

@section('content')

	<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="overview-wrap">
                    <h2 class="title-1">{{ Auth::user()->f_name }} {{ Auth::user()->l_name }}</h2>
                </div>
            </div>
        </div>
        <div class="row m-t-25">
            <div class="col-12">

    			<div class="card">
    			
    				<div class="card-header">
    					<div class="row">
    						<div class="col-6">
    							<strong>Medical History</strong> 
    						</div>
    						<div class="col-6 text-right">
    							@if(Session::has('message')) <span class="text-success p-1">{{ Session::get('message') }}</span> @endif
    						</div>
    					</div>
    				</div>
    				<div class="card-body card-block">
    						{!! $page !!}
    				</div>
    			</div>
			</div>
		</div>
	</div>		




@endsection

@section('java_script')

<script>

$(function(){
	 $(document).on('change', '#featureImage', function(e){
	   e.preventDefault();
	   var reader = new FileReader();
	   var _this = this;
	   reader.onload = function (e) {
	     $('.custom-file-label').text(_this.files[0].name);
	     $('#viewFeatureImage').attr('src', e.target.result);
	  }
	  reader.readAsDataURL(this.files[0]);
	});
			
});
$('textarea').each(function () {
    //const editor = Jodit.make(this);
});
</script>

@endsection