@extends('layouts.doctor-app')

@section('title')
    Doctor Dashboard
@endsection

@section('content')

    <!-- BREADCRUMB-->
    <section class="au-breadcrumb m-t-75">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="au-breadcrumb-content">
                            <div class="au-breadcrumb-left">
                                <h4 class="name">Support Assistant Add </h4>
                            </div>
                            <a href="{{ route('doctor.assistant') }}" class="au-btn au-btn-icon au-btn--green">
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->


    <section class="statistic">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card p-5">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
