<?php

namespace App\Http\Controllers\Supports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SupportHomeController extends Controller
{
    public function index()
    {
        return view('supports.dashboard');
    }
}
