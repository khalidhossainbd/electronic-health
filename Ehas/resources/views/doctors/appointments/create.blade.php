@extends('layouts.doctor-app')

@section('title')
    Doctor Dashboard
@endsection

@section('content')

    <!-- BREADCRUMB-->
    <section class="au-breadcrumb m-t-75">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="au-breadcrumb-content">
                            <div class="au-breadcrumb-left">
                                <h4 class="name">Doctor's Appointment Add </h4>
                            </div>
                            <a href="{{ route('doctor.appointment') }}" class="au-btn au-btn-icon au-btn--green">
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->


    <section class="statistic">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card p-5">
                            <form action="{{ route('doctor.appointment.store') }}" method="post" >
                        @csrf
                        <div class="form-group">
                            <label for="app_date" class="control-label mb-1">Appiontment date</label>
                            <input id="app_date" name="app_date" type="date" class="form-control" >
                        </div>
                        <div class="form-group has-success">
                            <label for="app_num" class="control-label mb-1">Number of Appointment</label>
                            <input id="app_num" name="app_num" type="text" class="form-control cc-name valid">
                            
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="str_time" class="control-label mb-1">Start Time</label>
                                    <input id="str_time" name="str_time" type="time" class="form-control">
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="duration" class="control-label mb-1">Time Duration</label>
                                <select class="form-control" id="duration" name="duration">
                                    <option value="">Select an option</option>
                                    <option value="10">10 Minutes</option>
                                    <option value="15">15 Minutes</option>
                                    <option value="20">20 Minutes</option>
                                    <option value="25">25 Minutes</option>
                                    <option value="30">30 Minutes</option>
                                    <option value="35">35 Minutes</option>
                                    <option value="40">40 Minutes</option>
                                </select>
                            </div>
                        </div>
                        <div>
                            <button id="payment-button" type="submit" class="btn btn-info btn-block">
                                <i class="fa fa-user-md fa-lg"></i>&nbsp;
                                <span id="payment-button-amount">Create</span>
                                <span id="payment-button-sending" style="display:none;">Sending…</span>
                            </button>
                        </div>
                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
