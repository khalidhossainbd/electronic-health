							@php
								$routeParam = ['page'=>'create' ];
								if(null !== ($request->has('page')) && ($request->filled('page'))){
									$routeParam['page'] = $request->page;
								}
								if(null !== ($request->has('id')) && ($request->filled('id'))){
									$routeParam['id'] = $request->id;
								}
							@endphp
							<form action="{{ route( 'user.health_prediction', $routeParam) }}" method="post" enctype="multipart/form-data">
							@csrf
    							<div class="row">
    								<div class="col-md-12">
        								<div class="table-data__tool">
                                            <div class="table-data__tool-left">
                                            	<strong>Health Prdiction Create</strong>  
                                            </div>
                                            <div class="table-data__tool-right">
                                                <a class="au-btn au-btn-icon au-btn--green au-btn--small" href="{{ route('user.health_prediction') }}"><i class="zmdi zmdi-view-list-alt"></i>see list</a>
                                            </div>
                                    	</div>
                                	</div>
    							</div>
    							@if($errors->any())
                                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                                @endif
    							<div class="row">
    								<div class="col-6">
        								<div class="form-group">
                							<label for="age" class=" form-control-label">Age</label> 
                							<input type="text" id="age" name="age" placeholder="Age" class="form-control" value="{{ val('age', $lifecycle) }}"> 
                						</div>
        							</div>
        							<div class="col-6">
        								<div class="form-group">
        									<label for="nf-email" class=" form-control-label">Gender</label>
                							<div class="form-check">
                                                <label for="inline-radio1" class="form-check-label mx-3 position-relative">
                	                                <input type="radio" id="inline-radio1"  name="gender" value="1" class="form-check-input" @if( val('gender', $lifecycle)) == 1 ) checked @endif>Male
            	                                </label>
                    	                        <label for="inline-radio2" class="form-check-label mx-3 position-relative">
                        		                    <input type="radio" id="inline-radio2" name="gender" value="2" class="form-check-input" @if( val('gender', $lifecycle)) == 2 ) checked @endif>Female
                                	            </label>
                                            </div>
                                        </div>
                                    </div>
    							</div>
    							
    							<div class="row">
    								<div class="col-4">
        								<div class="form-group">
                							<label for="cp" class=" form-control-label">CP</label> 
                							<input type="text" id="cp" name="cp" placeholder="CP" class="form-control" value="{{ val('cp', $lifecycle) }}"> 
                						</div>
        							</div>
        							<div class="col-4">
        								<div class="form-group">
                							<label for="pressurehight" class="form-control-label">Pressurehight</label> 
                							<input type="text" id="pressurehight" name="pressurehight" placeholder="Title" class="form-control" value="{{ val('pressurehight', $lifecycle) }}"> 
                						</div>
        							</div>
        							<div class="col-4">
        								<div class="form-group">
                							<label for="pressurelow" class="form-control-label">Pressurelow</label> 
                							<input type="text" id="pressurelow" name="pressurelow" placeholder="Pressurelow" class="form-control" value="{{ val('pressurelow', $lifecycle) }}"> 
                						</div>
        							</div>
        						</div>
    							<div class="row">
    								<div class="col-4">
        								<div class="form-group">
                							<label for="glucose" class=" form-control-label">Glucose</label> 
                							<input type="text" id="glucose" name="glucose" placeholder="Glucose" class="form-control" value="{{ val('glucose', $lifecycle) }}"> 
                						</div>
        							</div>
        							<div class="col-4">
        								<div class="form-group">
                							<label for="thalachh" class="form-control-label">thalachh</label> 
                							<input type="text" id="thalachh" name="thalachh" placeholder="thalachh" class="form-control" value="{{ val('thalachh', $lifecycle) }}"> 
                						</div>
        							</div>
        							<div class="col-4">
        								<div class="form-group">
                							<label for="troponin" class="form-control-label">Troponin</label> 
                							<input type="text" id="troponin" name="troponin" placeholder="Troponin" class="form-control" value="{{ val('troponin', $lifecycle) }}"> 
                						</div>
        							</div>
        						</div>

										<div class="row">
											<div class="col-4">
													<div class="form-group">
																<label for="oldpeak" class=" form-control-label">oldpeak</label> 
																<input type="text" id="oldpeak" name="oldpeak" placeholder="oldpeak" class="form-control" value="{{ val('oldpeak', $lifecycle) }}"> 
															</div>
												</div>
												<div class="col-4">
													<div class="form-group">
																<label for="thall" class="form-control-label">thall</label> 
																<input type="text" id="thall" name="thall" placeholder="thall" class="form-control" value="{{ val('thall', $lifecycle) }}"> 
															</div>
												</div>
												<div class="col-4">
													<div class="form-group">
																<label for="caa" class="form-control-label">caa</label> 
																<input type="text" id="caa" name="caa" placeholder="caa" class="form-control" value="{{ val('caa', $lifecycle) }}"> 
															</div>
												</div>
											</div>
        						
        						<div class="row">
    								<div class="col-6">
        								<div class="form-group">
                							<label for="weight" class=" form-control-label">Weight</label> 
                							<input type="text" id="weight" name="weight" placeholder="Weight" class="form-control" value="{{ val('weight', $lifecycle) }}"> 
                						</div>
        							</div>
        							<div class="col-6">
        								<div class="form-group">
        									<label for="date" class=" form-control-label">Date</label>
                							<input type="date" id="dob" name="date" placeholder="Date" class="form-control" value="{{ val('date', $lifecycle) }}"> 
                                        </div>
                                    </div>
    							</div>
    							<div class="row">
            						<div class="col-4 mx-0 px-1">
        								<button type="reset" class="btn btn-danger btn-sm  w-100">
            								<i class="fa fa-ban"></i> Reset
            							</button>
            						</div>
            						<div class="col-8 mx-0 px-1">
            							<button type="submit" class="btn btn-primary btn-sm w-100">
                    						<i class="fa fa-dot-circle-o"></i> Submit
                    					</button>
            						</div>
        						</div>
    						</form>