    <!-- ===== Footer section start ======= -->
    <section>
        <footer>
            <div class="container pt-5">
                <div class="row">
                    <div class="col-12 col-sm-8 offset-0 offset-sm-2">
                        <div class="row">
                            <div class="col-12">
                                <p class="text-center text-white font_two h3 mb-4">
                                    Keep connected to our latest offer and events..
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="mt-4" style="border-bottom: 0.03rem solid gray ;"></div>
                <div class="row">
                    <div class="col-12 col-sm-3">
                        <div class="footer-text my-3 my-sm-5">
                            <h3>About Us</h3>
                            <div class="devider-footer"></div>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptates sit eaque animi
                                neque qui officiis.</p>
                            <img class="img-fluid footer-logo" src="{{ asset('front/dist/images/ehas_white.png') }}" alt="EHAS">
                            <ul class="social-icon">
                                <li><a href="">
                                        <i class="fab fa-facebook-f"></i>
                                    </a></li>
                                <li><a href="">
                                        <i class="fab fa-youtube"></i>
                                    </a></li>
                                <li><a href="">
                                        <i class="fab fa-instagram"></i>
                                    </a></li>
                                <li><a href="">
                                        <i class="fab fa-twitter"></i>
                                    </a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-12 col-sm-3">
                        <div class="footer-text my-3 my-sm-5">
                            <h3>Get In Touch</h3>
                            <div class="devider-footer"></div>
                            <ul class="text-list">
                                <li><a href="#"><i class="fas fa-map-marker-alt"></i> &nbsp; 15 Bartholomew Row, <br> &nbsp;&nbsp;&nbsp;&nbsp; Birmingham B5 5JU</a></li>
                                <li><a href="#"><i class="fas fa-phone-volume"></i> &nbsp; +880 1600 000 000</a></li>
                                <li><a href="#"><i class="far fa-envelope"></i> &nbsp;support@gmail.com</a></li>
                                <li><a href="#"><i class="far fa-clock"></i> &nbsp;Mon - Sat ( 9:00 AM To 8:00 PM )</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-sm-3">
                        <div class="footer-text my-3 my-sm-5">
                            <h3>Useful Link</h3>
                            <div class="devider-footer"></div>
                            <ul class="text-list">
                                <li><a href="{{ route('about') }}">About Us</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Doctors</a></li>
                                <li><a href="#">Offers</a></li>
                                <li><a href="#">Events</a></li>
                                <li><a href="{{ route('about') }}">Blogs</a></li>
                                <li><a href="{{ route('privacy_policy') }}">Privacy Policy</a></li>
                                <li><a href="{{ route('terms_of_use') }}">Terms of Use</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-sm-3">
                        <div class="footer-text my-3 my-sm-5">
                            <h3>Subscribe</h3>
                            <div class="devider-footer"></div>
                            <div class="row">
                                <div class="col-12 mt-4">
                                    <div class="mb-3">
                                        <input type="text" class="form-control footer" id="name"
                                            placeholder="Your Full Name">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="mb-3">
                                        <input type="email" class="form-control footer" id="email"
                                            placeholder="name@example.com">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="d-grid">
                                        <button class="btn btn-danger footer-btn" type="button">Subscribe</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </footer>
    </section>
    <footer class="copyright">

        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <p class="text-center text-sm-left m-0 py-3">© 2023 All rights reserved by <a
                            href="#">ehas.care</a></p>
                </div>
                <div class="col-12 col-sm-6">

                    <p class="text-center text-sm-right m-0 py-3">
                        <a class="text-white" href="">Terms</a> &emsp; | &emsp; <a class="text-white"
                            href="">Privacy</a> &emsp; | &emsp; <a class="text-white" href="">Contact</a>
                    </p>

                </div>
            </div>
        </div>
    </footer>
    <!-- ===== Footer section end ======= -->