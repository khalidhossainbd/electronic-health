<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientMedicine extends Model
{
    use HasFactory;
    
    protected $table = 'patient_medicines';
    
    protected $primaryKey = 'id';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prescription_id', 'patient_id', 'doctor_id', 'medicine_id', 'medicine_time', 'appointment_id',  'status'
    ];
    
    public function medicine()
    {
        return $this->belongsTo(Medicine::class, 'medicine_id');
    }
}
