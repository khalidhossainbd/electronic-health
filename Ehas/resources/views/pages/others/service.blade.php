@extends('layouts.main')

@section('title')
    Service
@endsection

@section('content')


@include('pages.partials.pagetitle', ['title' => "Our Services"])


<section class="">
    <div class="alert alert-info py-5 m-0 border-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-4">
                    <h3>Online Appointment System</h3>
                </div>
                <div class="col-12 col-sm-8">
                    <p class="text-justify">
                        The Online Appointment System within the Health Appointment System (EHAS) epitomizes modern healthcare accessibility. Offering a seamless digital experience, it empowers patients to schedule appointments conveniently, reducing wait times and enhancing overall healthcare efficiency. Through user-friendly interfaces and automated reminders, EHAS Online Appointment System not only simplifies the scheduling process but also promotes patient engagement. The digital framework ensures accessibility anytime, anywhere, transforming traditional healthcare interactions into a more flexible and patient-centric approach. In essence, EHAS Online Appointment System represents a pivotal step towards a digitally-driven, efficient healthcare landscape.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success py-5 m-0 border-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-4">
                    <h3>Digital Perception</h3>
                </div>
                <div class="col-12 col-sm-8">
                    <p class="text-justify">
                        The Digital Perception of the Health Appointment System (EHAS) marks a revolutionary shift in healthcare delivery. Transforming traditional appointment processes, EHAS leverages digital technologies to enhance accessibility, streamline scheduling, and empower patients with real-time health management. Its user-friendly interfaces, coupled with AI-driven features, redefine the patient experience, fostering a sense of engagement and proactive healthcare involvement. The digital perception of EHAS extends beyond mere convenience, embodying a paradigm where technology becomes a catalyst for personalized, efficient, and patient-centric healthcare services, ultimately contributing to a more connected and responsive healthcare ecosystem.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-info py-5 m-0 border-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-4">
                    <h3>Health Record</h3>
                </div>
                <div class="col-12 col-sm-8">
                    <p class="text-justify">                        
                        The Health Record component of the Health Appointment System (EHAS) is a comprehensive digital repository that revolutionizes healthcare documentation. It securely consolidates and organizes patient health information, including medical history, diagnoses, treatments, and prescriptions. EHAS Health Record ensures accessibility for healthcare providers, facilitating informed decision-making, and streamlining patient care. This digital approach not only enhances data accuracy but also empowers patients to actively participate in their health management. With EHAS Health Record, the convergence of technology and healthcare creates a dynamic platform for efficient information exchange, contributing to a more interconnected and patient-centric healthcare system.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success py-5 m-0 border-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-4">
                    <h3>Health Prediction</h3>
                </div>
                <div class="col-12 col-sm-8">
                    <p class="text-justify">
                        The Health Prediction feature embedded within the Health Appointment System (EHAS) leverages advanced algorithms to analyze patient data and forecast potential health outcomes. By incorporating predictive analytics, EHAS Health Prediction aids healthcare providers in identifying trends, predicting risks, and customizing preventive interventions. This innovative digital component enhances the efficiency of healthcare delivery, offering a proactive approach to patient care. Through the analysis of historical health data, EHAS Health Prediction contributes to early detection, personalized treatment plans, and overall improved health outcomes, aligning with the transformative shift towards predictive and preventive healthcare strategies.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>  

@endsection