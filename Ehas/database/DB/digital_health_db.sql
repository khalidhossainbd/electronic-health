-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 05, 2024 at 05:13 PM
-- Server version: 8.0.35-0ubuntu0.22.04.1
-- PHP Version: 8.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digital_health`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Admin',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint UNSIGNED DEFAULT NULL,
  `updated_by` bigint UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `mobile`, `type`, `status`, `email_verified_at`, `password`, `created_by`, `updated_by`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Khalid Hossain', 'kadmin@gmail.com', '01720279279', 'Super Admin', 'Active', NULL, '$2y$10$KGx/PPmuRD8bp3fkQnBVvOqx4Z/jNmwoViLVZRI8X/0xRoP.QFQg2', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `apdates`
--

CREATE TABLE `apdates` (
  `id` bigint UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `month` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `apdates`
--

INSERT INTO `apdates` (`id`, `date`, `month`, `year`, `content`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '2023-12-19', '12', '2023', NULL, '1', 2, '2023-12-17 18:07:48', '2023-12-17 18:07:48'),
(2, '2024-01-03', '01', '2024', NULL, '1', 2, '2024-01-01 05:10:51', '2024-01-01 05:10:51');

-- --------------------------------------------------------

--
-- Table structure for table `apermissions`
--

CREATE TABLE `apermissions` (
  `id` bigint UNSIGNED NOT NULL,
  `patient_id` bigint UNSIGNED DEFAULT NULL,
  `doctor_id` bigint UNSIGNED DEFAULT NULL,
  `status` int DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` bigint UNSIGNED NOT NULL,
  `appointmentID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `duration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `zoom_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apdate_id` bigint UNSIGNED DEFAULT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `appointmentID`, `date`, `start_time`, `end_time`, `duration`, `content`, `status`, `zoom_link`, `apdate_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '17028364684263', '2023-12-19', '03:00:00', '03:15:00', '15', NULL, '1', NULL, 1, 2, '2023-12-17 18:07:48', '2023-12-17 18:07:48'),
(2, '17028364689299', '2023-12-19', '03:15:00', '03:30:00', '15', NULL, '2', NULL, 1, 2, '2023-12-17 18:07:48', '2023-12-18 16:00:34'),
(3, '17028364682130', '2023-12-19', '03:30:00', '03:45:00', '15', NULL, '2', NULL, 1, 2, '2023-12-17 18:07:48', '2023-12-17 18:07:48'),
(4, '17028364682580', '2023-12-19', '03:45:00', '04:00:00', '15', NULL, '1', NULL, 1, 2, '2023-12-17 18:07:48', '2023-12-17 18:07:48'),
(5, '17028364682829', '2023-12-19', '04:00:00', '04:15:00', '15', NULL, '1', NULL, 1, 2, '2023-12-17 18:07:48', '2023-12-17 18:07:48'),
(6, '17028364683374', '2023-12-19', '04:15:00', '04:30:00', '15', NULL, '1', NULL, 1, 2, '2023-12-17 18:07:48', '2023-12-17 18:07:48'),
(7, '17028364684673', '2023-12-19', '04:30:00', '04:45:00', '15', NULL, '1', NULL, 1, 2, '2023-12-17 18:07:48', '2023-12-17 18:07:48'),
(8, '17028364683135', '2023-12-19', '04:45:00', '05:00:00', '15', NULL, '2', NULL, 1, 2, '2023-12-17 18:07:48', '2023-12-18 16:22:56'),
(9, '17028364686163', '2023-12-19', '05:00:00', '05:15:00', '15', NULL, '1', NULL, 1, 2, '2023-12-17 18:07:48', '2023-12-17 18:07:48'),
(10, '17028364687362', '2023-12-19', '05:15:00', '05:30:00', '15', NULL, '1', NULL, 1, 2, '2023-12-17 18:07:48', '2023-12-17 18:07:48'),
(11, '17040858513536', '2024-01-03', '03:10:00', '03:20:00', '10', NULL, '2', 'https://us04web.zoom.us/j/2679632453?pwd=UVQzZHlWVjlVWHBkWGRka1FKUVJBUT09', 2, 2, '2024-01-01 05:10:51', '2024-01-01 05:15:20'),
(12, '17040858511558', '2024-01-03', '03:20:00', '03:30:00', '10', NULL, '1', 'https://us04web.zoom.us/j/2679632453?pwd=UVQzZHlWVjlVWHBkWGRka1FKUVJBUT09', 2, 2, '2024-01-01 05:10:51', '2024-01-01 05:10:51'),
(13, '17040858512531', '2024-01-03', '03:30:00', '03:40:00', '10', NULL, '1', 'https://us04web.zoom.us/j/2679632453?pwd=UVQzZHlWVjlVWHBkWGRka1FKUVJBUT09', 2, 2, '2024-01-01 05:10:51', '2024-01-01 05:10:51'),
(14, '17040858517917', '2024-01-03', '03:40:00', '03:50:00', '10', NULL, '1', 'https://us04web.zoom.us/j/2679632453?pwd=UVQzZHlWVjlVWHBkWGRka1FKUVJBUT09', 2, 2, '2024-01-01 05:10:51', '2024-01-01 05:10:51'),
(15, '17040858517326', '2024-01-03', '03:50:00', '04:00:00', '10', NULL, '1', 'https://us04web.zoom.us/j/2679632453?pwd=UVQzZHlWVjlVWHBkWGRka1FKUVJBUT09', 2, 2, '2024-01-01 05:10:51', '2024-01-01 05:10:51');

-- --------------------------------------------------------

--
-- Table structure for table `chatbot_hints`
--

CREATE TABLE `chatbot_hints` (
  `id` int NOT NULL,
  `question` varchar(100) NOT NULL,
  `reply` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chatbot_hints`
--

INSERT INTO `chatbot_hints` (`id`, `question`, `reply`) VALUES
(1, 'HI||Hello||Hola', 'Hello, how are you.'),
(2, 'How are you', 'Good to see you again!'),
(3, 'what is your name|| Whats your name', 'My name is Ehas Bot'),
(4, 'what should I call you', 'You can call me Vishal Bot'),
(5, 'Where are your from', 'I m from Bangladesh'),
(6, 'Bye||See you later||Have a Good Day', 'Sad to see you are going. Have a nice day');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint UNSIGNED NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zone_id` int DEFAULT '0',
  `status` int DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`, `zone_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'AF', 'Afghanistan', 0, 1, '2021-04-06 00:06:30', '2021-10-10 23:34:13', NULL),
(2, 'AL', 'Albania', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(3, 'DZ', 'Algeria', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(4, 'AS', 'American Samoa', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(5, 'AD', 'Andorra', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(6, 'AO', 'Angola', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(7, 'AI', 'Anguilla', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(8, 'AQ', 'Antarctica', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(9, 'AG', 'Antigua And Barbuda', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(10, 'AR', 'Argentina', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(11, 'AM', 'Armenia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(12, 'AW', 'Aruba', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(13, 'AU', 'Australia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(14, 'AT', 'Austria', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(15, 'AZ', 'Azerbaijan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(16, 'BS', 'Bahamas The', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(17, 'BH', 'Bahrain', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(18, 'BD', 'Bangladesh', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(19, 'BB', 'Barbados', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(20, 'BY', 'Belarus', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(21, 'BE', 'Belgium', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(22, 'BZ', 'Belize', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(23, 'BJ', 'Benin', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(24, 'BM', 'Bermuda', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(25, 'BT', 'Bhutan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(26, 'BO', 'Bolivia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(27, 'BA', 'Bosnia and Herzegovina', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(28, 'BW', 'Botswana', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(29, 'BV', 'Bouvet Island', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(30, 'BR', 'Brazil', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(31, 'IO', 'British Indian Ocean Territory', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(32, 'BN', 'Brunei', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(33, 'BG', 'Bulgaria', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(34, 'BF', 'Burkina Faso', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(35, 'BI', 'Burundi', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(36, 'KH', 'Cambodia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(37, 'CM', 'Cameroon', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(38, 'CA', 'Canada', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(39, 'CV', 'Cape Verde', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(40, 'KY', 'Cayman Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(41, 'CF', 'Central African Republic', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(42, 'TD', 'Chad', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(43, 'CL', 'Chile', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(44, 'CN', 'China', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(45, 'CX', 'Christmas Island', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(46, 'CC', 'Cocos (Keeling) Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(47, 'CO', 'Colombia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(48, 'KM', 'Comoros', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(49, 'CG', 'Republic Of The Congo', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(50, 'CD', 'Democratic Republic Of The Congo', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(51, 'CK', 'Cook Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(52, 'CR', 'Costa Rica', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(54, 'HR', 'Croatia (Hrvatska)', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(55, 'CU', 'Cuba', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(56, 'CY', 'Cyprus', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(57, 'CZ', 'Czech Republic', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(58, 'DK', 'Denmark', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(59, 'DJ', 'Djibouti', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(60, 'DM', 'Dominica', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(61, 'DO', 'Dominican Republic', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(62, 'TP', 'East Timor', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(63, 'EC', 'Ecuador', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(64, 'EG', 'Egypt', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(65, 'SV', 'El Salvador', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(66, 'GQ', 'Equatorial Guinea', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(67, 'ER', 'Eritrea', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(68, 'EE', 'Estonia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(69, 'ET', 'Ethiopia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(70, 'XA', 'External Territories of Australia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(71, 'FK', 'Falkland Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(72, 'FO', 'Faroe Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(73, 'FJ', 'Fiji Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(74, 'FI', 'Finland', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(75, 'FR', 'France', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(76, 'GF', 'French Guiana', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(77, 'PF', 'French Polynesia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(78, 'TF', 'French Southern Territories', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(79, 'GA', 'Gabon', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(80, 'GM', 'Gambia The', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(81, 'GE', 'Georgia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(82, 'DE', 'Germany', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(83, 'GH', 'Ghana', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(84, 'GI', 'Gibraltar', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(85, 'GR', 'Greece', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(86, 'GL', 'Greenland', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(87, 'GD', 'Grenada', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(88, 'GP', 'Guadeloupe', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(89, 'GU', 'Guam', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(90, 'GT', 'Guatemala', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(91, 'XU', 'Guernsey and Alderney', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(92, 'GN', 'Guinea', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(93, 'GW', 'Guinea-Bissau', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(94, 'GY', 'Guyana', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(95, 'HT', 'Haiti', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(96, 'HM', 'Heard and McDonald Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(97, 'HN', 'Honduras', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(98, 'HK', 'Hong Kong S.A.R.', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(99, 'HU', 'Hungary', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(100, 'IS', 'Iceland', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(101, 'IN', 'India', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(102, 'ID', 'Indonesia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(103, 'IR', 'Iran', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(104, 'IQ', 'Iraq', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(105, 'IE', 'Ireland', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(106, 'IL', 'Israel', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(107, 'IT', 'Italy', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(108, 'JM', 'Jamaica', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(109, 'JP', 'Japan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(110, 'XJ', 'Jersey', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(111, 'JO', 'Jordan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(112, 'KZ', 'Kazakhstan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(113, 'KE', 'Kenya', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(114, 'KI', 'Kiribati', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(115, 'KP', 'Korea North', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(116, 'KR', 'Korea South', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(117, 'KW', 'Kuwait', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(118, 'KG', 'Kyrgyzstan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(119, 'LA', 'Laos', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(120, 'LV', 'Latvia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(121, 'LB', 'Lebanon', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(122, 'LS', 'Lesotho', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(123, 'LR', 'Liberia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(124, 'LY', 'Libya', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(125, 'LI', 'Liechtenstein', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(126, 'LT', 'Lithuania', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(127, 'LU', 'Luxembourg', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(128, 'MO', 'Macau S.A.R.', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(129, 'MK', 'Macedonia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(130, 'MG', 'Madagascar', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(131, 'MW', 'Malawi', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(132, 'MY', 'Malaysia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(133, 'MV', 'Maldives', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(134, 'ML', 'Mali', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(135, 'MT', 'Malta', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(136, 'XM', 'Man (Isle of)', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(137, 'MH', 'Marshall Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(138, 'MQ', 'Martinique', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(139, 'MR', 'Mauritania', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(140, 'MU', 'Mauritius', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(141, 'YT', 'Mayotte', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(142, 'MX', 'Mexico', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(143, 'FM', 'Micronesia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(144, 'MD', 'Moldova', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(145, 'MC', 'Monaco', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(146, 'MN', 'Mongolia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(147, 'MS', 'Montserrat', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(148, 'MA', 'Morocco', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(149, 'MZ', 'Mozambique', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(150, 'MM', 'Myanmar', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(151, 'NA', 'Namibia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(152, 'NR', 'Nauru', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(153, 'NP', 'Nepal', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(154, 'AN', 'Netherlands Antilles', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(155, 'NL', 'Netherlands The', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(156, 'NC', 'New Caledonia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(157, 'NZ', 'New Zealand', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(158, 'NI', 'Nicaragua', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(159, 'NE', 'Niger', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(160, 'NG', 'Nigeria', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(161, 'NU', 'Niue', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(162, 'NF', 'Norfolk Island', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(163, 'MP', 'Northern Mariana Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(164, 'NO', 'Norway', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(165, 'OM', 'Oman', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(166, 'PK', 'Pakistan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(167, 'PW', 'Palau', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(168, 'PS', 'Palestinian Territory Occupied', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(169, 'PA', 'Panama', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(170, 'PG', 'Papua new Guinea', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(171, 'PY', 'Paraguay', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(172, 'PE', 'Peru', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(173, 'PH', 'Philippines', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(174, 'PN', 'Pitcairn Island', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(175, 'PL', 'Poland', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(176, 'PT', 'Portugal', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(177, 'PR', 'Puerto Rico', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(178, 'QA', 'Qatar', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(179, 'RE', 'Reunion', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(180, 'RO', 'Romania', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(181, 'RU', 'Russia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(182, 'RW', 'Rwanda', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(183, 'SH', 'Saint Helena', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(184, 'KN', 'Saint Kitts And Nevis', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(185, 'LC', 'Saint Lucia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(186, 'PM', 'Saint Pierre and Miquelon', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(187, 'VC', 'Saint Vincent And The Grenadines', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(188, 'WS', 'Samoa', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(189, 'SM', 'San Marino', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(190, 'ST', 'Sao Tome and Principe', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(191, 'SA', 'Saudi Arabia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(192, 'SN', 'Senegal', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(193, 'RS', 'Serbia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(194, 'SC', 'Seychelles', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(195, 'SL', 'Sierra Leone', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(196, 'SG', 'Singapore', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(197, 'SK', 'Slovakia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(198, 'SI', 'Slovenia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(199, 'XG', 'Smaller Territories of the UK', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(200, 'SB', 'Solomon Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(201, 'SO', 'Somalia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(202, 'ZA', 'South Africa', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(203, 'GS', 'South Georgia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(204, 'SS', 'South Sudan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(205, 'ES', 'Spain', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(206, 'LK', 'Sri Lanka', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(207, 'SD', 'Sudan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(208, 'SR', 'Suriname', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(210, 'SZ', 'Swaziland', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(211, 'SE', 'Sweden', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(212, 'CH', 'Switzerland', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(213, 'SY', 'Syria', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(214, 'TW', 'Taiwan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(215, 'TJ', 'Tajikistan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(216, 'TZ', 'Tanzania', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(217, 'TH', 'Thailand', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(218, 'TG', 'Togo', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(219, 'TK', 'Tokelau', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(220, 'TO', 'Tonga', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(221, 'TT', 'Trinidad And Tobago', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(222, 'TN', 'Tunisia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(223, 'TR', 'Turkey', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(224, 'TM', 'Turkmenistan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(225, 'TC', 'Turks And Caicos Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(226, 'TV', 'Tuvalu', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(227, 'UG', 'Uganda', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(228, 'UA', 'Ukraine', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(229, 'AE', 'United Arab Emirates', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(230, 'GB', 'United Kingdom', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(231, 'US', 'United States', 1, 1, '2021-04-06 00:06:30', '2022-09-13 00:53:18', NULL),
(232, 'UM', 'United States Minor Outlying Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(233, 'UY', 'Uruguay', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(234, 'UZ', 'Uzbekistan', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(235, 'VU', 'Vanuatu', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(236, 'VA', 'Vatican City State (Holy See)', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(237, 'VE', 'Venezuela', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(238, 'VN', 'Vietnam', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(239, 'VG', 'Virgin Islands (British)', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(240, 'VI', 'Virgin Islands (US)', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(241, 'WF', 'Wallis And Futuna Islands', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(242, 'EH', 'Western Sahara', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(243, 'YE', 'Yemen', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(244, 'YU', 'Yugoslavia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(245, 'ZM', 'Zambia', 0, 1, '2021-04-06 00:06:30', NULL, NULL),
(246, 'ZW', 'Zimbabwe', 0, 1, '2021-04-06 00:06:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `docprofiles`
--

CREATE TABLE `docprofiles` (
  `id` bigint UNSIGNED NOT NULL,
  `home_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_zone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_media` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shot_bio` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `price` int DEFAULT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `docprofiles`
--

INSERT INTO `docprofiles` (`id`, `home_phone`, `gender`, `nid`, `address`, `area`, `state`, `country`, `time_zone`, `social_media`, `video_url`, `shot_bio`, `price`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '01720279279', '1', '19852716425223151', 'House 37, Road 22, Rupnagar R/A, Mirpur, Dhaka', 'Coventry Road', 'Dhaka', NULL, NULL, 'https://www.youtube.com/watch?v=_ONeq4G8VEE', NULL, '<p>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.</p>', 200, 2, '2023-12-18 09:41:30', '2023-12-18 09:41:30');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `general_problems`
--

CREATE TABLE `general_problems` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` int DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lifecycles`
--

CREATE TABLE `lifecycles` (
  `id` bigint UNSIGNED NOT NULL,
  `age` int DEFAULT NULL,
  `gender` int DEFAULT NULL,
  `cp` varchar(222) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `pressurehight` int NOT NULL DEFAULT '0',
  `pressurelow` int NOT NULL DEFAULT '0',
  `glucose` varchar(33) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.00',
  `caa` varchar(33) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thall` varchar(33) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oldpeak` varchar(33) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thalachh` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.00',
  `troponin` varchar(22) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.00',
  `weight` varchar(22) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.00',
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `date` date DEFAULT NULL,
  `ha_prediction` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lifecycles`
--

INSERT INTO `lifecycles` (`id`, `age`, `gender`, `cp`, `pressurehight`, `pressurelow`, `glucose`, `caa`, `thall`, `oldpeak`, `thalachh`, `troponin`, `weight`, `user_id`, `date`, `ha_prediction`, `created_at`, `updated_at`) VALUES
(1, 67, 1, '2', 33, 3, '5.66', '', '', '', '23', '12', '4', 1, '2022-06-15', NULL, '2024-01-05 16:05:16', '2024-01-05 16:05:16'),
(2, 67, 1, '1', 3, 4, '3', '2', '2', '2', '182', '4', '50', 1, '2024-01-26', NULL, '2024-01-05 16:51:54', '2024-01-05 16:51:54'),
(3, 67, 1, '1', 33, 3, '3', '2', '2', '2', '182', '12', '50', 1, '2024-01-19', '[[0.4191106671106671,0.5808893328893329]]', '2024-01-05 17:03:10', '2024-01-05 17:03:10');

-- --------------------------------------------------------

--
-- Table structure for table `medcompanies`
--

CREATE TABLE `medcompanies` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` int DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medicines`
--

CREATE TABLE `medicines` (
  `id` bigint UNSIGNED NOT NULL,
  `org_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age_limit` int DEFAULT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` int DEFAULT '1',
  `medcompany_id` bigint UNSIGNED DEFAULT NULL,
  `medvariant_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medtests`
--

CREATE TABLE `medtests` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` int DEFAULT '1',
  `testcategory_id` bigint UNSIGNED DEFAULT NULL,
  `reference_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medvariants`
--

CREATE TABLE `medvariants` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` int DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int NOT NULL,
  `message` text NOT NULL,
  `added_on` datetime NOT NULL,
  `type` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `message`, `added_on`, `type`) VALUES
(1, 'Hi', '2020-04-22 12:41:04', 'user'),
(2, 'Hello, how are you.', '2020-04-22 12:41:05', 'bot'),
(3, 'what is your name', '2020-04-22 12:41:22', 'user'),
(4, 'My name is Vishal Bot', '2020-04-22 12:41:22', 'bot'),
(5, 'Where are your from', '2020-04-22 12:41:30', 'user'),
(6, 'I m from India', '2020-04-22 12:41:30', 'bot'),
(7, 'Go to hell', '2020-04-22 12:41:41', 'user'),
(8, 'Sorry not be able to understand you', '2020-04-22 12:41:41', 'bot'),
(9, 'bye', '2020-04-22 12:41:46', 'user'),
(10, 'Sad to see you are going. Have a nice day', '2020-04-22 12:41:46', 'bot'),
(11, 'hh', '2023-12-31 03:44:03', 'user'),
(12, 'Sorry not be able to understand you', '2023-12-31 03:44:03', 'bot'),
(13, 'Hi', '2023-12-31 03:44:13', 'user'),
(14, 'Hello, how are you.', '2023-12-31 03:44:13', 'bot'),
(15, 'Hello, What is your name?', '2023-12-31 03:44:27', 'user'),
(16, 'Sorry not be able to understand you', '2023-12-31 03:44:27', 'bot'),
(17, 'What is your name?', '2023-12-31 03:44:36', 'user'),
(18, 'Sorry not be able to understand you', '2023-12-31 03:44:36', 'bot'),
(19, 'whats your name', '2023-12-31 03:50:07', 'user'),
(20, 'My name is Ehas Bot', '2023-12-31 03:50:07', 'bot'),
(21, 'Where are your from', '2023-12-31 03:51:19', 'user'),
(22, 'I m from Bangladesh', '2023-12-31 03:51:19', 'bot');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_10_26_015030_create_admins_table', 1),
(6, '2023_12_04_091922_create_apdates_table', 1),
(7, '2023_12_04_091937_create_appointments_table', 1),
(8, '2023_12_04_092354_create_docprofiles_table', 1),
(9, '2023_12_15_142338_create_preprofiles_table', 1),
(10, '2023_12_15_142425_create_prehistories_table', 1),
(11, '2023_12_15_143716_create_testcategories_table', 1),
(12, '2023_12_15_144524_create_medcompanies_table', 1),
(13, '2023_12_15_144605_create_medvariants_table', 1),
(14, '2023_12_15_144646_create_medicines_table', 1),
(15, '2023_12_15_151509_create_medtests_table', 1),
(16, '2023_12_16_161631_create_orders_table', 1),
(17, '2023_12_16_162543_create_apermissions_table', 1),
(18, '2023_12_16_163502_create_prescriptions_table', 1),
(19, '2023_12_16_163552_create_patient_assesments_table', 1),
(20, '2023_12_16_163714_create_patient_med_tests_table', 1),
(21, '2023_12_16_163732_create_patient_medicines_table', 1),
(22, '2023_12_16_163807_create_patient_test_results_table', 1),
(23, '2023_12_17_170356_create_lifecycles_table', 2),
(24, '2023_12_29_061627_create_countries_table', 3),
(25, '2024_01_01_120712_create_general_problems_table', 4),
(26, '2024_01_01_120726_create_specific_problems_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint UNSIGNED NOT NULL,
  `orderID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `appointment_id` bigint UNSIGNED DEFAULT NULL,
  `transaction_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `orderID`, `name`, `email`, `mobile`, `amount`, `user_id`, `appointment_id`, `transaction_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '17028318323060905', 'Mr. Patient', 'user@gmail.com', '07476972191', 200, 1, 3, 'TRS972191', 1, '2023-12-18 09:50:37', '2023-12-18 09:50:37'),
(2, '829791', 'Mr. Patient', 'user@gmail.com', '07476972191', 20000, 1, 17028364689299, 'txn_3OOjF7JOMPLKDFPJ1wwk73Gi', 2, '2023-12-18 16:00:35', '2023-12-18 16:00:35'),
(3, '628967', 'Mr. Patient', 'user@gmail.com', '07476972191', 20000, 1, 17028364683135, 'txn_3OOjakJOMPLKDFPJ0MAnD0Ug', 2, '2023-12-18 16:22:56', '2023-12-18 16:22:56'),
(4, '956777', 'Mr. Patient', 'user@gmail.com', '07476972191', 20000, 1, 17040858513536, 'txn_3OTdqNJOMPLKDFPJ1TfjNhJF', 2, '2024-01-01 05:15:20', '2024-01-01 05:15:20');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient_assesments`
--

CREATE TABLE `patient_assesments` (
  `id` bigint UNSIGNED NOT NULL,
  `prescription_id` bigint UNSIGNED DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `patient_id` bigint UNSIGNED DEFAULT NULL,
  `doctor_id` bigint UNSIGNED DEFAULT NULL,
  `appointment_id` bigint UNSIGNED DEFAULT NULL,
  `status` int DEFAULT '1',
  `general_problem_id` bigint UNSIGNED DEFAULT NULL,
  `specific_problem_id` bigint UNSIGNED DEFAULT NULL,
  `detail_observation` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `symptoms` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient_medicines`
--

CREATE TABLE `patient_medicines` (
  `id` bigint UNSIGNED NOT NULL,
  `prescription_id` bigint UNSIGNED DEFAULT NULL,
  `patient_id` bigint UNSIGNED DEFAULT NULL,
  `doctor_id` bigint UNSIGNED DEFAULT NULL,
  `medicine_id` bigint UNSIGNED DEFAULT NULL,
  `medicine_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appointment_id` bigint UNSIGNED DEFAULT NULL,
  `status` int DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient_med_tests`
--

CREATE TABLE `patient_med_tests` (
  `id` bigint UNSIGNED NOT NULL,
  `prescription_id` bigint UNSIGNED DEFAULT NULL,
  `testcategory_id` bigint UNSIGNED DEFAULT NULL,
  `med_test_id` bigint UNSIGNED DEFAULT NULL,
  `patient_id` bigint UNSIGNED DEFAULT NULL,
  `appointment_id` bigint UNSIGNED DEFAULT NULL,
  `status` int DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient_test_results`
--

CREATE TABLE `patient_test_results` (
  `id` bigint UNSIGNED NOT NULL,
  `prescription_id` bigint UNSIGNED DEFAULT NULL,
  `patient_id` bigint UNSIGNED DEFAULT NULL,
  `appointment_id` bigint UNSIGNED DEFAULT NULL,
  `status` int DEFAULT '1',
  `date` date DEFAULT NULL,
  `patient_med_test_id` bigint UNSIGNED DEFAULT NULL,
  `result_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `result_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prehistories`
--

CREATE TABLE `prehistories` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_age` int DEFAULT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `report` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `preprofiles`
--

CREATE TABLE `preprofiles` (
  `id` bigint UNSIGNED NOT NULL,
  `gender` int DEFAULT '1',
  `home_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `weight` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disable_history` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `preprofiles`
--

INSERT INTO `preprofiles` (`id`, `gender`, `home_phone`, `dob`, `weight`, `disable_history`, `address`, `area`, `state`, `country`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, '01720279279', '1984-12-10', NULL, NULL, 'House 37, Road 22, Rupnagar R/A, Mirpur, Dhaka', 'Coventry Road', 'Dhaka', '18', 1, '2023-12-31 05:53:30', '2023-12-31 05:53:30');

-- --------------------------------------------------------

--
-- Table structure for table `prescriptions`
--

CREATE TABLE `prescriptions` (
  `id` bigint UNSIGNED NOT NULL,
  `prescriptionID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` bigint UNSIGNED DEFAULT NULL,
  `patient_id` bigint UNSIGNED DEFAULT NULL,
  `doctor_id` bigint UNSIGNED DEFAULT NULL,
  `date` date DEFAULT NULL,
  `appointment_id` bigint UNSIGNED DEFAULT NULL,
  `status` int DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `specific_problems`
--

CREATE TABLE `specific_problems` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `general_problem_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` int DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testcategories`
--

CREATE TABLE `testcategories` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `userID` bigint DEFAULT NULL,
  `f_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('doctor','patient','support') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'patient',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int DEFAULT NULL,
  `status` enum('Active','Inactive') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified` int NOT NULL DEFAULT '0',
  `user_id` bigint DEFAULT NULL,
  `created_by` bigint UNSIGNED DEFAULT NULL,
  `updated_by` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userID`, `f_name`, `l_name`, `email`, `mobile`, `designation`, `role`, `code`, `active`, `status`, `email_verified_at`, `password`, `images`, `verified`, `user_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 17028318323060995, 'Mr. Patient', 'Hossain', 'user@gmail.com', '07476972191', NULL, 'patient', NULL, NULL, 'Active', NULL, '$2y$10$pCd2VBH1c3pysynswUAzmeZJNDv3rHezHO4HzNJRu/qtXr252XazS', 'dummy.png', 0, NULL, NULL, NULL, NULL, NULL),
(2, 17028318323273356, 'Mr. Doctor', 'Hossain', 'doctor@gmail.com', '07476972192', 'Child Specialist', 'doctor', NULL, NULL, 'Active', NULL, '$2y$10$hmSw3MLEJ0f8CkTXoazR5.4HcIEIN1E8PxOO62tXRvtzo51.TgR4u', 'dummy.png', 0, NULL, NULL, NULL, NULL, NULL),
(3, 17028318322554606, 'Mr. Support', 'Hossain', 'support@gmail.com', '07476972193', NULL, 'support', NULL, NULL, 'Active', NULL, '$2y$10$nxhPmUDsTQEHnYT8pgR.vepVsbspYb1Y1hX8yZ6Sq/H5VAOQd8KyG', 'dummy.png', 0, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD UNIQUE KEY `admins_mobile_unique` (`mobile`);

--
-- Indexes for table `apdates`
--
ALTER TABLE `apdates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apermissions`
--
ALTER TABLE `apermissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbot_hints`
--
ALTER TABLE `chatbot_hints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `docprofiles`
--
ALTER TABLE `docprofiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `general_problems`
--
ALTER TABLE `general_problems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lifecycles`
--
ALTER TABLE `lifecycles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medcompanies`
--
ALTER TABLE `medcompanies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medicines`
--
ALTER TABLE `medicines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medtests`
--
ALTER TABLE `medtests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medvariants`
--
ALTER TABLE `medvariants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `patient_assesments`
--
ALTER TABLE `patient_assesments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_medicines`
--
ALTER TABLE `patient_medicines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_med_tests`
--
ALTER TABLE `patient_med_tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_test_results`
--
ALTER TABLE `patient_test_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `prehistories`
--
ALTER TABLE `prehistories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preprofiles`
--
ALTER TABLE `preprofiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prescriptions`
--
ALTER TABLE `prescriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specific_problems`
--
ALTER TABLE `specific_problems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testcategories`
--
ALTER TABLE `testcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `apdates`
--
ALTER TABLE `apdates`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `apermissions`
--
ALTER TABLE `apermissions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `chatbot_hints`
--
ALTER TABLE `chatbot_hints`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `docprofiles`
--
ALTER TABLE `docprofiles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general_problems`
--
ALTER TABLE `general_problems`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lifecycles`
--
ALTER TABLE `lifecycles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `medcompanies`
--
ALTER TABLE `medcompanies`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medicines`
--
ALTER TABLE `medicines`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medtests`
--
ALTER TABLE `medtests`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medvariants`
--
ALTER TABLE `medvariants`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `patient_assesments`
--
ALTER TABLE `patient_assesments`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient_medicines`
--
ALTER TABLE `patient_medicines`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient_med_tests`
--
ALTER TABLE `patient_med_tests`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient_test_results`
--
ALTER TABLE `patient_test_results`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prehistories`
--
ALTER TABLE `prehistories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `preprofiles`
--
ALTER TABLE `preprofiles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prescriptions`
--
ALTER TABLE `prescriptions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `specific_problems`
--
ALTER TABLE `specific_problems`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testcategories`
--
ALTER TABLE `testcategories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
