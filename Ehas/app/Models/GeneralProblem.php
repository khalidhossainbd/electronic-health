<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeneralProblem extends Model
{
    use HasFactory;
    
    protected $table = 'general_problems';
    
    protected $fillable = ['title', 'content', 'status'];
    
    public function specifics()
    {
        return $this->hasMany(SpecificProblem::class, 'general_problem_id');
    }
}
