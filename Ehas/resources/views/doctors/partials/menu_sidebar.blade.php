<aside class="menu-sidebar2">
    <div class="logo">
        <a href="{{ route('doctor.dashboard') }}">
            {{-- <img src="{{ asset('main/images/icon/logo-white.png') }}" alt="Cool Admin" /> --}}
            <img class="img-fluid" src="{{ asset('front/dist/images/ehas_white.png') }}" alt="Logo EHAS" style="max-height: 55px;">
        </a>
    </div>
    <div class="menu-sidebar2__content js-scrollbar1">
        <div class="account2">
            <div class="image img-cir img-120">
                <img src="{{ Auth::user()->images == "dummy.png" ? asset('images/dummy.png') : asset('uploads/users/'.Auth::user()->images) }}" alt="{{ Auth::user()->name }}" alt="John Doe" />
            </div>
            <h4 class="name">{{ Auth::user()->f_name }} {{ Auth::user()->l_name }}</h4>
            <p> <i>{{ Auth::user()->designation }} </i></p>
        </div>
        <nav class="navbar-sidebar2">
            <ul class="list-unstyled navbar__list">
                <li class="active">
                    <a href="{{ route('doctor.dashboard') }}">
                        <i class="fas fa-tachometer-alt"></i>Dashboard
                    </a>
                    {{-- <span class="inbox-num">3</span> --}}
                </li>
                <li>
                    <a href="{{ route('doctor.profile') }}">
                        <i class="fas fa-chart-bar"></i>Profile
                    </a>
                    {{-- <span class="inbox-num">3</span> --}}
                </li>
                <li>
                    <a href="{{ route('doctor.active-appointment') }}">
                        <i class="fas fa-calendar-alt"></i>Appointment
                    </a>
                </li>
                <li>
                    <a href="{{ route('doctor.assistant') }}">
                        <i class="far fa-check-square"></i>Assistant
                    </a>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-trophy"></i>Setups
                        <span class="arrow">
                            <i class="fas fa-angle-down"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="{{ route('doctor.appointment') }}">
                                <i class="fas fa-table"></i>Appointment Setup</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="far fa-check-square"></i>Payment Setup</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-calendar-alt"></i>Paitent History</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-map-marker-alt"></i>Payment History</a>
                        </li>
                    </ul>
                </li>
                {{--<li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-copy"></i>Pages
                        <span class="arrow">
                            <i class="fas fa-angle-down"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="#">
                                <i class="fas fa-sign-in-alt"></i>Login</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-user"></i>Register</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-unlock-alt"></i>Forget Password</a>
                        </li>
                    </ul>
                </li>
                 <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-desktop"></i>UI Elements
                        <span class="arrow">
                            <i class="fas fa-angle-down"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="button.html">
                                <i class="fab fa-flickr"></i>Button</a>
                        </li>
                        <li>
                            <a href="badge.html">
                                <i class="fas fa-comment-alt"></i>Badges</a>
                        </li>
                        <li>
                            <a href="tab.html">
                                <i class="far fa-window-maximize"></i>Tabs</a>
                        </li>
                        <li>
                            <a href="card.html">
                                <i class="far fa-id-card"></i>Cards</a>
                        </li>
                        <li>
                            <a href="alert.html">
                                <i class="far fa-bell"></i>Alerts</a>
                        </li>
                        <li>
                            <a href="progress-bar.html">
                                <i class="fas fa-tasks"></i>Progress Bars</a>
                        </li>
                        <li>
                            <a href="modal.html">
                                <i class="far fa-window-restore"></i>Modals</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-toggle-on"></i>Switchs</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-th-large"></i>Grids</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-font-awesome"></i>FontAwesome</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-font"></i>Typography</a>
                        </li>
                    </ul>
                </li> --}}
            </ul>
        </nav>
    </div>
</aside>