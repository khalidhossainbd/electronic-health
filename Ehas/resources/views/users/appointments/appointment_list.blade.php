@extends('layouts.user-app')

@section('title')
    Patient Profile
@endsection

@section('content')

	<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="overview-wrap">
                    <h2 class="title-1">{{ Auth::user()->f_name }} {{ Auth::user()->l_name }}</h2>
                </div>
            </div>
        </div>
        <div class="row m-t-25">
            <div class="col-12">
    			<div class="card">
    				<div class="card-header">
    					<strong>Upcoming</strong> Appointments
    				</div>
    				<div class="card-body card-block">
                        @if(count($myappointment)>0)
                        <table class="table table-bordered">
                            <thead>     
                                <tr>
                                    <td>Order ID</td>
                                    <td>Doctor Name</td>
                                    <td>Date</td>
                                    <td>Start Time</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                        
    					@foreach($myappointment as $item)
                            <tbody>
                                <tr>
                                    <td>{{ $item->orderID }}</td>
                                    <td>
                                        {{ App\Helpers\Phelper::getDoctorName($item->appointment_id)->f_name }}
                                        {{ App\Helpers\Phelper::getDoctorName($item->appointment_id)->l_name }}
                                    </td>
                                    <td>{{ App\Helpers\Phelper::getAppDate($item->appointment_id) }}</td>
                                    <td>{{ App\Helpers\Phelper::getAppTime($item->appointment_id)}}</td>
                                    <td>
                                       
                                        <a href="{{ App\Helpers\Phelper::getLink($item->appointment_id)}}" class="au-btn au-btn-icon au-btn--blue my-2 text-white" target="_new" onclick="window.open(this.href,'newwindow','width=800,height=600'); return false;">GO LIVE</a>
                                    </td>
                                </tr>
                            </tbody>
                        @endforeach
                        </table>
                        @else
                        @endif
    				</div>
    			</div>
		    </div>
		</div>
	</div>	

@endsection