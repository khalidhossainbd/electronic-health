<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testcategory extends Model
{
    use HasFactory;


    protected $table = 'testcategories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

        /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','status'
    ];
    
    public function tests()
    {
        return $this->hasMany(Medtest::class, 'testcategory_id');
    }
    public function scopeStatus($query, $id)
    {
        return $query->where('status', $id);
    }
   
}
