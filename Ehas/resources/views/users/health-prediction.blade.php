@extends('layouts.user-app')

@section('title')
    Patient Profile
@endsection

@section('content')

	<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="overview-wrap">
                    <h2 class="title-1">{{ Auth::user()->f_name }} {{ Auth::user()->l_name }}</h2>
                </div>
            </div>
        </div>
        <div class="row m-t-25">
            <div class="col-12">
    			<div class="card">
    				<div class="card-header">
    					<div class="row">
    						<div class="col-6">
    							<strong>Health Prediction</strong> 
    						</div>
    						<div class="col-6 text-right">
    							@if(Session::has('message')) <span class="text-success p-1">{{ Session::get('message') }}</span> @endif
    						</div>
    					</div>
    				</div>
    				<div class="card-body card-block">
    						{!! $page !!}
    				</div>
    			</div>
			</div>
		</div>
	</div>		




@endsection

@section('java_script')

<script>

$(function(){
	 
			
});
$('textarea').each(function () {
    //const editor = Jodit.make(this);
});
</script>

@endsection