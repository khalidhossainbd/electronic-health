@extends('layouts.doctor-app')

@section('title')
    Doctor Dashboard
@endsection

@section('content')

    <!-- BREADCRUMB-->
    <section class="au-breadcrumb m-t-75">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="au-breadcrumb-content">
                            <div class="au-breadcrumb-left">
                                <h4 class="name">Welcome to EHAS </h4>
                                
                            </div>
                            <a href="#" class="au-btn au-btn-icon au-btn--green">
                                View prescription
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
    <!-- STATISTIC-->
    <section class="statistic">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                	    <div class="col-12">
                    		@if(Session::has('message')) <span class="text-success p-1">{{ Session::get('message') }}</span> @endif
                    	</div>

                    <div class="col-12 bg-white p-5">
                        <ul class="nav nav-tabs justify-content-end nav-pills nav-fill border-0 mb-3" id="myTab" role="tablist">
                          <li class="nav-item mx-1">
                            <a class="nav-link @if($request->has('tab') && $request->tab == 'patient' ) active @endif border border-primary"  href="{{ route('doctor.visit', ['id' =>$id, 'tab'=>'patient']) }}">Patient</a>
                          </li>
                          <li class="nav-item mx-1">
                            <a class="nav-link @if($request->has('tab') && $request->tab == 'assesment' ) active @endif border border-primary"  href="{{ route('doctor.visit', ['id' =>$id, 'tab'=>'assesment']) }}">Assesment</a>
                          </li>
                          <li class="nav-item mx-1">
                            <a class="nav-link @if($request->has('tab') && $request->tab == 'observation' ) active @endif border border-primary"  href="{{ route('doctor.visit', ['id' =>$id, 'tab'=>'observation']) }}">Observation</a>
                          </li>
                          <li class="nav-item mx-1">
                            <a class="nav-link @if($request->has('tab') && $request->tab == 'medical' ) active @endif border border-primary"  href="{{ route('doctor.visit', ['id' =>$id, 'tab'=>'medical']) }}">Medical Test</a>
                          </li>
                          <li class="nav-item mx-1">
                            <a class="nav-link @if($request->has('tab') && $request->tab == 'medicine' ) active @endif border border-primary"  href="{{ route('doctor.visit', ['id' =>$id, 'tab'=>'medicine']) }}">Medicine</a>
                          </li>
                        </ul>
                        <div class="tab-content border" id="myTabContent">
                          <div class="tab-pane fade @if($request->has('tab') && $request->tab == 'patient' ) show active @endif p-3" id="home" role="tabpanel" aria-labelledby="home-tab">
                            @include('doctors.prescriptions.patient')
                          </div>
                          <div class="tab-pane fade @if($request->has('tab') && $request->tab == 'assesment' ) show active @endif p-3" id="assesment" role="tabpanel" aria-labelledby="assesment-tab">
                            @include('doctors.prescriptions.assesment')
                          </div>
                          <div class="tab-pane fade @if($request->has('tab') && $request->tab == 'observation' ) show active @endif p-3" id="observation" role="tabpanel" aria-labelledby="observation-tab">
                            @include('doctors.prescriptions.observation')
                          </div>
                          <div class="tab-pane fade @if($request->has('tab') && $request->tab == 'medical' ) show active @endif p-3" id="medical" role="tabpanel" aria-labelledby="medical-tab">
                            @include('doctors.prescriptions.medical_test')
                          </div>
                          <div class="tab-pane fade @if($request->has('tab') && $request->tab == 'medicine' ) show active @endif p-3" id="medicine" role="tabpanel" aria-labelledby="medicine-tab">
                            @include('doctors.prescriptions.medicine')
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END STATISTIC-->
@endsection

@section('java_script')
<script>
	
	$(function(){
		var general_problems = '{!! $general_problems->toJson() !!}';	
		general_problems = JSON.parse(general_problems);
		$(document).on('change', '#general_problem_id', function(e){
			e.preventDefault();
			var general_id = $(this).val();
			var general = general_problems.find((item)=>{return item.id == general_id});
			if(!general){return;}
			var specifics = general.specifics;
			if(!specifics){return;}
			var specific_options = specifics.map(function(item){
					return `<option value="${item.id}">${item.title}</option>`;
				});
			specific_options = specific_options.join('');
			console.log(specific_options);
			$('#specific_problem_id').html(specific_options);
		});

		$(function(){
			$(`[name="medicine_id"]`).select2();
		});

		$(document).on('click', '.cancel-pop', function(e){
			e.preventDefault();
			//$("[data-toggle='popover']").popover('hide');
		});
		$("[data-toggle='popover']").popover({ trigger: "hover" });
	});
</script>

@endsection
