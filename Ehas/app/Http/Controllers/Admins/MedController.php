<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Medicine;
use Redirect,Response;
use App\Models\Medvariant;
use App\Models\Medcompany;

class MedController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        view()->share('module', 'Medicine');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Medicine::query();
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.meds.index', compact('data','rank'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $variants = Medvariant::where('status', 1)->get();
        $companies = Medcompany::where('status', 1)->get();
        return view('admins.meds.create', ['variants' => $variants, 'companies' => $companies]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'org_name' => ['required', 'string', 'max:255'],
            'com_name' => ['required', 'string', 'max:255'],
            'medvariant_id' => ['required', 'string', 'max:255'],
            'medvariant_id' => ['required', 'string', 'max:255'],
        ]);
        
        
        $med = new Medicine();
        $med->org_name = $request->org_name;
        $med->com_name = $request->com_name;
        $med->age_limit = $request->age_limit;
        $med->details = $request->details;
        $med->medvariant_id = $request->medvariant_id;
        $med->medcompany_id = $request->medcompany_id;
        $med->status = $request->status == 'on' ? 1 : 0;
        
        if($med->save()){
            return Redirect::route('meds.index')->withSuccess('Great! New Medicine added successfully.');
        }
        
        return Redirect::route('meds.index')->withError('Sorry! Medicine add Failed.');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Medicine::findOrFail($id);
        $variants = Medvariant::where('status', 1)->get();
        $companies = Medcompany::where('status', 1)->get();
        return view('admins.meds.edit', compact('data', 'variants', 'companies'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'org_name' => ['required', 'string', 'max:255'],
            'com_name' => ['required', 'string', 'max:255'],
            'medvariant_id' => ['required', 'string', 'max:255'],
            'medvariant_id' => ['required', 'string', 'max:255'],
        ]);
        
        $med = Medicine::findOrFail($id);
        $med->org_name = $request->org_name;
        $med->com_name = $request->com_name;
        $med->age_limit = $request->age_limit;
        $med->details = $request->details;
        $med->medvariant_id = $request->medvariant_id;
        $med->medcompany_id = $request->medcompany_id;
        $med->status = $request->status == 'on' ? 1 : 0;
        
        if($med->save()){
            return Redirect::route('meds.index')->withSuccess('Great! Medicine updated successfully.');
        }
        
        return Redirect::route('meds.index')->withError('Sorry! Medicine updated Failed.');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Medicine::destroy($id);
        return Redirect::route('meds.index')->withSuccess('Great! Medicine deleted successfully.');
    }
    

}
