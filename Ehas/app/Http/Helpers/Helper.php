<?php 

if(!function_exists('get_object')){
    function get_object($items = null){
        if(null != $items){
            return json_decode(json_encode($items));
        }
        return json_decode('{}');
    }
}


if(!function_exists('getSelectOptions')){
    function getSelectOptions($items, $field, $selected = null)
    {
        if($items->count()){
            $selectBlock = $items->map(function($item)use($selected, $field){
                $selected = $item->id == ($selected ) ? 'selected' : '';
                return '<option value="'.$item->id.'" '.$selected.'>'.$item->$field.'</option>';
            })->all();
            $selectBlock = implode('', $selectBlock);
            return $selectBlock;
        }
        return "";
    }
}


if(!function_exists('val')){
    function val($key, $collection){
        if(null !== $collection && $collection->offsetExists($key)){
            return $collection->$key;
        }
        return old($key);
    }
}


if(!function_exists('uploadImage')){
    function uploadImage($file, $path, $diamension = [800, 600], $existing = false )
    {
        $filenewname = "";
        makeDirPath($path);
        try{
            $image = Intervention\Image\Facades\Image::make($file);
            $fileName = $file->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).time().".".$filetype;
            $image->resize($diamension[0], $diamension[1]);
            $image->save($path.$filenewname);
            if($existing && !empty($existing) && file_exists($path.$existing)){
                Illuminate\Support\Facades\File::delete($path.$existing);
            }
            
        }catch(Exception $e){
            $filenewname = "";
        }
        
        return $filenewname;
    }
}

if(!function_exists('makeDirPath')){
    function makeDirPath($path) {
        return file_exists($path) || mkdir($path, 0777, true);
    }
}

if(!function_exists('get_http_response_code')){
    
    function get_http_response_code($theURL) {
        $headers = get_headers($theURL);
        return substr($headers[0], 9, 3);
    }
    
}
