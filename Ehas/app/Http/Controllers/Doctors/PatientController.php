<?php

namespace App\Http\Controllers\Doctors;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Lifecycle;
use App\Models\PatientAssesment;
use App\Models\Prescription;
use App\Models\Testcategory;
use App\Models\User;
use App\Models\Docprofile;
use App\Models\Appointment;
use App\Models\Apdate;
use Illuminate\Support\Facades\Session;

use DateTime;

use Auth;
use Redirect,Response;
use App\Models\GeneralProblem;
use App\Models\SpecificProblem;
use App\Models\Medtest;
use App\Models\PatientMedTest;
use App\Models\Medicine;
use App\Models\PatientMedicine;


class PatientController extends Controller
{
    public function index (Request $request)
    {
		$doctor_id = Auth::user()->id;
		$appointment_data = Apdate::where('user_id', $doctor_id)->whereDate('date', '>=', date('Y-m-d'))->get();
		// dd($appointment_data);
		$appointments = null;
		if(count($appointment_data)>0){
			$appointments = Appointment::where('user_id', $doctor_id)->whereDate('date', '=', $appointment_data->first()->date)->where('status', 2)->get();
			if($request->has('date')){
				$appointments = Appointment::where('user_id', $doctor_id)->whereDate('date', '=', $request->date)->where('status', 2)->get();
			}
		}
		return view('doctors.appointments.active-appointments', get_defined_vars());
    }
    
    private function handleAssesment($request, $id){
        $request->validate([
            'general_problem_id'  => ['required', 'string', 'max:255'],
            'detail_observation'  => ['required', 'string', 'max:255'],
            'specific_problem_id' => ['required', 'string', 'max:255'],
            'symptoms'            => ['required', 'string', 'max:255'],
        ]);
        $appointment = Appointment::find($id);
        $common = [
            'patient_id' => $appointment->order->patient->id,
            'doctor_id' => $appointment->doctor->id,
            'appointment_id' => $id,
        ];
        
        $prescription = Prescription::firstOrCreate($common, array_merge($common, [
            'prescriptionID' => time().rand(1111, 9999),
            'date' => $appointment->date,
            'status' => 1,
            'order_id' => $appointment->order->id,
        ]));
        
        $assesment = $request->merge([
            'prescription_id' => $prescription->id,
            'created_by' => $appointment->doctor->id,
            'patient_id' => $appointment->order->patient->id,
            'doctor_id' => $appointment->doctor->id,
            'appointment_id' => $id,
            'status' => 1,
        ])->except(['_token', 'handleAssesment']);
        
        $ifUpdated = PatientAssesment::updateOrCreate($common, $assesment);
        $message = 'Patient Assesment Update Failed.';
        if($ifUpdated){
            $message =  'Patient Assesment Updated';
        }
        return $message;
    }
    
    private function handleObservation($request, $id)
    {
        $request->validate([
            'impluse' => ['numeric', 'max:255'],
            'pressurehight' => ['numeric', 'max:255'],
            'pressurelow' => ['numeric', 'max:255'],
            'glucose' => ['numeric', 'between:0,99.99'],
            'kcm' => ['numeric', 'between:0,99.99'],
            'troponin' => ['numeric', 'between:0,99.99'],
        ]);
        $appointment = Appointment::find($id);
        $data = $request->merge(['user_id' => $appointment->order->patient->id])->except(['_token', 'handleObservation']);
        $ifUpdated = Lifecycle::create($data);
        $message = 'Patient Ovservation Create Failed.';
        if($ifUpdated){
            $message =  'Patient Ovservation Created';
        }
        return $message;
    }
    
    private function handleMedical($request, $id)
    {
        $request->validate([
            'tests.*' => 'required|numeric'
        ]);
        
        $appointment = Appointment::find($id);
        $common = ['patient_id' => $appointment->order->patient->id,'doctor_id' => $appointment->doctor->id,'appointment_id' => $id];
        $prescription = Prescription::firstOrCreate($common, array_merge($common, ['prescriptionID' => time().rand(1111, 9999),'date' => $appointment->date,'status' => 1,'order_id' => $appointment->order->id]));
        $tests = collect($request->tests);
        $appointment = Appointment::find($id);
        $items = $tests->map(function($item)use($appointment, $prescription){
            $test = Medtest::find($item);
            return ['prescription_id'=>$prescription->id, 'testcategory_id'=>$test->category->id, 'med_test_id' => $item, 'patient_id' => $appointment->order->patient->id, 'appointment_id' => $appointment->id, 'status' => 1];
        });
        PatientMedTest::where(['prescription_id'=>$prescription->id, 'patient_id' => $appointment->order->patient->id, 'appointment_id' => $appointment->id])->delete();
        $ifUpdated = PatientMedTest::insert($items->all());
        $message = 'Patient Medical Test Update Failed.';
        if($ifUpdated){
            $message =  'Patient Medical Test Update Successful';
        }
        return $message;
    }
    
    private function handleMedicine($request, $id){
        $request->validate([
            'medicine_time' => 'required|numeric',
            'medicine_id' => 'required|numeric'
        ]);
        $appointment = Appointment::find($id);
        $common = ['patient_id' => $appointment->order->patient->id,'doctor_id' => $appointment->doctor->id,'appointment_id' => $id];
        $prescription = Prescription::firstOrCreate($common, array_merge($common, ['prescriptionID' => time().rand(1111, 9999),'date' => $appointment->date,'status' => 1,'order_id' => $appointment->order->id]));
        $ifUpdated = PatientMedicine::create($request->merge($common)->merge(['status' => 1, 'prescription_id' => $prescription->id])->except(['_token', 'handleMedicine']));
        $message = 'Patient Medicine Add Failed.';
        if($ifUpdated){
            $message =  'Patient Medicine Add Successfull';
        }
        return $message;
    }
    
    public function handlePostRequest($request, $id)
    {
        $method = $request->method;
        if(method_exists($this, $method)){
            return $this->$method($request, $id);
        }
        return 'Invalid Request';
    }

    public function visit(Request $request, $id)
    {
        if($request->isMethod('post')){
            $responseMsg = $this->handlePostRequest($request, $id);
            return redirect()->route('doctor.visit', ['id'=>$id, 'tab'=>$request->tab])->with('message', $responseMsg);
        }
        if($request->has('action') && $request->action == 'delete' && $request->has('medicine_id')){
            PatientMedicine::find($request->medicine_id)->delete();
            return redirect()->route('doctor.visit', ['id'=>$id, 'tab'=>$request->tab])->with('message', 'Medicine Removed');
        }
        
        $appoint = Appointment::with(['order'=>function($b){return $b->with(['patient'=>function($p){return $p->with(['prehistories', 'lifecycles']);}]);}])->find($id);
        $general_problems = GeneralProblem::with(['specifics'=>function($b){return $b->where('status', 1);}])->where('status', 1)->get();
        $common = ['patient_id' => $appoint->order->patient->id,'doctor_id' => $appoint->doctor->id,'appointment_id' => $id];
        $assesment = PatientAssesment::where($common)->first();
        $specific_problems = SpecificProblem::where('status', 1)->get();
        $lifecycle = collect();
        $categories = Testcategory::whereHas('tests',function($a){return $a->status(1);})->with(['tests' => function($b){return $b->status(1);}])->status(1)->get();
        $medtests = PatientMedTest::where(['patient_id' => $appoint->order->patient->id,'appointment_id' => $id])->pluck('med_test_id');
        $medicines = Medicine::where('status', 1)->get();
        $patientMedicines = PatientMedicine::where('status', 1)->get();
        return view('doctors.appointments.doctor-visit', get_defined_vars());
    }

}
