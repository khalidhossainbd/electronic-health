<?php

use App\Http\Controllers\Doctors\DoctorHomeController;
use App\Http\Controllers\Doctors\AssistantController;
use App\Http\Controllers\Doctors\AppointmentController;
use App\Http\Controllers\Doctors\PrescriptionController;
use App\Http\Controllers\Doctors\PatientController;

Route::prefix('/doctors')->middleware(['auth', 'role:doctor'])->group(function(){

    Route::get('/dashboard', [DoctorHomeController::class ,'index'])->name('doctor.dashboard');

    Route::get('/profile', [DoctorHomeController::class ,'docProfile'])->name('doctor.profile');
    Route::get('/profile-create', [DoctorHomeController::class ,'docProfileCreate'])->name('doctor.profile.create');
    Route::post('/profile-store', [DoctorHomeController::class ,'docProfileStore'])->name('doctor.profile.store');
    Route::get('/profile-edit/{id}', [DoctorHomeController::class ,'docProfileEdit'])->name('doctor.profile.edit');
    Route::patch('/profile-update/{id}', [DoctorHomeController::class ,'docProfileUpdate'])->name('doctor.profile.update');


    Route::get('/assistant', [AssistantController::class ,'index'])->name('doctor.assistant');
    Route::get('/assistant-create', [AssistantController::class ,'create'])->name('doctor.assistant.create');

    Route::get('/appointment', [AppointmentController::class ,'index'])->name('doctor.appointment');
    Route::get('/appointment-create', [AppointmentController::class ,'create'])->name('doctor.appointment.create');
    Route::post('/appointment-store', [AppointmentController::class ,'store'])->name('doctor.appointment.store');

	Route::get('/active-appointment', [PatientController::class, 'index'])->name('doctor.active-appointment');
    Route::any('/doctor-appointment-visit/{id}', [PatientController::class, 'visit'])->name('doctor.visit');
	
    // Prescription route list
    Route::get('/doctor-prescription', [PrescriptionController::class, 'index'])->name('doctor.prescription');

});