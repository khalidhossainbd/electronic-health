<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Appointment;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Prescription;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = json_decode('{}');
        $data->patient = User::where('role', "patient")->count();
        $data->doctor = User::where('role', "doctor")->count();
        $data->prescription = Prescription::count();
        $data->appointment = Appointment::count();
        $data->order = Order::sum('amount');
        return view('admins.dashboard', get_defined_vars());
    }
    
    public function statusChange(Request $request)
    {
        $table = $request->table;
        $id = $request->id;
        $status = $request->status;
        $field = isset($request->field) && !empty($request->field) ? $request->field : 'status' ;
        $status = DB::table($table)->where('id', $id)->update([$field => $status]);
        if($status){
            return response()->json(['status' => 1, 'msg' => "Data Updated"]);
        }
        return response()->json(['status' => 0, 'msg' => "Data Could not be Updated"]);
    }
    
    public function admin_create()
    {
        $module = 'Admin';
        return view('admins.users.admins.create', get_defined_vars()); 
    }
    
    
    public function admin_list()
    {
        $module = 'Admin';
        $data = Admin::query();
        
        $data->where('status', "Active");
        
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.users.admins.index', get_defined_vars());    
    }
    
    public function doctor_list()
    {
        $module = 'Doctor';
        $data = User::query();
        $data->where('role', "doctor");
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.users.doctors.index', get_defined_vars());
    }
    
    public function patient_list()
    {
        $module = 'Patient';
        $data = User::query();
        $data->where('role', "patient");
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.users.patients.index', get_defined_vars());
    }
    
    public function support_list()
    {
        $module = 'Support';
        $data = User::query();
        $data->where('role', "support");
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.users.supports.index', get_defined_vars());
    }
    
    public function appointments()
    {
        $module = 'Appointment';
        $data = User::query();
        $data->where('role', "doctor");
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.front.appointments.index', get_defined_vars());
    }
    
    public function appointments_list($id)
    {
        $module = 'Appointment Detail';
        $doctor = User::find($id);
        $data = Appointment::query();
        $data->where('user_id', $id);
        $data = $data->paginate(100);
        $rank = $data->firstItem();
        return view('admins.front.appointments.appointment-list', get_defined_vars());
    }
    
    public function appointment_sold()
    {
        $module = 'Sold Appointments ';
        $data = Appointment::query();
        $data->whereHas('order');
        $data = $data->paginate(100);
        $rank = $data->firstItem();
        return view('admins.front.appointments.appointment-sold', get_defined_vars());
    }
    
    public function prescriptions()
    {
        $module = 'Prescription';
        $data = Prescription::query();
        $data = $data->paginate(100);
        $rank = $data->firstItem();
        return view('admins.front.prescriptions.index', get_defined_vars());
    }
    
    public function earnings()
    {
        $module = 'Earnings ';
        $data = Order::query();
        $data = $data->paginate(100);
        $rank = $data->firstItem();
        return view('admins.front.appointments.earnings', get_defined_vars());
    }
}
