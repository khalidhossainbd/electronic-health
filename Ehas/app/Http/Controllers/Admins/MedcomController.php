<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Medcompany;
use Redirect,Response;

class MedcomController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $module = "Medical Company";
    
    public function __construct()
    {
        $this->middleware('auth:admin');
        view()->share('module', 'Medical Company');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Medcompany::query();
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.medcoms.index', compact('data','rank'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admins.medcoms.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'logo'=>['required', 'mimes:jpeg,jpg,bmp,png'],
            'details'=> ['required', 'string', 'max:255'],
        ]);
        
        $feature_image_file = $request->file('logo');
        $feature_image_link = "";
        if(!empty($feature_image_file)){
            $path = 'uploads/medcoms/';
            $feature_image_link = uploadImage($feature_image_file, $path, [600, 600]);
        }
        

        $medcom = new Medcompany();
        $medcom->name = $request->name;
        $medcom->details = $request->details;
        $medcom->status = $request->status == 'on' ? 1 : 0;
        $medcom->logo = $feature_image_link;
        
        if($medcom->save()){
            return Redirect::route('medcoms.index')->withSuccess('Great! New Medcal Company added successfully.');
        }
        
        return Redirect::route('medcoms.index')->withError('Sorry! New Medcal Company add Failed.');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Medcompany::findOrFail($id);
        return view('admins.medcoms.edit', compact('data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'details'=> ['required', 'string', 'max:255'],
        ]);
        
        $medcom = Medcompany::findOrFail($id);
        $feature_image_link = $medcom->logo;
        $feature_image_file = $request->file('logo');
        if(!empty($feature_image_file)){
            $path = 'uploads/medcoms/';
            $feature_image_link = uploadImage($feature_image_file, $path,  [800, 600], $medcom->logo);
        }
        
        
        
        $medcom->name = $request->name;
        $medcom->details = $request->details;
        $medcom->status = $request->status == 'on' ? 1 : 0;
        $medcom->logo = $feature_image_link;
        
        if($medcom->save()){
            return Redirect::route('medcoms.index')->withSuccess('Great! Medcal Company updated successfully.');
        }
        
        return Redirect::route('medcoms.index')->withError('Sorry! Medcal Company updated Failed.');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Medcompany::destroy($id);
        return Redirect::route('medcoms.index')->withSuccess('Great! Medcal Company deleted successfully.');
    }
    

}
