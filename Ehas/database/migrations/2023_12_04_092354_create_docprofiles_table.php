<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('docprofiles', function (Blueprint $table) {
            $table->id();
            $table->string('home_phone')->nullable();
            $table->string('gender')->nullable();
            $table->string('nid')->nullable();
            $table->text('address')->nullable(); 
            $table->string('area')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('time_zone')->nullable();
            $table->string('social_media')->nullable();
            $table->string('video_url')->nullable();
            $table->text('shot_bio')->nullable();
            $table->integer('price')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('docprofiles');
    }
};
