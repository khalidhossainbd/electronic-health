@extends('layouts.back-app')

@section('content')

	<style>
        .pagination {
            justify-content: center !important;
        }    
    </style>
    
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">{{ $module }}</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">{{ $module }}</a></li>
                            <li class="breadcrumb-item"><a href="#!">List</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


	<div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                   <div class="d-flex">
                		<div class="col">
                			 <h5>{{$module}} List of Doctor {{ $doctor->f_name . ' ' . $doctor->l_name }}</h5> 
                		</div>
                		<div class="col text-right">
                			<a class="btn btn-sm btn-primary" href="#"> <i class="feather icon-plus-square"></i> Add New</a>
                		</div>
                	</div>
                	

                </div>
                <div class="card-body">
                
           			<div class="row">
                        <div class="col-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        
                            {{-- <li class="breadcrumb-item active">Dashboard</li> --}}
                            @if ($message = Session::get('success'))
                               <div class="alert alert-success alert-block">
                                  <button type="button" class="close" data-dismiss="alert">×</button>
                                  <strong>{{ $message }}</strong>
                               </div>
                               <br>
                            @endif
                        </div>
                    </div>   
                                 
                	<table id="medcoms-table" class="table table-condensed">
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Appointment ID</th>
                            <th>Date</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Duration</th>
                            <th>Sell Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@if(isset($data) && count($data))
                       	 	@foreach($data as $item)
                        	<tr>
                        		<td>{{ $loop->iteration }}</td>
                        		<td>
                        			<p>{{ $item->appointmentID   }} </p>
                        		</td>
                        		<td>
                        			<p>{{ $item->date }} </p>
                        		</td>
                        		<td>
                        			<p>{!! $item->start_time !!} </p>
                        		</td>
                        		<td>
                        			<p>{!! $item->end_time !!} </p>
                        		</td>
                        		<td>
                        			<p>{!! $item->duration !!} </p>
                        		</td>
                        		<td>
                        			<p>{!! $item->order ? '<span class="badge badge-success">Sold</span>' :'<span class="badge badge-danger">Unsold</span>' !!} </p>
                        		</td>
                        	</tr>
                        	@endforeach
                    	@endif	
                    </tbody>
                	</table>
                    <div class="row justify-content-md-center">
                        <div class="col-6 col-sm-6">
        					{{ $data->appends(request()->query())->links() }}
                        </div>
                    </div>            
                
                </div>
            </div>
        </div>
    </div>
@endsection

@section('java_script')
	<script>
		$(function(){
			$(document).on('click', '.cancel-pop', function(e){
				e.preventDefault();
				$("[data-toggle='popover']").popover('hide');
			});
		});
	</script>
@endsection
