<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="{{ route('home') }}">
            <img class="img-fluid" src="{{ asset('front/dist/images/ehas.png') }}" alt="Logo EHAS" style="max-height: 55px;">
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
               {{--  <li class="active has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="#">Dashboard </a>
                        </li>

                    </ul>
                </li> --}}
                <li class="active">
                    <a href="{{ route('user.dashboard') }}">
                        <i class="fas fa-tachometer-alt"></i>Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{ route('user.profile') }}">
                        <i class="fas fa-chart-bar"></i>Profile
                    </a>
                </li>
                <li>
                    <a href="{{ route('user.medical_history') }}">
                        <i class="fas fa-table"></i>Medical History</a>
                </li>
                <li>
                    <a href="{{ route('user.health_prediction') }}">
                        <i class="fas fa-table"></i>Health Prediction</a>
                </li>
                <li>
                    <a href="{{ route('user.appointment.list') }}">
                        <i class="far fa-check-square"></i>Appointments</a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-calendar-alt"></i>Medical Test Record</a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-copy"></i>Presciptions
                    </a>
                </li>
                
                <li>
                    <a href="#">
                        <i class="fas fa-desktop"></i>Payments
                    </a>
                </li>
                {{-- <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-copy"></i>Pages</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="#">Login</a>
                        </li>
                        <li>
                            <a href="#">Register</a>
                        </li>
                        <li>
                            <a href="#">Forget Password</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-desktop"></i>UI Elements</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="#">Button</a>
                        </li>
                        <li>
                            <a href="#">Badges</a>
                        </li>
                        <li>
                            <a href="#">Tabs</a>
                        </li>
                        <li>
                            <a href="#">Cards</a>
                        </li>
                        <li>
                            <a href="#">Alerts</a>
                        </li>
                        <li>
                            <a href="#">Progress Bars</a>
                        </li>
                        <li>
                            <a href="#">Modals</a>
                        </li>
                        <li>
                            <a href="#">Switchs</a>
                        </li>
                        <li>
                            <a href="#">Grids</a>
                        </li>
                        <li>
                            <a href="#">Fontawesome Icon</a>
                        </li>
                        <li>
                            <a href="#">Typography</a>
                        </li>
                    </ul>
                </li> --}}
            </ul>
        </nav>
    </div>
</aside>
