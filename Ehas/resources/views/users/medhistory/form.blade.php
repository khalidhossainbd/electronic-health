							@php
								$routeParam = ['page'=>'create' ];
								if(null !== ($request->has('page')) && ($request->filled('page'))){
									$routeParam['page'] = $request->page;
								}
								if(null !== ($request->has('id')) && ($request->filled('id'))){
									$routeParam['id'] = $request->id;
								}
							@endphp
							<form action="{{ route( 'user.medical_history', $routeParam) }}" method="post" enctype="multipart/form-data">
							@csrf
    							<div class="row">
    								<div class="col-md-12">
        								<div class="table-data__tool">
                                            <div class="table-data__tool-left">
                                            	<strong>Medical History Create</strong>  
                                            </div>
                                            <div class="table-data__tool-right">
                                                <a class="au-btn au-btn-icon au-btn--green au-btn--small" href="{{ route('user.medical_history') }}"><i class="zmdi zmdi-view-list-alt"></i>see list</a>
                                            </div>
                                    	</div>
                                	</div>
    							</div>
    							<div class="row">
    								<div class="col-6">
        								<div class="form-group">
                							<label for="home_phone" class=" form-control-label">Title</label> 
                							<input type="text" id="title" name="title" placeholder="Title" class="form-control" value="{{ val('title', $prehistory) }}"> 
                						</div>
        							</div>
        							<div class="col-6">
        								<div class="form-group">
                							<label for="date" class="form-control-label">Date</label> 
                							<input type="date" id="date" name="date" placeholder="date" class="form-control" value="{{ val('date', $prehistory) }}"> 
                						</div>
        							</div>
        						</div>
        						<div class="row">
        							<div class="col-6">
        								<div class="form-group">
                							<label for="details" class=" form-control-label">Details</label> 
                							<textarea name="details" id="details" rows="4" placeholder="Details..." class="form-control">{{ val('details', $prehistory) }}</textarea> 
                						</div>
        							</div>   
    								<div class="col-6">
        								<div class="form-group">
        									<div class="w-50 view-feature-image">
                                             <label for="status">View Report</label></br>
                                             <img src="{{ $prehistory->count() ? $prehistory->getFeatureImage(50, true) : '' }}" id="viewFeatureImage" />
                                           </div>
        								
                                          <label for="status">Report</label>
                                          <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text">Upload</span>
                                           </div>
                                           <div class="custom-file">
                                              <input type="file" name="report" class="custom-file-input" id="featureImage">
                                              <label class="custom-file-label" for="featureImage">Choose file</label>
                                           </div>
                                        </div>
                                    
                                            
                                       </div>
        							</div>    						
        						</div>
    							<div class="row">
            						<div class="col-4 mx-0 px-1">
        								<button type="reset" class="btn btn-danger btn-sm  w-100">
            								<i class="fa fa-ban"></i> Reset
            							</button>
            						</div>
            						<div class="col-8 mx-0 px-1">
            							<button type="submit" class="btn btn-primary btn-sm w-100">
                    						<i class="fa fa-dot-circle-o"></i> Submit
                    					</button>
            						</div>
        						</div>
    						</form>