from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, FloatField, RadioField, SubmitField
from wtforms.validators import DataRequired, Length, AnyOf, NumberRange

class FeaturesValidate(FlaskForm):
    age = IntegerField('age', validators=[DataRequired(), NumberRange(min=0, max=150)])
    sex = IntegerField('sex', validators=[DataRequired(), NumberRange(min=0, max=1)])
    sex = RadioField('Gender', choices=[(0, 'Male'), (1, 'Female')], validators=[DataRequired(), AnyOf([0, 1])])
    cp = IntegerField('cp', validators=[DataRequired(), NumberRange(min=0, max=3)])
    oldpeak = FloatField('oldpeak', validators=[DataRequired(), NumberRange(min=0, max=10)])
    thalachh = IntegerField('thalachh', validators=[DataRequired(), NumberRange(min=0, max=320)])
    thall = IntegerField('thall', validators=[DataRequired(), NumberRange(min=0, max=3)])
    caa = IntegerField('caa', validators=[DataRequired(), NumberRange(min=0, max=4)])

    submit = SubmitField('Submit')
