<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Apdate;
use App\Models\Appointment;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.index');
    }

    public function contact()
    {
        return view('pages.others.contact');
    }

    public function about()
    {
        return view('pages.others.about');
    }
    
    public function consultants()
    {
        $data['doctors'] = User::where('role', 'doctor')->get();
        return view('pages.dynamic.consultant', $data);
    }
    
    public function consultant_profile(Request $request, $id)
    {
        
        $data['doctor'] = User::where('userID', $id)->first(); 
        $data['appointment_data'] = Apdate::where('user_id', $data['doctor']->id)->whereDate('date', '>=', date('Y-m-d'))->get();
        
        $data['appointments'] = Appointment::where('user_id', $data['doctor']->id)->whereDate('date', '=', date('Y-m-d'))->get();
        if($request->has('date')){
            $data['appointments'] = Appointment::where('user_id', $data['doctor']->id)->whereDate('date', '=', $request->date)->get();
        }

        return view('pages.dynamic.consultant-profile', $data);
    }
    
    public function liveChat()
    {
        return view('pages.dynamic.chatbot');
    }

    public function service()
    {
        return view('pages.others.service');
    }
    
    
    //static pages
    public function privary_policy()
    {
        return view('pages.static.privacy_policy');
    }
    
    public function terms_of_use()
    {
        return view('pages.static.terms_of_use');
    }
    
    public function support()
    {
        return view('pages.static.support');
    }

}
