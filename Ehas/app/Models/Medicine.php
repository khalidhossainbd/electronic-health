<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Medicine extends Model
{
    use HasFactory;


    protected $table = 'medicines';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

        /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'org_name', 'com_name', 'age_limit', 'details', 'status', 'medcompany_id', 'medvariant_id'
    ];
    
    public function company()
    {
        return $this->belongsTo(Medcompany::class, 'medcompany_id');
    }
    
    public function variant()
    {
        return $this->belongsTo(Medvariant::class, 'medvariant_id');
    }
    
}
