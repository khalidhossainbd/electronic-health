from flask import Flask
from config import Config
from flask_wtf.csrf import CSRFProtect

from app.api.views.resource_views import predict_bp

def create_app():
  app = Flask(__name__)
  app.config.from_object(Config)
  # csrf = CSRFProtect(app)
  
  # Exclude CSRF protection for the /api endpoint
  # csrf.exempt(predict_bp)

  # Register blueprints
  app.register_blueprint(predict_bp, url_prefix='/api')

  return app