@extends('layouts.doctor-app')

@section('title')
    Doctor Dashboard
@endsection

@section('content')

    <!-- BREADCRUMB-->
    <section class="au-breadcrumb m-t-75">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="au-breadcrumb-content">
                            <div class="au-breadcrumb-left">
                                <h4 class="name">Welcome to EHAS </h4>
                                
                            </div>
                            <a href="#" class="au-btn au-btn-icon au-btn--green">
                                View prescription
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->

    <!-- STATISTIC-->
    <section class="statistic">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 bg-white p-5">
                        <ul class="nav nav-tabs justify-content-end nav-pills nav-fill border-0 mb-3" id="myTab" role="tablist">
                          <li class="nav-item mx-1">
                            <a class="nav-link active border border-primary" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Patient</a>
                          </li>
                          <li class="nav-item mx-1">
                            <a class="nav-link border border-primary" id="assesment-tab" data-toggle="tab" href="#assesment" role="tab" aria-controls="assesment" aria-selected="false">Assesment</a>
                          </li>
                          <li class="nav-item mx-1">
                            <a class="nav-link border border-primary" id="observation-tab" data-toggle="tab" href="#observation" role="tab" aria-controls="observation" aria-selected="false">Observation</a>
                          </li>
                          <li class="nav-item mx-1">
                            <a class="nav-link border border-primary" id="medical-tab" data-toggle="tab" href="#medical" role="tab" aria-controls="medical" aria-selected="false">Medical Test</a>
                          </li>
                          <li class="nav-item mx-1">
                            <a class="nav-link border border-primary" id="medicine-tab" data-toggle="tab" href="#medicine" role="tab" aria-controls="medicine" aria-selected="false">Medicine</a>
                          </li>
                        </ul>
                        <div class="tab-content border" id="myTabContent">
                          <div class="tab-pane fade show active p-3" id="home" role="tabpanel" aria-labelledby="home-tab">
                            @include('doctors.prescriptions.patient')
                          </div>
                          <div class="tab-pane fade p-3" id="assesment" role="tabpanel" aria-labelledby="assesment-tab">
                            @include('doctors.prescriptions.assesment')
                          </div>
                          <div class="tab-pane fade p-3" id="observation" role="tabpanel" aria-labelledby="observation-tab">
                            @include('doctors.prescriptions.observation')
                          </div>
                          <div class="tab-pane fade p-3" id="medical" role="tabpanel" aria-labelledby="medical-tab">
                            @include('doctors.prescriptions.medical_test')
                          </div>
                          <div class="tab-pane fade p-3" id="medicine" role="tabpanel" aria-labelledby="medicine-tab">
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium minus possimus sint inventore dicta quasi quas quidem enim modi reiciendis cumque tenetur at, veritatis atque quaerat illo. Ratione, eligendi, minima.
                            </p>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END STATISTIC-->



@endsection
