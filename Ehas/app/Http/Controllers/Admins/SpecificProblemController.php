<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SpecificProblem;
use Redirect,Response;
use App\Models\GeneralProblem;

class SpecificProblemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        view()->share('module', 'specific Problem');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = SpecificProblem::query();
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.health.specific.index', compact('data','rank'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data = collect();
        $general_problems = GeneralProblem::where('status', 1)->get();
        return view('admins.health.specific.form', get_defined_vars());
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
        ]);
        
        $data = new SpecificProblem();
        $data->title = $request->title;
        $data->general_problem_id = $request->general_problem_id;
        $data->content = $request->content;
        $data->status = $request->status == 'on' ? 1 : 0;
        
        if($data->save()){
            return Redirect::route('specific.index')->withSuccess('Great! New specific Problem added successfully.');
        }
        
        return Redirect::route('specific.index')->withError('Sorry! New specific Problem add Failed.');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = SpecificProblem::findOrFail($id);
        $general_problems = GeneralProblem::where('status', 1)->get();
        return view('admins.health.specific.form', get_defined_vars());
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
        ]);
        
        $data = SpecificProblem::findOrFail($id);
        
        $data->title = $request->title;
        $data->general_problem_id = $request->general_problem_id;
        $data->content = $request->content;
        $data->status = $request->status == 'on' ? 1 : 0;
        
        if($data->save()){
            return Redirect::route('specific.index')->withSuccess('Great! New specific Problem updated successfully.');
        }
        
        return Redirect::route('specific.index')->withError('Sorry! New specific Problem updated Failed.');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        SpecificProblem::destroy($id);
        return Redirect::route('specific.index')->withSuccess('Great! specific Problem deleted successfully.');
    }
    

}
