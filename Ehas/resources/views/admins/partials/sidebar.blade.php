	<!-- [ navigation menu ] start -->
	<nav class="pcoded-navbar">
		<div class="navbar-wrapper ">
			<div class="navbar-content scroll-div">
				
				<div class="">
					<div class="main-menu-header">
						<img class="img-radius" src="{{ asset('assets/images/user/avatar-2.jpg') }}" alt="User-Profile-Image">
						<div class="user-details">
							<span>{{ Auth::user()->name }}</span>
							{{-- <div id="more-details">UX Designer<i class="fa fa-chevron-down m-l-5"></i></div> --}}
						</div>
					</div>
					{{-- <div class="collapse" id="nav-user-link">
						<ul class="list-unstyled">
							<li class="list-group-item"><a href="#"><i class="feather icon-user m-r-5"></i>View Profile</a></li>
							<li class="list-group-item"><a href="#!"><i class="feather icon-settings m-r-5"></i>Settings</a></li>
							<li class="list-group-item"><a href="#"><i class="feather icon-log-out m-r-5"></i>Logout</a></li>
						</ul>
					</div> --}}
				</div>
				
				<ul class="nav pcoded-inner-navbar ">
					<li class="nav-item pcoded-menu-caption">
						<label>Navigation</label>
					</li>
					<li class="nav-item">
					    <a href="{{ route('admin.dashboard') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
					</li>
					<li class="nav-item pcoded-hasmenu">
					    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Admin User Setup</span></a>
					    <ul class="pcoded-submenu">
					        <li><a href="{{ route('admin.admin_list') }}">Admin User List</a></li>
					        <li><a href="{{ route('admin.admin_create') }}">Add Admin User</a></li>
					    </ul>
					</li>
					<li class="nav-item pcoded-hasmenu">
					    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Application User Setup</span></a>
					    <ul class="pcoded-submenu">
					        <li><a href="{{ route('admin.doctor_list') }}">Doctor List</a></li>
					        <li><a href="{{ route('admin.patient_list') }}">Patients List</a></li>
					        <li><a href="{{ route('admin.support_list') }}">Support User List</a></li>
					    </ul>
					</li>
					<li class="nav-item pcoded-menu-caption">
						<label>UI Element</label>
					</li>
					<li class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">Application Setup</span></a>
						<ul class="pcoded-submenu">							
							<li><a href="{{ route('admin.appointments') }}">Appointment List</a></li>
							<li><a href="{{ route('admin.appointments_sold') }}">Sold Appointment</a></li>
							<li><a href="{{ route('admin.prescriptions') }}">Prescription List</a></li>
							<li><a href="{{ route('admin.earnings') }}">Earning Details</a></li>							
						</ul>
					</li>
					
					<li class="nav-item pcoded-menu-caption">
					    <label>Medical Informations</label>
					</li>
					
					<li class="nav-item pcoded-hasmenu">
					    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Medical Tests</span></a>
					    <ul class="pcoded-submenu">
					    	<li><a href="{{ route('categories.index') }}">Category</a></li>
					        <li><a href="{{ route('medtests.index') }}">Test</a></li>
					    </ul>
					</li>
					
					<li class="nav-item pcoded-hasmenu">
					    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Medicine</span></a>
					    <ul class="pcoded-submenu">
					    	<li><a href="{{ route('medcoms.index') }}">Medical Company</a></li>
					    	<li><a href="{{ route('medvars.index') }}">Variants</a></li>
					        <li><a href="{{ route('meds.index') }}">Medicine</a></li>
					    </ul>
					</li>

					<li class="nav-item pcoded-hasmenu">
					    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Health Specification</span></a>
					    <ul class="pcoded-submenu">
					    	<li><a href="{{ route('general.index') }}">General Problem</a></li>
					        <li><a href="{{ route('specific.index') }}">Specific Problem</a></li>
					    </ul>
					</li>
					
					<li class="nav-item"><a href="sample-page.html" class="nav-link "><span class="pcoded-micon"><i class="feather icon-sidebar"></i></span><span class="pcoded-mtext">Reports</span></a></li>
					
					{{-- <li class="nav-item pcoded-menu-caption">
					    <label>Stock &amp; Purches</label>
					</li>
					<li class="nav-item">
					    <a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Stock Setup</span></a>
					</li>
					<li class="nav-item">
					    <a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-align-justify"></i></span><span class="pcoded-mtext">Orders Setup</span></a>
					</li>
					<li class="nav-item pcoded-menu-caption">
						<label>Chart & Maps</label>
					</li>
					<li class="nav-item">
					    <a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-pie-chart"></i></span><span class="pcoded-mtext">Chart</span></a>
					</li>
					<li class="nav-item">
					    <a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-map"></i></span><span class="pcoded-mtext">Maps</span></a>
					</li>
					<li class="nav-item pcoded-menu-caption">
						<label>Pages</label>
					</li>
					<li class="nav-item pcoded-hasmenu">
					    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-lock"></i></span><span class="pcoded-mtext">Authentication</span></a>
					    <ul class="pcoded-submenu">
					        <li><a href="auth-signup.html" target="_blank">Sign up</a></li>
					        <li><a href="auth-signin.html" target="_blank">Sign in</a></li>
					    </ul>
					</li> --}}
					

				</ul>
			</div>
		</div>
	</nav>
	<!-- [ navigation menu ] end -->