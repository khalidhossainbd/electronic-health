<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prehistory extends Model
{
    use HasFactory;

    protected $table = 'prehistories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

        /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'date', 'details', 'report', 'user_id'
    ];
    
    public static $uploadlocation = 'uploads/medreports/';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    
    public function getFeatureImage($size = 50, $link = false)
    {
        $uploadLocation = self::$uploadlocation;
        $imageLink = asset('assets/images/default.png');
        if (file_exists( public_path() . '/'. $uploadLocation . $this->report)) {
            $imageLink = asset($uploadLocation .  $this->report);
        }
        if($link){
            return $imageLink;
        }
        return '<img style="max-height: '.$size.'px;" class="img-responsive" id="viewFeatureImage" src="'.$imageLink.'" />';
    }
    
}
