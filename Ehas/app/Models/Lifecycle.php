<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lifecycle extends Model
{
    use HasFactory;


    protected $table = 'lifecycles';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'age',    'gender',    'cp', 'thalachh', 'oldpeak', 'troponin', 'thall', 'caa',
        'pressurehight',    'pressurelow',    'glucose',  'weight', 'date',  'user_id', 'ha_prediction'
    ];
}
