						<div class="row">
							<div class="col-md-12">

                            <div class="table-data__tool">
                                <div class="table-data__tool-left">
                                	<strong>Health Prdiction List</strong>  
                                </div>
                                <div class="table-data__tool-right">
                                    <a class="au-btn au-btn-icon au-btn--green au-btn--small" href="{{ route('user.health_prediction', ['page'=>'create']) }}">Add Information</a>
                                </div>
                            </div>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2 table-bordered">
                                    <thead>
                                        <tr>
                                        	{{-- <th>Sl.</th> --}}
                                            <th>Date</th>
                                            <th>Age</th>
                                            <th>Gender</th>
                                            <th>Weight</th>
                                            <th>#</th>
                                            <th>#</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@if(null !== ($lifecycles) && isset($lifecycles) && !empty($lifecycles))
                                    	@foreach($lifecycles as $item)
                                        <tr class="tr-shadow">
                                            {{-- <td>{{$loop->iteration}} </td> --}}
                                            <td>{{ $item->date }} </td>
                                            <td>{{ $item->age }} </td>
                                            <td>{{ $item->gender && $item->gender == 1 ?  'Male' : 'Female' }} </td>
                                            <td>{{ $item->weight }}</td>
                                            <td class="desc">
                                            	<p class="p-0 m-0">Impulse: {{ $item->impluse }}</p>
                                            	<p class="p-0 m-0">Pressurehight: {{ $item->pressurehight }}</p>
                                            	<p class="p-0 m-0">Pressurelow: {{ $item->pressurelow }}</p>
                                            </td>
                                            <td class="desc">
                                            	<p class="p-0 m-0">Glucose: {{ $item->glucose }}</p>
                                            	<p class="p-0 m-0">KCM: {{ $item->kcm }}</p>
                                            	<p class="p-0 m-0">Troponin: {{ $item->troponin }}</p>
                                            </td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <a href="{{ route( 'user.health_prediction', ['page'=>'edit', 'id' => $item->id ]) }}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </a>
                                                    <a href="{{ route( 'user.health_prediction', ['page'=>'delete', 'id' => $item->id ]) }}" onclick="return confirm('Are you sure?');" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>