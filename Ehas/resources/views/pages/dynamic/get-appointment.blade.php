@extends('layouts.main')

@section('title')
    Electronic Health Appointment System
@endsection

@section('content')
	@include('pages.partials.pagetitle', ['title' => $doctor->f_name])
	
<section>
	<div class="my-5">
		<div class="container">
						
			<div class="row">				
				<div class="col-12 col-sm-4">
					<h3>Welcome to EHAS</h3>
					<p>
						You wish to get appointment of doctor <br>
						{{ $doctor->f_name }} {{ $doctor->l_name }} <br>
						{{ $doctor->designation }}
					</p>
					<table class="table table-bordered w-100">
						<tbody>
							<tr>
								<td>Appointment date : {{ $myappointment->date }}</td>
							</tr>
							<tr>
								<td>Appointment time : {{ $myappointment->start_time }}</td>
							</tr>
							<tr>
								<td>Price : {{ @$doctor->docprofile->price }}</td>
							</tr>
						</tbody>
					</table>
				</div>
				

				<div class="col-12 col-sm-8">
					<form action="{{ route('appointment.post', $myappointment->appointmentID) }}" method="post">
						@csrf
						<input type="hidden" name="amount" value="{{ @$doctor->docprofile->price }}" />
						<div class="row">
							<div class="form-group col-12">
							    <label for="">Name</label>
							    <input type="text" class="form-control" name="name" id="" placeholder="Full Name" required>
							</div>
							<div class="form-group col-12">
							    <label for="">Card Number</label>
							    <input type="text" class="form-control" name="card_number" id="" placeholder="Card Number" required>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
								    <label for="">CVC</label>
								    <input type="text" class="form-control" name="cvc" id="" placeholder="cvc" required>
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
								    <label for="">EXP month</label>
								    <input type="text" class="form-control" name="expiration_month" id="" placeholder="Ful Name" required>
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
								    <label for="">Exp Year</label>
								    <input type="text" class="form-control" name="expiration_year" id="" placeholder="Example: 2000" required>
								</div>
							</div>
							<div class="col-12">
								<button class="btn btn-success" type="submit">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection