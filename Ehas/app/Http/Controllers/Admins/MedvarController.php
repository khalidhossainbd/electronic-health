<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Medvariant;
use Redirect,Response;

class MedvarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        view()->share('module', 'Medicine Variant');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Medvariant::query();
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.meds.medvars.index', compact('data','rank'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admins.meds.medvars.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
        ]);
        
        
        $medvar = new Medvariant();
        $medvar->title = $request->title;
        $medvar->details = $request->details;
        $medvar->status = $request->status == 'on' ? 1 : 0;
        
        if($medvar->save()){
            return Redirect::route('medvars.index')->withSuccess('Great! New Medicine Variant added successfully.');
        }
        
        return Redirect::route('medvars.index')->withError('Sorry! New Medicine Variant add Failed.');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Medvariant::findOrFail($id);
        return view('admins.meds.medvars.edit', compact('data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
        ]);
        
        $medvar = Medvariant::findOrFail($id);
        
        $medvar->title = $request->title;
        $medvar->details = $request->details;
        $medvar->status = $request->status == 'on' ? 1 : 0;
        
        if($medvar->save()){
            return Redirect::route('medvars.index')->withSuccess('Great! New Medicine Variant updated successfully.');
        }
        
        return Redirect::route('medvars.index')->withError('Sorry! New Medicine Variant updated Failed.');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Medvariant::destroy($id);
        return Redirect::route('medvars.index')->withSuccess('Great! Medicine Variant deleted successfully.');
    }
    

}
