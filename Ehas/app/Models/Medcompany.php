<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medcompany extends Model
{
    use HasFactory;


    protected $table = 'medcompanies';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

        /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'logo', 'details', 'status'
    ];
    
    
    public function getFeatureImage($size = 50, $link = false)
    {
        $imageLink = asset('assets/images/default.png');
        if (file_exists( public_path() . '/uploads/medcoms/' . $this->logo)) {
            $imageLink = asset('/uploads/medcoms/' . $this->logo);
        } 
        if($link){return $imageLink;}
        return '<img style="max-height: '.$size.'px;" class="img-responsive w-100" id="viewFeatureImage" src="'.$imageLink.'" />';
    }
    
    
    
    
    
}
