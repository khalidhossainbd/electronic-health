<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Medtest;
use Redirect,Response;
use App\Models\Testcategory;

class MedestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        view()->share('module', 'Medical Test');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Medtest::query();
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.medtests.index', compact('data','rank'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Testcategory::where('status', 1)->get();
        return view('admins.medtests.create', ['categories' => $categories]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'details' => ['required', 'string', 'max:255'],
            'testcategory_id' => ['required', 'string', 'max:255'],
        ]);
        
        
        $test = new Medtest();
        $test->title = $request->title;
        $test->details = $request->details;
        $test->testcategory_id = $request->testcategory_id;
        $test->status = $request->status == 'on' ? 1 : 0;
        
        if($test->save()){
            return Redirect::route('medtests.index')->withSuccess('Great! New Medical Test added successfully.');
        }
        
        return Redirect::route('medtests.index')->withError('Sorry! Medical Test add Failed.');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Medtest::findOrFail($id);
        $categories = Testcategory::where('status', 1)->get();
        return view('admins.medtests.edit', compact('data', 'categories'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'details' => ['required', 'string', 'max:255'],
            'testcategory_id' => ['required', 'string', 'max:255']
        ]);
        
        $test = Medtest::findOrFail($id);
        $test->title = $request->title;
        $test->details = $request->details;
        $test->testcategory_id = $request->testcategory_id;
        $test->status = $request->status == 'on' ? 1 : 0;
        
        if($test->save()){
            return Redirect::route('medtests.index')->withSuccess('Great! Medical Test updated successfully.');
        }
        
        return Redirect::route('medtests.index')->withError('Sorry! Medical Test updated Failed.');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Medtest::destroy($id);
        return Redirect::route('medtests.index')->withSuccess('Great!  Medical Test deleted successfully.');
    }
    

}
