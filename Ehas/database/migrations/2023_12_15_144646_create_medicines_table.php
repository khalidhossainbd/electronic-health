<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('medicines', function (Blueprint $table) {
            $table->id();
            $table->string('org_name')->nullable();
            $table->string('com_name')->nullable();
            $table->integer('age_limit')->nullable();
            $table->text('details')->nullable();
            $table->integer('status')->default('1')->nullable();
            $table->unsignedBigInteger('medcompany_id')->nullable();
            $table->unsignedBigInteger('medvariant_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('medicines');
    }
};
