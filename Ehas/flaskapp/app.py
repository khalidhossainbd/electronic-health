# import os, joblib
# import numpy as np
# from flask import Flask, request, jsonify
# from werkzeug.exceptions import abort

# app = Flask(__name__)
# app.config['SECRET_KEY'] = 'secret_key'
# # Define the path to the models folder
# models_folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'ai_models')

# def load_model():
#     model_path = os.path.join(models_folder, 'heartattack_rf92.joblib')
#     loaded_rf = joblib.load(model_path)
#     return loaded_rf

# @app.route('/')
# def index():
#     return jsonify({'data': 'Hello flask!'}) 

# @app.route('/predict', methods=('GET', 'POST'))
# def predict():
#     model = load_model()
#     if request.method == 'POST':
#         # return '99'
#         age = request.form['age']
#         sex = request.form['sex']
#         cp = request.form['cp']
#         oldpeak = request.form['oldpeak']
#         thalachh = request.form['thalachh']
#         thall = request.form['thall']
#         caa = request.form['caa']

#         return 'eee444'

#         model_features = {'age': age, 'sex': sex, 'cp': cp, 'trtbps': 120, 'chol': 157, 'fbs': 0,
#                     'restecg': 0, 'thalachh': thalachh, 'exng':0, 'oldpeak': oldpeak, 'slp': 2,
#                     'caa': caa, 'thall': thall}
        
#         model_features_scaled = np.array([list(model_features.values())])

#         # Make predictions
#         prediction = model.predict_proba(model_features_scaled)
#         print(prediction)
#         response = jsonify(prediction.tolist())
#         return response 
#     else:
#         # result - 0
#         """ new_data = {'age': 67, 'sex': 1, 'cp': 0, 'trtbps': 160, 'chol': 286, 'fbs': 0,
#                     'restecg': 0, 'thalachh': 108, 'exng':1, 'oldpeak': 1.5, 'slp': 1,
#                     'caa':3, 'thall': 2} """

#         # result - 1
#         new_data = {'age': 41, 'sex': 1, 'cp': 1, 'trtbps': 120, 'chol': 157, 'fbs': 0,
#                     'restecg': 0, 'thalachh': 182, 'exng':0, 'oldpeak': 0, 'slp': 2,
#                     'caa':0, 'thall': 2}

#         new_data_scaled = np.array([list(new_data.values())])

#         # Make predictions
#         prediction = model.predict_proba(new_data_scaled)
#         print(prediction)
#         response = jsonify(prediction.tolist())
#         return response 

# if __name__ == '__main__':
#     app.run(debug=True)