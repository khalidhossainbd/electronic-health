<style>
.modal-backdrop {
  z-index: -1;
}
</style>


@if(@$appoint)
	<div class="row py-5 border">
		<div class="col-12 col-sm-6">
			<p>Patient Name: {{ App\Helpers\Phelper::getPatientName(@$appoint->id)->f_name }}	{{ App\Helpers\Phelper::getPatientName(@$appoint->id)->l_name }}</p>
			<p>Mobile Number: {{ App\Helpers\Phelper::getPatientName(@$appoint->id)->mobile }}</p>
			<p>Email: {{ App\Helpers\Phelper::getPatientName(@$appoint->id)->email }}</p>
			{{-- <a type="button" class="au-btn au-btn-icon au-btn--blue my-2 text-white" >VIEW HISTORY</a> --}}
		</div>
		<div class="col-12 col-sm-6 border-left pl-5">
			<p>Appointment Date: {{ @$appoint->date }}</p>
			<p>Appointment Time: {{ @$appoint->start_time }}</p>
			<p>Appointment Duration: {{ @$appoint->duration }} Munites</p>
			<a href="{{ @$appoint->zoom_link }}" class="au-btn au-btn-icon au-btn--blue my-2 text-white" target="_new" onclick="window.open(this.href,'newwindow','width=800,height=600'); return false;">GO LIVE</a>
		</div>
	</div>
	<div class="row py-5 border">
		<div class="col-12 col-sm-5 border">
			<h3>Patient Medical History</h3>
			<div class="table-responsive table-responsive-data2">
                <table class="table table-data2 table-bordered">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Date</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>	
                    	@if(null !== ($appoint?->order?->patient?->prehistories))
                        	@foreach($appoint?->order?->patient?->prehistories as $item)
                            <tr class="tr-shadow">
                                <td>
                                   {{ $item->title }}
                                </td>
                                <td >{{ $item->date }}</td>
                                <td class="desc">{{ $item->details }}</td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
									
		</div>
		<div class="col-12 col-sm-7 border">
			<h3>Health Prediction</h3>
				<div class="table-responsive table-responsive-data2">
                    <table class="table table-data2 table-bordered">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>#</th>
                                <th>#</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@if(null !== ($appoint?->order?->patient?->lifecycles))
                        	@foreach($appoint?->order?->patient?->lifecycles as $item)
                            <tr class="tr-shadow">
                                <td>{{ $item->date }} </td>
                                <td>
                                	<p class="p-0 m-0">Age: {{ $item->age }}</p>
                                	<p class="p-0 m-0">Gender: {{ $item->gender && $item->gender == 1 ?  'Male' : 'Female'  }}</p>
                                	<p class="p-0 m-0">Weight: {{ $item->weight }}</p>
                                </td>
                                <td class="desc">
                                	<p class="p-0 m-0">Impulse: {{ $item->impluse }}</p>
                                	<p class="p-0 m-0">Pressurehight: {{ $item->pressurehight }}</p>
                                	<p class="p-0 m-0">Pressurelow: {{ $item->pressurelow }}</p>
                                </td>
                                <td class="desc">
                                	<p class="p-0 m-0">Glucose: {{ $item->glucose }}</p>
                                	<p class="p-0 m-0">KCM: {{ $item->kcm }}</p>
                                	<p class="p-0 m-0">Troponin: {{ $item->troponin }}</p>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
		</div>
	</div>
	
@else
<div class="row py-5">
	<div class="col-12 col-sm-6">		
		<div class="form-group">
		    <label for="exampleInputEmail1">Find Patient</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
	<div class="col-12 col-sm-6 border-left">
		<div>
			<p>Patient Name: Example Name</p>
			<p>
				Mobile Number: 07475 896896
			</p>
			<p>
				Email Address: example@mail.com
			</p>
			<button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#largeModal">VIEW HISTORY</button>
		</div>
	</div>
</div>
@endif




