   <form action="{{ route('doctor.visit', ['id' =>$id, 'tab'=>'medical']) }}" method="post">
   @csrf
<input type="hidden" name="method" value="handleMedical" />
      <div class="row my-2">
@if(isset($categories) && $categories->count())
  <div class="col-12 col-sm-3">
    <ul class="nav nav-tabs flex-column nav-pills nav-fill" id="myTab" role="tablist">
      @foreach($categories as $category)
      <li class="nav-item border border-primary">
        <a class="nav-link {{ $loop->first ? 'active' : '' }}" id="cat-{{$category->id}}-tab" data-toggle="tab" href="#cat-{{$category->id}}" role="tab" aria-controls="cat-{{$category->id}}" aria-selected="{{ $loop->first }}">{{ $category->title }}</a>
      </li>
      @endforeach
    </ul>
  </div>
  
  <div class="col-12 col-sm-9 border p-3">
    <div class="tab-content" id="myTabContent">
      @foreach($categories as $category)
      <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="cat-{{$category->id}}" role="tabpanel" aria-labelledby="cat-{{$category->id}}-tab">
        <div class="row">
        	@if(isset($category->tests))
			 @foreach($category->tests as $test)      
          	<div class="col-4">
                <div class="form-group form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck{{$test->id}}" name="tests[]" value="{{$test->id}}" @if($medtests->contains($test->id)) checked @endif>
                  <label class="form-check-label" for="exampleCheck{{$test->id}}">{{ $test->title }}</label>
                </div>
          	</div>
          @endforeach
         @endif 
        </div>
      </div>
      @endforeach
      
      
    </div>
  </div>
  @endif
</div>


      <div class="row">
		<div class="col-4 mx-0 px-1">
			<button type="reset" class="btn btn-danger btn-sm  w-100">
				<i class="fa fa-ban"></i> Reset
			</button>
		</div>
		<div class="col-8 mx-0 px-1">
			<button type="submit" class="btn btn-primary btn-sm w-100">
				<i class="fa fa-dot-circle-o"></i> Submit
			</button>
		</div>
	</div>
   
  </form>



