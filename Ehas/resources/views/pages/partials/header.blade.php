
<!-- ========== Navber Tab section Start ======== -->
<section>
    <div class="nab-top-bg">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <p class="p-0 m-0 text-white">Hotline: +44 07476 972192</p>
                </div>
                <div class="col-12 col-sm-6">
                    @guest
                    <a href="{{ route('login') }}">
                        <p class="p-0 m-0 text-white text-right"><i class="fas fa-sign-in-alt"></i> Login</p>
                    </a>
                    @else
                    <p class="p-0 m-0 text-white text-right">
                        <a class="text-white" href="{{ route('user.dashboard') }}">{{ Auth::user()->f_name }} {{ Auth::user()->l_name }}</a> | 
                        <a class="text-white" title="Logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('user-logout-form').submit();">
                            <i class="zmdi zmdi-power"></i> Logout
                        </a>

                        <form id="user-logout-form" action="{{ route('logout') }}" method="POST">
                            @csrf
                        </form>
                    </p>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <nav class="navbar navbar-expand-lg navbar-light shadow">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img class="img-fluid" src="{{ asset('front/dist/images/ehas.png') }}" alt="Logo EHAS">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" href="#">Booking Online</a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('consultants') }}">Consultants</a>
                    </li>
                    <!-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('service') }}">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('contact') }}">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link live_chat" href="{{ route('live.chat') }}">Live Chat</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</section>
<!-- ========== Navber Tab section End ======== -->