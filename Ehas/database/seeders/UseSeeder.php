<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
       DB::table('users')->insert(
         [
           'userID' => time().rand(1111111,9999999),
           'f_name' => 'Mr. Patient',
           'l_name' => 'Hossain',
           'email' => 'user@gmail.com',
           'mobile' => '07476972191',
           'role' => 'patient',
           'images' => 'dummy.png',
           'status' => 'Active',
           'password' => Hash::make('password'),
        ]);

         DB::table('users')->insert(
         [
            'userID' => time().rand(1111111,9999999),
            'f_name' => 'Mr. Doctor',
            'l_name' => 'Hossain',
            'designation' => 'Carbon',
            'email' => 'doctor@gmail.com',
            'mobile' => '07476972192',
            'role' => 'doctor',
            'images' => 'dummy.png',
            'status' => 'Active',
            'password' => Hash::make('password'),
         ]);

         DB::table('users')->insert(
         [
            'userID' => time().rand(1111111,9999999),
            'f_name' => 'Mr. Support',
            'l_name' => 'Hossain',
            'email' => 'support@gmail.com',
            'mobile' => '07476972193',
            'role' => 'support',
            'images' => 'dummy.png',
            'status' => 'Active',
            'password' => Hash::make('password'),
        ]);

    }
 }
