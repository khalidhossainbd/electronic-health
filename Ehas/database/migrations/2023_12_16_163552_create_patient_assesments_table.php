<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('patient_assesments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('prescription_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->unsignedBigInteger('patient_id')->nullable();
            $table->unsignedBigInteger('doctor_id')->nullable();
            $table->unsignedBigInteger('appointment_id')->nullable();
            $table->integer('status')->default('1')->nullable();
            $table->unsignedBigInteger('general_problem_id')->nullable();
            $table->unsignedBigInteger('specific_problem_id')->nullable();
            $table->text('detail_observation')->nullable();
            $table->text('symptoms')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('patient_assesments');
    }
};

//'prescription_id', 'created_by', 'patient_id', 'doctor_id',  'appointment_id', 'status', 'general_problem_id',
//'specific_problem_id', 'detail_observation', 'symptoms'
