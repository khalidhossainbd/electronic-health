<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dream Doctor | Online Doctor Appointment System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Phoenixcoded" />
    <!-- Favicon icon -->
    <link rel="icon" href="{{ asset('images/dark_short.png') }}" type="image/gif" sizes="16x16">
    <!-- vendor css -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/jodit/jodit.min.css') }}"/>

</head>

<body class="">

<!-- [ Pre-loader ] start -->
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Pre-loader ] End -->

@include('admins.partials.sidebar')
@include('admins.partials.header')

<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        
        <!-- [ breadcrumb ] end -->


        @yield('content')


    </div>
</div>

<!-- Required Js -->
<script src="{{ asset('assets/js/vendor-all.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/pcoded.min.js') }}"></script>

<!-- Apex Chart -->
<script src="{{ asset('assets/js/plugins/apexcharts.min.js') }}"></script>


<!-- custom-chart js -->
<script src="{{ asset('assets/js/pages/dashboard-main.js') }}"></script>
<script src="{{ asset('assets/plugins/jodit/jodit.min.js') }}"></script>

<script>
	var general = {
			status_route : "{{ route('status.change') }}",
			csrf : '{{ csrf_token() }}'
		};
</script>

<script src="{{ asset('assets/js/custom.js') }}"></script>

@yield('java_script')

</body>

</html>
