<form action="{{ route('doctor.visit', ['id' =>$id, 'tab'=>'observation']) }}" method="post" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="method" value="handleObservation" />
	<div class="row">
		<div class="col-6">
			<div class="form-group">
				<label for="age" class=" form-control-label">Age</label> 
				<input type="text" id="age" name="age" placeholder="Age" class="form-control" value="{{ val('age', $lifecycle) }}"> 
			</div>
		</div>
		<div class="col-6">
			<div class="form-group">
				<label for="nf-email" class=" form-control-label">Gender</label>
				<div class="form-check">
                    <label for="inline-radio1" class="form-check-label mx-3 position-relative">
                        <input type="radio" id="inline-radio1"  name="gender" value="1" class="form-check-input" @if( val('gender', $lifecycle)) == 1 ) checked @endif>Male
                    </label>
                    <label for="inline-radio2" class="form-check-label mx-3 position-relative">
	                    <input type="radio" id="inline-radio2" name="gender" value="2" class="form-check-input" @if( val('gender', $lifecycle)) == 2 ) checked @endif>Female
    	            </label>
                </div>
            </div>
        </div>
	</div>
	<div class="row">
		<div class="col-4">
			<div class="form-group">
				<label for="impluse" class=" form-control-label">Impulse</label> 
				<input type="text" id="impluse" name="impluse" placeholder="Impulse" class="form-control" value="{{ val('impluse', $lifecycle) }}"> 
			</div>
		</div>
		<div class="col-4">
			<div class="form-group">
				<label for="pressurehight" class="form-control-label">Pressurehight</label> 
				<input type="text" id="pressurehight" name="pressurehight" placeholder="Title" class="form-control" value="{{ val('pressurehight', $lifecycle) }}"> 
			</div>
		</div>
		<div class="col-4">
			<div class="form-group">
				<label for="pressurelow" class="form-control-label">Pressurelow</label> 
				<input type="text" id="pressurelow" name="pressurelow" placeholder="Pressurelow" class="form-control" value="{{ val('pressurelow', $lifecycle) }}"> 
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-4">
			<div class="form-group">
				<label for="glucose" class=" form-control-label">Glucose</label> 
				<input type="text" id="glucose" name="glucose" placeholder="Glucose" class="form-control" value="{{ val('glucose', $lifecycle) }}"> 
			</div>
		</div>
		<div class="col-4">
			<div class="form-group">
				<label for="kcm" class="form-control-label">KCM</label> 
				<input type="text" id="kcm" name="kcm" placeholder="KCM" class="form-control" value="{{ val('kcm', $lifecycle) }}"> 
			</div>
		</div>
		<div class="col-4">
			<div class="form-group">
				<label for="troponin" class="form-control-label">Troponin</label> 
				<input type="text" id="troponin" name="troponin" placeholder="Troponin" class="form-control" value="{{ val('troponin', $lifecycle) }}"> 
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-6">
			<div class="form-group">
				<label for="weight" class=" form-control-label">Weight</label> 
				<input type="text" id="weight" name="weight" placeholder="Weight" class="form-control" value="{{ val('weight', $lifecycle) }}"> 
			</div>
		</div>
		<div class="col-6">
			<div class="form-group">
				<label for="date" class=" form-control-label">Date</label>
				<input type="date" id="dob" name="date" placeholder="Date" class="form-control" value="{{ val('date', $lifecycle) }}"> 
            </div>
        </div>
	</div>
	<div class="row">
		<div class="col-4 mx-0 px-1">
			<button type="reset" class="btn btn-danger btn-sm  w-100">
				<i class="fa fa-ban"></i> Reset
			</button>
		</div>
		<div class="col-8 mx-0 px-1">
			<button type="submit" class="btn btn-primary btn-sm w-100">
				<i class="fa fa-dot-circle-o"></i> Submit
			</button>
		</div>
	</div>
</form>