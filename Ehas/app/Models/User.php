<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'f_name', 'l_name', 'email', 'password', 'mobile', 'role', 'status', 'images', 'code', 'active', 'userID', 'designation', 'user_id', 'verified'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function docprofile()
    {
        return $this->hasOne(Docprofile::class);
    }
    
    public function patprofile()
    {
        return $this->hasOne(Preprofile::class);
    }
    
    public function prehistories()
    {
        return $this->hasMany(Prehistory::class);
    }
    
    public function lifecycles()
    {
        return $this->hasMany(Lifecycle::class);
    }
    
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }
    
    public function orders()
    {
        return $this->hasManyThrough(Order::class, Appointment::class, 'user_id', 'appointment_id', 'id', 'appointmentID',);
    }
    
    public function fullName(){
        return $this->f_name . " " . $this->l_name;
    }
    
    
    
}
