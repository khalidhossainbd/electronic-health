<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientTestResult extends Model
{
    use HasFactory;
    
    protected $table = 'patient_test_results';
    
    protected $primaryKey = 'id';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prescription_id', 'patient_id', 'appointment_id',  'status', 'date', 'patient_med_test_id', 'result_value', 'result_details'
    ];
    
}
