<?php
namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\User;
use App\Models\Apdate;
use App\Models\Appointment;
use Illuminate\Http\Request;
use Stripe\Stripe;
use App\Http\Requests\CardVerificationRequest;
use Auth;

class AppointmentPaymentController extends Controller
{
    public function get_appointment($id)
    {
        $data['patient'] = Auth::user(); 
        $data['myappointment'] = Appointment::where('appointmentID', $id)->first();
        $data['doctor'] = User::find($data['myappointment']->user_id);
        return view('pages.dynamic.get-appointment', $data);
    }

    public function post_appointment(CardVerificationRequest $cardrequest, $appointmentID)
    {

        $payment = collect();
        $card = collect($cardrequest->validated());
        $payment->status = false;
        $payment->charge = null;
        $payment->error = null;
        $grand_total = $cardrequest->amount;
        $amount = round($grand_total * 100);
        
        // dd($card->get('card_number'));
        
        try{
            
            $stripe = new \Stripe\StripeClient(
                env('STRIPE_SECRET')
                );
            
            $token = $stripe->tokens->create([
                'card' => [
                    'number' => $card->get('card_number'),
                    'exp_month' => $card->get('expiration_month'),
                    'exp_year' => $card->get('expiration_year'),
                    'cvc' => $card->get('cvc'),
                ],
            ]);
            
            $user = json_decode("{}");
            $user->id= Auth::user()->id;
            $user->name = Auth::user()->f_name;
            $user->phone = Auth::user()->mobile;
            
            Stripe::setApiKey(env('STRIPE_SECRET'));
            
            $charge = \Stripe\Charge::create([
                'amount' => $amount,
                'currency' => 'GBP',
                'description' => "Thank you for suscribe",
                'source' => $token->id,
                'shipping' => [
                    'name' => $user->name,
                    'phone' => $user->phone,
                    'address' => [
                        'line1' => "House 100",
                        'postal_code' => "B8 2RT",
                        'city' => "Birmingham",
                        'state' => "Birmingham",
                        'country' => "UK",
                    ],
                ],
            ]);
        }
        catch (\Exception $e) {
            $payment->error = $e;
            dd($payment);
            return $payment;
        }
        
        if(isset($charge) && isset($charge->status) && $charge->status == 'succeeded'){
            $appointment = Appointment::where('appointmentID', $appointmentID)->first();
            $appointment->status = 2;
            $appointment->save();
            
            $order = Order::create([
                'orderID'=>mt_rand(100000,999999), 'name' =>$user->name, 'email'=> Auth::user()->email, 'mobile' => Auth::user()->mobile, 'amount' => $amount, 'user_id' => $user->id, 'appointment_id' => $appointmentID, 'transaction_id' => $charge->balance_transaction, 'status' => 2
            ]);

        }
        
        return redirect()->route('appointment.confirmation', $order->id);

       
    }
    
    
    public function confirmation($id)
    {
        // return "Thanks for you order You have just completed your payment. The seeler will reach out to you as soon as possible";
        return view('pages.dynamic.pay-success');
    }

}
