@extends('layouts.back-app')

@section('content')

    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">{{ $module }}</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">{{ $module }}</a></li>
                            <li class="breadcrumb-item"><a href="#!">Create</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


	<div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                   <div class="d-flex">
                		<div class="col">
                			 <h5>Create a {{$module}} </h5>
                		</div>
                		<div class="col text-right">
                			<a class="btn btn-sm btn-primary" href="{{ route('medvars.index') }}"> <i class="feather icon-list"></i>{{$module}} List</a>
                		</div>
                	</div>
                </div>
                <div class="card-body">
                
                <div class="row">
                    <div class="col-12">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        
                        {{-- <li class="breadcrumb-item active">Dashboard</li> --}}
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        <br>
                        @endif
                    </div>
                </div>
                
                    <form action="{{ route('medvars.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="floating-label" for="title">Title</label>
                                    <input type="text" name="title" class="form-control" id="title" aria-describedby="nameHelp" value="{{ old('title') }}">
                                </div>
                                
                              	<div class="form-group">
                                   <label for="short_desc">Details</label>
                                   <textarea class="form-control" name="details" id="details"  rows="3">{{ old('details') }}</textarea>
                               	</div>
                               
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input"  name="status" id="status">
                                        <label class="custom-control-label" for="status">Status</label>
                                    </div>        
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-sm-12">
                        		<button class="btn  btn-primary w-100" type="submit">Add {{$module}}</button>
                        	</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

