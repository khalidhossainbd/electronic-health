from flask import Blueprint, request, jsonify, current_app
import os, joblib
import numpy as np
from werkzeug.exceptions import abort
from app.api.forms.model_features import FeaturesValidate
from flask_wtf.csrf import CSRFProtect, CSRFError

csrf = CSRFProtect()
predict_bp = Blueprint('predict', __name__)

def load_model():
    models_folder = os.path.join(current_app.root_path, '../', 'ai_models')
    model_path = os.path.join(models_folder, 'heartattack_rf92.joblib')
    loaded_rf = joblib.load(model_path)
    return loaded_rf

@predict_bp.route('/')
def index():
    return jsonify({'data': 'Hello folks!!!'})

@predict_bp.route('/predict', methods=['POST'])
@csrf.exempt
def predict():
    form = FeaturesValidate()
    # if form.validate_on_submit():
    if 1 == 1:
      model = load_model()
      if request.method == 'POST':    
          # Get JSON data from the request body
          form_data = request.get_json()
          if form_data:
            age = form_data.get('age')
            sex = form_data.get('sex')
            cp = form_data.get('cp')
            oldpeak = form_data.get('oldpeak')
            thalachh = form_data.get('thalachh')
            thall = form_data.get('thall')
            caa = form_data.get('caa')

            model_features = {'age': age, 'sex': sex, 'cp': cp, 'trtbps': 120, 'chol': 157, 'fbs': 0,
                        'restecg': 0, 'thalachh': thalachh, 'exng':0, 'oldpeak': oldpeak, 'slp': 2,
                        'caa': caa, 'thall': thall}
            
            model_features_scaled = np.array([list(model_features.values())])

            # Make predictions
            prediction = model.predict_proba(model_features_scaled)
            print(prediction)
            response = jsonify(prediction.tolist())
            return response 
          else:
            return jsonify({'error': 'No JSON data found in the request body'}), 400
    else:
        # Handle the case where form validation fails
        return jsonify({'errors': form.errors}), 400
    