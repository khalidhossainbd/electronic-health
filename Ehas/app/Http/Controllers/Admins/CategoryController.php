<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testcategory;
use Redirect,Response;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        view()->share('module', 'Test Category');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Testcategory::query();
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.medtests.categories.index', compact('data','rank'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admins.medtests.categories.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
        ]);
        
        
        $category = new Testcategory();
        $category->title = $request->title;
        $category->status = $request->status == 'on' ? 1 : 0;
        
        if($category->save()){
            return Redirect::route('categories.index')->withSuccess('Great! New Testcategory added successfully.');
        }
        
        return Redirect::route('categories.index')->withError('Sorry! New Testcategory add Failed.');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Testcategory::findOrFail($id);
        return view('admins.medtests.categories.edit', compact('data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
        ]);
        
        $category = Testcategory::findOrFail($id);
        
        $category->title = $request->title;
        $category->status = $request->status == 'on' ? 1 : 0;
        
        if($category->save()){
            return Redirect::route('categories.index')->withSuccess('Great! New Testcategory updated successfully.');
        }
        
        return Redirect::route('categories.index')->withError('Sorry! New Testcategory updated Failed.');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Testcategory::destroy($id);
        return Redirect::route('categories.index')->withSuccess('Great! Testcategory deleted successfully.');
    }
    

}
