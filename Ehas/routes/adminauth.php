<?php

use App\Http\Controllers\Admins\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Admins\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Admins\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Admins\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Admins\Auth\NewPasswordController;
use App\Http\Controllers\Admins\Auth\PasswordController;
use App\Http\Controllers\Admins\Auth\PasswordResetLinkController;
use App\Http\Controllers\Admins\Auth\VerifyEmailController;
use Illuminate\Support\Facades\Route;

Route::middleware('guest:admin')->group(function () {

    Route::get('/kadmin-user/login', [AuthenticatedSessionController::class, 'create'])
        ->name('admin.login');

    Route::post('/kadmin-user/login', [AuthenticatedSessionController::class, 'store'])->name('admin.login');

    Route::get('kadmin-user/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->name('admin.password.request');

    Route::post('kadmin-user/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->name('admin.password.email');

    Route::get('kadmin-user/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->name('admin.password.reset');

    Route::post('kadmin-user/reset-password', [NewPasswordController::class, 'store'])
        ->name('admin.password.store');
});

Route::middleware('auth:admin')->group(function () {
    Route::get('kadmin-user/verify-email', EmailVerificationPromptController::class)
        ->name('admin.verification.notice');

    Route::get('kadmin-user/verify-email/{id}/{hash}', VerifyEmailController::class)
        ->middleware(['signed', 'throttle:6,1'])
        ->name('admin.verification.verify');

    Route::post('admin/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
        ->middleware('throttle:6,1')
        ->name('admin.verification.send');

    Route::get('kadmin-user/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->name('admin.password.confirm');

    Route::post('kadmin-user/confirm-password', [ConfirmablePasswordController::class, 'store']);

    Route::put('kadmin-user/password', [PasswordController::class, 'update'])->name('admin.password.update');

    Route::post('kadmin-user/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->name('admin.logout');
});
