<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lifecycles', function (Blueprint $table) {
            $table->id();
            $table->integer('age')->nullable();
            $table->integer('gender')->nullable();
            $table->integer('cp')->nullable();
            $table->integer('thalachh')->nullable();
            $table->integer('oldpeak')->nullable();
            $table->string('troponin')->default(0);
            $table->integer('thall')->nullable();
            $table->integer('caa')->nullable();
            $table->integer('pressurehight')->default(0);
            $table->integer('pressurelow')->default(0);
            $table->string('glucose')->default("0");
            $table->integer('ha_prediction')->nullable();
            $table->integer('weight')->default(0);
            $table->unsignedBigInteger('user_id')->nullable();
            $table->date('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lifecycles');
    }
};
//'age',	'gender',	'impluse',	'pressurehight',	'pressurelow',	'glucose',	'kcm',	'troponin',	'weight', 'user_id', 'date'