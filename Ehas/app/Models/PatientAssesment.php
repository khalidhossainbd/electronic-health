<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientAssesment extends Model
{
    use HasFactory;
    
    protected $table = 'patient_assesments';
    
    protected $primaryKey = 'id';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prescription_id', 'created_by', 'patient_id', 'doctor_id',  'appointment_id', 'status', 'general_problem_id', 'specific_problem_id', 'detail_observation', 'symptoms'
    ];
    
    
    
    
    
    
    
    
}
