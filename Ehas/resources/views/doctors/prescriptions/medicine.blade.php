<div class="row">
	<div class="col-12 col-sm-6">
		<form action="{{ route('doctor.visit', ['id' =>$id, 'tab'=>'medicine']) }}" method="post">
    	@csrf
    	<input type="hidden" name="method" value="handleMedicine" />
			<div class="form-group">
    		    <label for="">Specific Problem</label>
    		    <select id="medicine_time" class="form-control mfw-5 @error('medicine_time') is-invalid @enderror" name="medicine_time">
    		      {!! getSelectOptions(collect(json_decode(json_encode(array_map(function($item){return ['id'=>$item, 'medicine_time'=>$item." Time"];},range(1,5,1))))), 'medicine_time',   val('medicine_time', $request) ) !!}
    		    </select>
				 @error('medicine_time')
                 <span class="invalid-feedback" role="alert">
                  	<strong>{{ $message }}</strong>
                 </span>
                 @enderror
    		</div>
    		<div class="form-group">
    		    <label for="">Medicine</label>
    		    <select id="medicine_id" class="form-control mfw-5 @error('medicine_id') is-invalid @enderror" name="medicine_id">
    		      {!! getSelectOptions($medicines, 'org_name',   val('medicine_id', $request) ) !!}
    		    </select>
				 @error('medicine_id')
                 <span class="invalid-feedback" role="alert">
                  	<strong>{{ $message }}</strong>
                 </span>
                 @enderror
    		</div>
			<div class="form-group">
    			<button type="submit" class="btn btn-primary my-3 w-100">Submit</button>
    		</div>
		</form>
	</div>
	<div class="col-12 col-sm-6">
		<table id="meds-table" class="table table-condensed">
            <thead>
                <tr>
                    <th>SL.</th>
                    <th>Organic Name</th>
					<th>Medicine Time</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            	@if(isset($patientMedicines) && count($patientMedicines))
               	 	@foreach($patientMedicines as $item)
                	<tr>
                		<td>{{ $loop->iteration }}</td>
                		<td> {{ $item->medicine->org_name }}</td>
                		<td>  {{ $item->medicine_time }} time</td>
                		<td>
                			<p>
                    			<a class='btn btn-sm btn-danger mx-2' onclick="return confirm('Are you sure?')" href='{{ route('doctor.visit', ['id' =>$id, 'tab'=>'medicine','medicine_id' => $item->id, 'action' => 'delete']) }}'>Delete</a>
                			</p>
                		</td>
                	</tr>
                	@endforeach
            	@endif	
        	</tbody>
    	</table>
	</div>
</div>
