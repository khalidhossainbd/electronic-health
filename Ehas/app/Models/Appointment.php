<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    protected $table = 'appointments';

    /**
    * The database primary key value.
    *
    * @var string
    */
    
    protected $primaryKey = 'id';
    
    protected $fillable = [

        'appointmentID', 'date', 'start_time', 'end_time', 'duration', 'status', 'apdate_id', 'user_id', 'zoom_link'
    ];

    public function apdate()
    {
        return $this->belongsTo(Apdate::class);
    }
    
    public function order()
    {
        return $this->hasOne(Order::class, 'appointment_id', 'appointmentID');
    }
    
    public function doctor()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
   
}
