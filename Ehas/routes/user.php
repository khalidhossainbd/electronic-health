<?php


use App\Http\Controllers\Users\UserHomeController;
use App\Http\Controllers\AppointmentPaymentController;
use App\Http\Controllers\ProfileController;

Route::middleware(['auth', 'role:patient'])->group(function (){
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/get-appointment/{id}', [AppointmentPaymentController::class, 'get_appointment'])->name('appointment.get');
    Route::post('/get-appointment/{id}', [AppointmentPaymentController::class, 'post_appointment'])->name('appointment.post');
    Route::get('/order-confirmed/{orderid}', [AppointmentPaymentController::class, 'confirmation'])->name('appointment.confirmation');
    
});


Route::prefix('/users')->middleware('auth')->group(function(){

    Route::get('/dashboard', [UserHomeController::class ,'index'])->name('user.dashboard');
    Route::any('/profile', [UserHomeController::class ,'profile'])->name('user.profile');
    Route::any('/medical-history', [UserHomeController::class ,'medicalHistory'])->name('user.medical_history');
    Route::any('/health-prediction', [UserHomeController::class ,'healthPrediction'])->name('user.health_prediction');
    Route::get('/appointments_list', [UserHomeController::class ,'appointmentList'])->name('user.appointment.list');

});