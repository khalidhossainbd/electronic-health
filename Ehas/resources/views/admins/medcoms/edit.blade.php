@extends('layouts.back-app')

@section('content')

    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">{{ $module }}</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">{{ $module }}</a></li>
                            <li class="breadcrumb-item"><a href="#!">Update  {{ $module }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


	<div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                   <div class="d-flex">
                		<div class="col">
                			 <h5>Update {{$module}}</h5>
                		</div>
                		<div class="col text-right">
                			<a class="btn btn-sm btn-primary" href="{{ route('medcoms.index') }}"> <i class="feather icon-list"></i> {{$module}} List</a>
                		</div>
                	</div>
                </div>
                <div class="card-body">
                
                <div class="row">
                    <div class="col-12">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        
                        {{-- <li class="breadcrumb-item active">Dashboard</li> --}}
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        <br>
                        @endif
                    </div>
                </div>
                
                    <form action="{{ route('medcoms.update', $data->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
					@method('Patch')
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="floating-label" for="name">Name</label>
                                    <input type="text" name="name" class="form-control" id="name" aria-describedby="nameHelp" value="{{ $data->name }}">
                                </div>
                                
                                <div class="form-group">
                                   <label for="short_desc">Details</label>
                                   <textarea class="form-control" name="details" id="details"  rows="3">{!! $data->details !!}</textarea>
                               	</div>
                                
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input"  name="status" id="status" {{ $data->status == 1 ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="status">Status</label>
                                    </div>        
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                	<label for="status">Image</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" name="logo" class="custom-file-input" id="featureImage">
                                            <label class="custom-file-label" for="featureImage">Choose file</label>
                                        </div>
                                    </div>
                                    
									<div class="w-100 view-feature-image">
										<label for="featureImage">View Feature Image</label></br>
										{!! $data->getFeatureImage(200)  !!}
									</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-sm-12">
                        		<button class="btn  btn-primary w-100" type="submit">Update {{$module}}</button>
                        	</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('java_script')
	<script type="text/javascript">

        $(document).on('change keyup blur', '[name="name"]', function(e){
        	var slug = slugify($(this).val());
        	$('[name="slug"]').val(slug);
        });

		$(document).on('change', '#featureImage', function(e){
			e.preventDefault();
			var reader = new FileReader();
			var _this = this;
			reader.onload = function (e) {
				$('.custom-file-label').text(_this.files[0].name);
				$('#viewFeatureImage').attr('src', e.target.result);
			}
			reader.readAsDataURL(this.files[0]);
		});
        
		

	</script>
@endsection