@extends('layouts.back-app')

@section('content')

	<style>
        .pagination {
            justify-content: center !important;
        }    
    </style>
    
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">{{ $module }}</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">{{ $module }}</a></li>
                            <li class="breadcrumb-item"><a href="#!">List</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


	<div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                   <div class="d-flex">
                		<div class="col">
                			 <h5>{{$module}} List</h5> 
                		</div>
                		<div class="col text-right">
                			<a class="btn btn-sm btn-primary" href="{{ route('medcoms.create') }}"> <i class="feather icon-plus-square"></i> Add New</a>
                		</div>
                	</div>

                </div>
                <div class="card-body">
                
           			<div class="row">
                        <div class="col-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        
                            {{-- <li class="breadcrumb-item active">Dashboard</li> --}}
                            @if ($message = Session::get('success'))
                               <div class="alert alert-success alert-block">
                                  <button type="button" class="close" data-dismiss="alert">×</button>
                                  <strong>{{ $message }}</strong>
                               </div>
                               <br>
                            @endif
                        </div>
                    </div>   
                                 
                	<table id="medcoms-table" class="table table-condensed">
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Created At</th>
                            <th>Logo</th>
                            <th>Name</th>
                            <th>Details</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@if(isset($data) && count($data))
                       	 	@foreach($data as $item)
                        	<tr>
                        		<td>{{ $loop->iteration }}</td>
                        		<td>
                        			{{ $item->created_at->diffForHumans() }}
                        		</td>
                        		<td>{!! $item->getFeatureImage() !!}</td>
                        		<td>
                        			<p>{{ $item->name }} </p>
                        		</td>
                        		<td>
                        			<p>{!! $item->details !!} </p>
                        		</td>
                        		<td>
                        			<div class="form-group">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" {{ $item->status == 1 ? 'checked' : '' }} data-table="medcompanies" data-id="{{$item->id}}" name="status" id="status-{{$item->id}}">
                                            <label class="custom-control-label" for="status-{{$item->id}}"></label>
                                        </div>        
                                    </div>
                        		</td>
                        		<td>
                        			<p>
                            			<a class="btn btn-sm btn-primary" href="{{ route('medcoms.edit', $item->id) }}">Edit</a>
                            			<button type="button" class="btn btn-sm btn-danger" data-toggle="popover" data-html="true" data-placement="top" title="<b>Are You Sure?</b>" data-content="<a class='btn btn-sm btn-info cancel-pop mx-2' href='#'>No</a><a class='btn btn-sm btn-danger mx-2' href='{{ route('medcoms.delete', $item->id) }}'>Yes</a>">Delete</button>
                        			</p>
                        		</td>
                        	</tr>
                        	@endforeach
                    	@endif	
                    </tbody>
                	</table>
                    <div class="row justify-content-md-center">
                        <div class="col-6 col-sm-6">
        					{{ $data->appends(request()->query())->links() }}
                        </div>
                    </div>            
                
                </div>
            </div>
        </div>
    </div>
@endsection

@section('java_script')
	<script>
		$(function(){
			$(document).on('click', '.cancel-pop', function(e){
				e.preventDefault();
				$("[data-toggle='popover']").popover('hide');
			});
		});
	</script>
@endsection