@extends('layouts.back-app')

@section('content')

<div class="row">
        <!-- table card-1 start -->
    <div class="col-md-12 col-xl-12">.
    	<div class="card  p-4">
    		<h1>Welcome to EHASH</h1>
    	</div>
    </div>
</div>
    <!-- [ Main Content ] start -->
    <div class="row">
        <!-- table card-1 start -->
        <div class="col-md-12 col-xl-12">
            <div class="card flat-card">
                <div class="row-table">
                    <div class="col-sm-6 card-body br">
                        <div class="row">
                            <div class="col-sm-4">
                                <i class="icon feather icon-user text-c-green mb-1 d-block"></i>
                            </div>
                            <div class="col-sm-8 text-md-center">
                                <h5>{{$data->patient}}</h5>
                                <span>Patients</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <i class="icon feather icon-shield text-c-red mb-1 d-block"></i>
                            </div>
                            <div class="col-sm-8 text-md-center">
                                <h5>{{$data->doctor}}</h5>
                                <span>Doctors</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-table">
                    <div class="col-sm-6 card-body br">
                        <div class="row">
                            <div class="col-sm-4">
                                <i class="icon feather icon-file-text text-c-blue mb-1 d-block"></i>
                            </div>
                            <div class="col-sm-8 text-md-center">
                                <h5>{{$data->prescription}}</h5>
                                <span>Prescriptions</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <i class="icon feather icon-mail text-c-yellow mb-1 d-block"></i>
                            </div>
                            <div class="col-sm-8 text-md-center">
                                <h5>{{$data->appointment}}</h5>
                                <span>Appointments</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- widget primary card start --> 
            <div class="card flat-card widget-primary-card">
                <div class="row-table">
                    <div class="col-sm-3 card-body">
                        <i class="feather icon-star-on"></i>
                    </div>
                    <div class="col-sm-9">
                        <h4>{{$data->order}}</h4>
                        <h6>Earnings</h6>
                    </div>
                </div>
            </div>
            <!-- widget primary card end -->
        </div>
        <!-- table card-1 end -->
        <!-- table card-2 start -->
        
        <!-- table card-2 end -->
        <!-- Widget primary-success card start -->
        
    </div>
    <!-- [ Main Content ] end -->
@endsection
