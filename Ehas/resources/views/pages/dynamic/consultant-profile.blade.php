@extends('layouts.main')

@section('title')
    Electronic Health Appointment System
@endsection

@section('content')
	@include('pages.partials.pagetitle', ['title' => $doctor->f_name])
	
<section>
	<div class="my-5">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="jumbotron">
						<div class="row">
							<div class="col-12 col-sm-4">
								<img class="img-thumbnail rounded-circle d-block my-2 mx-auto" src="{{ $doctor->images == "dummy.png" ? asset('images/dummy.png') : asset('uploads/users/'.$doctor->images) }}" alt="Card image cap" style="max-height: 220px; max-width: 220px;">
							</div>
							<div class="col-12 col-sm-8">
								<h1 class="display-5">{{ $doctor->f_name }} {{ $doctor->l_name }}</h1>
								  <p class="lead">{{ @$doctor->designation }}</p>
								  <hr class="my-4">
								  <p>{!! @$doctor->docprofile->shot_bio !!}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-12 col-sm-4">
					<form action="" method="get">
    					<div class="form-group">
    				      <label for="inputState">Appointment Date</label>
    				      <select id="inputState" class="form-control" name="date">
    				        @foreach($appointment_data as $mydate)
    				        	
    				        	<option value="{{ $mydate->date }}">{{ $mydate->date }}</option>
    				        	
    				        @endforeach
    				      </select>
    				    </div>
    				    <div class="form-group">
    				    	<button type="submit" class="form-control btn btn-secondary">Search</button>
    				    </div>
				    </form>
				</div>
				<div class="col-12 col-sm-8">
					<table class="table table-bordered w-100">
						<thead>
							<tr>
								<th>Appointment Time</th><th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($appointments as $appointment)
								@if($appointment->status)
								<tr>
									<td>{{ $appointment->start_time }}</td><td><a href="{{ route('appointment.get', $appointment->appointmentID) }}" class="btn btn-primary">Confirm</a></td>
								</tr>
								@else
    				        	<tr>
									<td>{{ $appointment->start_time }}</td>
									<td>
										<a href="#" class="btn btn-primary disabled">Booked</a>
									</td>
								</tr>
    				        	@endif
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection