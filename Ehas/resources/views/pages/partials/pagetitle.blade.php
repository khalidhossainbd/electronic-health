<?php

if(isset($title))
{
    $mytitle = $title;
}else{
    $mytitle = "Page Title";
}

?>

<section>
    <div class="bg-light py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="page-title">{{ $mytitle }}</h3>
                </div>
            </div>
        </div>
    </div>  
</section>

