<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GeneralProblem;
use Redirect,Response;

class GeneralProblemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        view()->share('module', 'General Problem');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = GeneralProblem::query();
        $data = $data->paginate(5);
        $rank = $data->firstItem();
        return view('admins.health.general.index', compact('data','rank'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data = collect();
        return view('admins.health.general.form', ['data'=>$data]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
        ]);
        
        $data = new GeneralProblem();
        $data->title = $request->title;
        $data->content = $request->content;
        $data->status = $request->status == 'on' ? 1 : 0;
        
        if($data->save()){
            return Redirect::route('general.index')->withSuccess('Great! New General Problem added successfully.');
        }
        
        return Redirect::route('general.index')->withError('Sorry! New General Problem add Failed.');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = GeneralProblem::findOrFail($id);
        return view('admins.health.general.form', compact('data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
        ]);
        
        $data = GeneralProblem::findOrFail($id);
        
        $data->title = $request->title;
        $data->content = $request->content;
        $data->status = $request->status == 'on' ? 1 : 0;
        
        if($data->save()){
            return Redirect::route('general.index')->withSuccess('Great! New General Problem updated successfully.');
        }
        
        return Redirect::route('general.index')->withError('Sorry! New General Problem updated Failed.');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        GeneralProblem::destroy($id);
        return Redirect::route('general.index')->withSuccess('Great! General Problem deleted successfully.');
    }
    

}
