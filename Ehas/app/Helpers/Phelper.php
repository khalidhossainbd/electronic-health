<?php

namespace App\Helpers;

use App\Models\Appointment;
use App\Models\Order;
use App\Models\User;
class Phelper
{
	public static function getPatientName($id)
	{
		$app = Appointment::find($id);
		$order = Order::where('appointment_id', $app->appointmentID)->first();
		$patient = User::find($order->user_id);
		return $patient;
	}

	public static function getDoctorName($id)
	{
		$app = Appointment::where('appointmentID', $id)->first();
		$mydoc = User::find($app->user_id);
		return $mydoc;
	}

	public static function getAppDate($id)
	{
		$app = Appointment::where('appointmentID', $id)->first();
		return $app->date;
	}

	public static function getAppTime($id)
	{
		$app = Appointment::where('appointmentID', $id)->first();
		return $app->start_time;
	}

	public static function getLink($id)
	{
		$app = Appointment::where('appointmentID', $id)->first();
		return $app->zoom_link;
	}


}