						<div class="row">
							<div class="col-md-12">

                            <div class="table-data__tool">
                                <div class="table-data__tool-left">
                                	<strong>Medical History List</strong>  
                                </div>
                                <div class="table-data__tool-right">
                                    <a class="au-btn au-btn-icon au-btn--green au-btn--small" href="{{ route('user.medical_history', ['page'=>'create']) }}">Add Information</a>
                                </div>
                            </div>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            {{-- <th>Report</th> --}}
                                            <th>Date</th>
                                            <th>Title</th>                                              
                                            <th>Details</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@if(null !== ($prehistories) && isset($prehistories) && !empty($prehistories))
                                    	@foreach($prehistories as $item)
                                        <tr class="tr-shadow">
                                            {{-- <td> {!! $item->getFeatureImage(50) !!}</td> --}}
                                            <td>
                                               {{ $item->title }}
                                            </td>
                                            <td >{{ $item->date }}</td>
                                            <td class="desc">{{ $item->details }}</td>
                                            
                                            <td>
                                                <div class="table-data-feature">
                                                    <a href="{{ route( 'user.medical_history', ['page'=>'edit', 'id' => $item->id ]) }}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </a>
                                                    <a href="{{ route( 'user.medical_history', ['page'=>'delete', 'id' => $item->id ]) }}" onclick="return confirm('Are you sure?');" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>