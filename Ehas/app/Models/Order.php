<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    
    protected $table = 'orders';
    
    protected $primaryKey = 'id';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'orderID', 'name', 'email', 'mobile', 'amount', 'user_id', 'appointment_id', 'transaction_id', 'status'
    ];
    
    public function patient()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
