<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $helpers = glob(app_path().'/Http/Helpers/*.php');
        if(isset($helpers) && count($helpers)){
            foreach ($helpers as $filename){
                if(file_exists($filename)){
                    require_once($filename);
                }
            }
        }
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
