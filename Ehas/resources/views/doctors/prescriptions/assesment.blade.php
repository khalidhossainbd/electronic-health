<form action="{{ route('doctor.visit', ['id' =>$id, 'tab'=>'assesment']) }}" method="post">
@csrf
<input type="hidden" name="method" value="handleAssesment" />

    <div class="row">
    	<div class="col-12 col-sm-6">
    		<div class="form-group">
    		    <label for="">General Problem</label>
    		    <select id="general_problem_id" class="form-control mfw-5 @error('general_problem_id') is-invalid @enderror" name="general_problem_id">
    		      {!! getSelectOptions($general_problems, 'title',   val('general_problem_id', ($request->has('general_problem_id') ?  $request : $assesment  )  ) ) !!}
    		    </select>
				 @error('general_problem_id')
                 <span class="invalid-feedback" role="alert">
                  	<strong>{{ $message }}</strong>
                 </span>
                 @enderror
             </div>
    		
    		<div class="form-group">
    		    <label for="detail_observation">Detail Observation</label>
    		    <textarea id="detail_observation"  class="form-control mfw-5 @error('detail_observation') is-invalid @enderror" rows="3" name="detail_observation">{!! val('detail_observation', ($request->has('detail_observation') ?  $request : $assesment  )) !!}</textarea>
    		     @error('detail_observation')
                 <span class="invalid-feedback" role="alert">
                  	<strong>{{ $message }}</strong>
                 </span>
                 @enderror
    		</div>
    	</div>
    	<div class="col-12 col-sm-6">
    		<div class="form-group">
    		    <label for="">Specific Problem</label>
    		    <select id="specific_problem_id" class="form-control mfw-5 @error('specific_problem_id') is-invalid @enderror" name="specific_problem_id">
    		      {!! getSelectOptions($specific_problems, 'title',   val('specific_problem_id', ($request->has('specific_problem_id') ?  $request : $assesment  )) ) !!}
    		    </select>
				 @error('specific_problem_id')
                 <span class="invalid-feedback" role="alert">
                  	<strong>{{ $message }}</strong>
                 </span>
                 @enderror
    		</div>
    		<div class="form-group">
    		    <label for="symptoms">Symtom</label>
    		    <textarea id="symptoms" rows="3" class="form-control mfw-5 @error('symptoms') is-invalid @enderror" name="symptoms">{!! val('symptoms', ($request->has('symptoms') ?  $request : $assesment  )) !!}</textarea>
    		     @error('symptoms')
                 <span class="invalid-feedback" role="alert">
                  	<strong>{{ $message }}</strong>
                 </span>
                 @enderror
    		</div>
    	</div>
    	<div class="col-12">
    		<button type="submit" class="btn btn-primary my-3">Submit</button>
    	</div>
    </div>
</form>