<?php

use App\Http\Controllers\Supports\SupportHomeController;


Route::prefix('/supports')->middleware(['auth', 'role:support'])->group(function(){

    Route::get('/dashboard', [SupportHomeController::class ,'index'])->name('support.dashboard');

});
