<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EHAS | @yield('title')</title>
    <meta name='keywords' content='your, tags'>
    <meta name='description' content='150 words'>
    <meta name='subject' content='your website subject'>
    <meta name='copyright' content='company name'>
    <meta name='language' content='ES'>
    <meta name='robots' content='index,follow'>
    <meta name='revised' content='Sunday, July 18th, 2010, 5:15 pm'>
    <meta name='abstract' content=''>
    <meta name='topic' content=''>
    <meta name='summary' content=''>
    <meta name='Classification' content='Business'>
    <meta name='author' content='Md Khalid Hossain, infoarksit@gmail.com'>
    <meta name='og:title' content='The Rock'>
    <meta name='og:type' content='movie'>
    <meta name='og:url' content=''>
    <meta name='og:image' content=''>
    <meta name='og:site_name' content='IMDb'>
    <meta name='og:description' content=''>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="{{ asset('front/dist/images/icon.png') }}" type="image/gif" sizes="16x16">
    <link rel="stylesheet" href="{{ asset('front/node_modules/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/node_modules/@fortawesome/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/dist/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('front/dist/css/style.css') }}">
</head>

<body>


    @include('pages.partials.header')

    @yield('content')

    @include('pages.partials.footer')


    <script src="{{ asset('front/node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('front/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('front/node_modules/@fortawesome/fontawesome-free/js/all.min.js') }}"></script>

    <script src="{{ asset('front/dist/js/main.js') }}"></script>

    @yield('java_script')

</body>

</html>