<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('preprofiles', function (Blueprint $table) {
            $table->id();
            $table->integer('gender')->default('1')->nullable();
            $table->string('home_phone')->nullable();
            $table->date('dob')->nullable();
            $table->string('weight')->nullable();
            $table->text('disable_history')->nullable();
            $table->text('address')->nullable();
            $table->string('area')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('preprofiles');
    }
};
