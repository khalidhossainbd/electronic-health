@extends('layouts.user-app')

@section('title')
    Patient Profile
@endsection

@section('content')

	<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="overview-wrap">
                    <h2 class="title-1">{{ Auth::user()->f_name }} {{ Auth::user()->l_name }}</h2>
                </div>
            </div>
        </div>
        <div class="row m-t-25">
            <div class="col-12">

    			<div class="card">
    			<form action="{{ route('user.profile') }}" method="post" class="">
    				<div class="card-header">
    					<div class="row">
    						<div class="col-6">
    							<strong>Profile</strong> Update
    						</div>
    						<div class="col-6 text-right">
    							@if(Session::has('message')) <span class="text-success p-1">{{ Session::get('message') }}</span> @endif
    						</div>
    					</div>
    				</div>
    				<div class="card-body card-block">
    					
    						@csrf
    						<div class="row">
								<div class="col-6">
    								<div class="form-group">
            							<label for="home_phone" class=" form-control-label">Home Phone</label> 
            							<input type="text" id="home_phone" name="home_phone" placeholder="Home Phone" class="form-control" value="{{ val('home_phone', $userprofile) }}"> 
            						</div>
    							</div>
    							<div class="col-6">
    								<div class="form-group">
    									<label for="nf-email" class=" form-control-label">Gender</label>
            							<div class="form-check">
                                            <label for="inline-radio1" class="form-check-label mx-3 position-relative">
            	                                <input type="radio" id="inline-radio1"  name="gender" value="1" class="form-check-input" @if( val('gender', $userprofile) == 1 ) checked @endif>Male
        	                                </label>
                	                        <label for="inline-radio2" class="form-check-label mx-3 position-relative">
                    		                    <input type="radio" id="inline-radio2" name="gender" value="2" class="form-check-input" @if( val('gender', $userprofile) == 2 ) checked @endif>Female
                            	            </label>
                                        </div>
                                    </div>
                                </div>
    						</div>
    						<div class="row">
    							<div class="col-6">
    								<div class="form-group">
            							<label for="dob" class=" form-control-label">Date of Birth</label> 
            							<input type="date" id="dob" name="dob" placeholder="Date of Birth" class="form-control" value="{{ val('dob', $userprofile) }}"> 
            						</div>
    							</div>
    							<div class="col-6">
    								<div class="form-group">
            							<label for="area" class=" form-control-label">Area</label> 
            							<input type="text" id="area" name="area" placeholder="Area" class="form-control" value="{{ val('area', $userprofile) }}"> 
            						</div>
    							</div>
    						</div>
    						<div class="row">
    							<div class="col-6">
    								<div class="form-group">
            							<label for="state" class=" form-control-label">State</label> 
            							<input type="text" id="state" name="state" placeholder="State" class="form-control" value="{{ val('state', $userprofile) }}"> 
            						</div>
    							</div>
    							<div class="col-6">
    								<div class="form-group">
            							<label for="country" class=" form-control-label">Country</label> 
            							<select name="country" id="country" class="form-control">
                                            <option value="0">Please select</option>
                                            {!! getSelectOptions($countries, 'name', val('country', $userprofile)) !!}
                                        </select> 
            						</div>
    							</div>
    						</div>
    						<div class="row">
    							<div class="col-6">
    								<div class="form-group">
            							<label for="address" class=" form-control-label">Address</label> 
            							<textarea name="address" id="address" rows="3" placeholder="Adress..." class="form-control">{{ val('address', $userprofile) }}</textarea> 
            						</div>
    							</div>   
								<div class="col-6">
    								<div class="form-group">
            							<label for="disable_history" class=" form-control-label">Disable History</label> 
            							<textarea name="disable_history" id="disable_history" rows="3" placeholder="Content..." class="form-control">{{ val('disable_history', $userprofile) }}</textarea> 
            						</div>
    							</div>    						
    						</div>
    				</div>
    				<div class="card-footer text-center">
    					<div class="row">
    						<div class="col-4 mx-0 px-1">
								<button type="reset" class="btn btn-danger btn-sm  w-100">
    								<i class="fa fa-ban"></i> Reset
    							</button>
    						</div>
    						<div class="col-8 mx-0 px-1">
    							<button type="submit" class="btn btn-primary btn-sm w-100">
            						<i class="fa fa-dot-circle-o"></i> Submit
            					</button>
    						</div>
    					</div>
    				</div>
    				</form>
    			</div>
			</div>
		</div>
	</div>		




@endsection

@section('java_script')

<script>
$('textarea').each(function () {
    //const editor = Jodit.make(this);
});
</script>

@endsection