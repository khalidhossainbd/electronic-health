@extends('layouts.main')

@section('title')
    Electronic Health Appointment System
@endsection

@section('content')

	@include('pages.partials.pagetitle', ['title' => "EHAS Consultant List"])


 <section>
 	<div class="my-5">
 		<div class="container">
 			<div class="row">
 				@foreach($doctors as $item)
                    @if($item->docprofile)
     				<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <div class="doctor-slider-container">
                            {{-- <img class="img-fluid" src="{{ asset('front/dist/images/img/s62.jpg') }}"> --}}
                            <img src="{{ $item->images == "dummy.png" ? asset('images/dummy.png') : asset('uploads/users/'.$item->images) }}" alt="{{ $item->f_name }}" />
                        </div>
                        <div class="doctor-slider-text">
                            <h3 class="h4 text-center mt-5">{{ $item->f_name }} {{ $item->l_name }}</h3>
                            <p class="text-center">{{ $item->designation }}</p>
                            {{-- <p class="text-justify">{!! @$item->docprofile->shot_bio !!}</p> --}}
                        </div>
                        <a class="btn btn-success slider-book-btn" href="{{ route('consultant.profile', $item->userID) }}">VIEW PROFILE</a>
                    </div>
                    @endif
                @endforeach
 			</div>
 		</div>
 	</div>
 </section>

@endsection
