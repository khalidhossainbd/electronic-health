<?php

namespace App\Http\Controllers\Doctors;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AssistantController extends Controller
{
    public function index ()
    {
        return view('doctors.assistants.index');
    }

    public function create()
    {
        return view('doctors.assistants.create');
    }
}
