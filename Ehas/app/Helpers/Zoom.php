<?php

namespace App\Helpers;
use App\Library\Zoom_Api;


class Zoom
{
	public static function Meeting($mydate, $mytopic, $durations)
	{
		$zoom_meeting = new Zoom_Api();

		date_default_timezone_set('Asia/Dhaka'); 
        
        $date = $mydate;
        
        $data = array();
        $data['topic'] 		= $mytopic;
        $data['start_date'] = $date;
        $data['duration'] 	= 30;
        $data['type'] 		= 2;
        $data['password'] 	= "123456";

        try{
        	$response = $zoom_meeting->createMeeting($data);
        }catch (Exception $ex) {
            echo $ex;
        }

        return $response;
	}
}