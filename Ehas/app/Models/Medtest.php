<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medtest extends Model
{
    use HasFactory;

    protected $table = 'medtests';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

     /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'details', 'status', 'testcategory_id','reference_value'
    ];
    
    public function category()
    {
        return $this->belongsTo(Testcategory::class, 'testcategory_id');
    }
    public function scopeStatus($query, $id)
    {
        return $query->where('status', $id);
    }
    
}
