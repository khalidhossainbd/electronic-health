@extends('layouts.main')

@section('title')
    Contact Us
@endsection

@section('content')


@include('pages.partials.pagetitle', ['title' => "Contact Us"])

<section>
    <div class="my-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="my-4"><u>We are open for your sugession.</u></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="name">Full Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Your Name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" placeholder="Example: example@mail.com">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="mobile">Mobile Number</label>
                        <input type="text" class="form-control" id="mobile" placeholder="Mobile Number">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your mobile number with anyone else.</small>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="">Your Message</label>
                        <textarea class="form-control" id="" rows="9"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <button type="button" class="btn btn-primary btn-block">Submit</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="mt-5">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2429.778258234509!2d-1.885372923435763!3d52.48315047205157!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4870bc8328e543a5%3A0xa09a7e01e95fcbbf!2sCurzon%20Building!5e0!3m2!1sen!2suk!4v1704648689356!5m2!1sen!2suk" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
</section>


@endsection