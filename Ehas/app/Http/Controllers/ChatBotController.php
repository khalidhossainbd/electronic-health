<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect,Response;
use Auth;

class ChatBotController extends Controller
{
    public function getMessage(Request $request)
    {   
        $txt = $request->get('txt');
        $result = DB::table('chatbot_hints')->where('question', 'like', '%'.$txt.'%')->select('reply')->first();
        $html="Sorry not be able to understand you";
        if(null !== $result && $result->reply){
            $html= $result->reply;
        }
		$added_on=date('Y-m-d h:i:s');
		DB::table('message')->insert([['message'=>$txt, 'added_on' =>$added_on, 'type' => 'user' ],['message'=>$html, 'added_on' =>$added_on, 'type' => 'bot' ]]);
        return response()->json($html);
    }
}
