<?php

namespace App\Http\Controllers\Doctors;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Docprofile;
use App\Models\Appointment;
use App\Models\Apdate;

use App\Helpers\Zoom;

use DateTime;

use Auth;
use Session;
use Redirect,Response;


class AppointmentController extends Controller
{
    public function index ()
    {
        $data['appointments']= Appointment::where('user_id' , Auth::user()->id)->orderBy('id', 'desc')->paginate(10);
        return view('doctors.appointments.index', $data);
    }

    public function create()
    {
        return view('doctors.appointments.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'app_date' => ['required'],
            'str_time' => ['required'],
            'duration'=>['required']
        ]);

        $ap_date = new Apdate();

        $ap_date->date = $request->app_date;
        $ap_date->month = date("m",strtotime($request->app_date));
        $ap_date->year = date("Y",strtotime($request->app_date));
        $ap_date->user_id = Auth::user()->id ;

        $mydate = $ap_date->save();

        $a = new DateTime($request->str_time);

        $interval = $request->duration;

        if($mydate){           
            
            for($i =0 ; $i < $request->app_num; $i++){

                $inval = $interval * $i ;
                $inval1 = $interval * ($i+1) ;
                
                $dotime = date("h:i:s", strtotime("+".$inval." minutes", strtotime($request->str_time)));
                $todo = date("h:i:s", strtotime("+".$inval1." minutes", strtotime($request->str_time)));

                $myapp = new Appointment();
                $myapp->appointmentID = time().rand(1111, 9999);
                $myapp->date = $request->app_date;
                $myapp->start_time = $dotime;
                $myapp->end_time = $todo;
                $myapp->duration = $interval;
                $myapp->apdate_id = $ap_date->id;
                $myapp->zoom_link = "https://us04web.zoom.us/j/2679632453?pwd=UVQzZHlWVjlVWHBkWGRka1FKUVJBUT09";
                $myapp->user_id = Auth::user()->id ;

                // $test = Zoom::Meeting($request->app_date, Auth::user()->name, $interval);

                // dd($test);

                $myapp->save();
                
            }
        }

        return redirect()->route('doctor.appointment')->withSuccess('Great! Appointment schedule Add successfully.');
    }


}
