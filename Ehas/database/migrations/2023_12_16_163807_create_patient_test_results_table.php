<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('patient_test_results', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('prescription_id')->nullable();
            $table->unsignedBigInteger('patient_id')->nullable();
            $table->unsignedBigInteger('appointment_id')->nullable();
            $table->integer('status')->default('1')->nullable();
            $table->date('date')->nullable();
            $table->unsignedBigInteger('patient_med_test_id')->nullable();
            $table->text('result_value')->nullable();
            $table->text('result_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('patient_test_results');
    }
};
//'prescription_id', 'patient_id', 'appointment_id',  'status', 'date', 'patient_med_test_id', 'result_value', 'result_details'