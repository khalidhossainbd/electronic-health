<?php

namespace App\Http\Controllers\Doctors;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Docprofile;

use Auth;

class DoctorHomeController extends Controller
{
    public function index()
    {
        return view('doctors.dashboard');
    }

    public function docProfile()
    {
        return view('doctors.profiles.index');
    }

    public function docProfileCreate()
    {
        return view('doctors.profiles.create');
    }


    public function docProfileStore(Request $request)
    {
        // dd($request);
        $request->validate([
            'home_phone' => ['required', 'string', 'max:255'],
            'shot_bio' => ['required', 'string', 'max:255'],
        ]);

        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        Docprofile::create($data);

        return redirect()->route('doctor.profile')->withSuccess('Doctor Profile Information Add Successfully!');
    }

    public function docProfileEdit($id)
    {
        $editData = Docprofile::find($id);
        return view('doctors.profiles.create', compact('editData'));
    }

    public function docProfileUpdate(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            'home_phone' => ['required', 'string', 'max:255'],
            'shot_bio' => ['required', 'string', 'max:255'],
        ]);

        $result = Docprofile::find($id);

        $data = $request->all();

        $result->update($data);

        return redirect()->route('doctor.profile')->withSuccess('Doctor Profile Information update Successfully!');
    }


}
