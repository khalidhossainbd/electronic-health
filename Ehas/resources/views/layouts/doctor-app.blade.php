<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>EHAS | @yield('title')</title>
    <link rel="icon" href="{{ asset('front/dist/images/icon.png') }}" type="image/gif" sizes="16x16">

    <!-- Fontfaces CSS-->
    <link href="{{ asset('main/css/font-face.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('main/vendor/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('main/vendor/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('main/vendor/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('main/vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ asset('main/vendor/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('main/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('main/vendor/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('main/vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('main/vendor/slick/slick.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('main/vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('main/vendor/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('main/vendor/vector-map/jqvmap.min.css') }}" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{{ asset('main/jodit/jodit.min.css') }}"/>

    <!-- Main CSS-->
    <link href="{{ asset('main/css/theme.css') }}" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- MENU SIDEBAR-->
        @include('doctors.partials.menu_sidebar')
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container2">
            <!-- HEADER DESKTOP-->
            @include('doctors.partials.header_desktop')

            @include('doctors.partials.header_mobile')
            
            <!-- END HEADER DESKTOP-->

            @yield('content')

            @include('doctors.partials.footer')
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="{{ asset('main/vendor/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap JS-->
    <script src="{{ asset('main/vendor/bootstrap-4.1/popper.min.js') }}"></script>
    <script src="{{ asset('main/vendor/bootstrap-4.1/bootstrap.min.js') }}"></script>
    <!-- Vendor JS       -->
    <script src="{{ asset('main/vendor/slick/slick.min.js') }}"></script>
    <script src="{{ asset('main/vendor/wow/wow.min.js') }}"></script>
    <script src="{{ asset('main/vendor/animsition/animsition.min.js') }}"></script>
    <script src="{{ asset('main/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}">
    </script>
    <script src="{{ asset('main/vendor/counter-up/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('main/vendor/counter-up/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('main/vendor/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('main/vendor/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('main/vendor/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('main/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('main/vendor/vector-map/jquery.vmap.js') }}"></script>
    <script src="{{ asset('main/vendor/vector-map/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('main/vendor/vector-map/jquery.vmap.sampledata.js') }}"></script>
    <script src="{{ asset('main/vendor/vector-map/jquery.vmap.world.js') }}"></script>



    <!-- Main JS-->
    <script src="{{ asset('main/js/main.js') }}"></script>

    <script src="{{ asset('main/jodit/jodit.min.js') }}"></script>


    @yield('java_script')

</body>

</html>
<!-- end document-->
