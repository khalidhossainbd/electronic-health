<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Auth;
use App\Models\Country;
use App\Models\Lifecycle;
use App\Models\Order;
use App\Models\Prehistory;
use App\Models\Preprofile;
use Exception;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

class UserHomeController extends Controller
{
    public function index()
    {
        // if(Auth::user()->role == "doctor")
        // {
        //     return redirect()->route('doctor.dashboard');

        // }elseif(Auth::user()->role == "support")
        // {
        //     return redirect()->route('support.dashboard');

        // }else{
        //     return view('users.dashboard');
        // }

        return view('users.dashboard');
    }


    public function profile(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'home_phone' => ['required', 'string', 'max:255'],
                'gender' => ['required', 'string', 'max:255'],
            ]);
            $ifUpdated = Preprofile::updateOrCreate(['user_id' => $request->user()->id], $request->except(['_token']));
            if ($ifUpdated) {
                Session::flash('message', 'Profile Udated!');
            }
        }
        $userprofile = $request->user()->patprofile;
        $countries = Country::where('status', 1)->get();
        return view('users.profile', get_defined_vars());
    }

    public function handleAddEdit($request, $updateId)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'at_age' => ['numeric'],
            'report' => ['required', 'mimes:jpeg,jpg,bmp,png']
        ]);
        $report_file = $request->file('report');
        $report_link = "";
        if ($updateId) {
            $prehistory = Prehistory::find($updateId);
            $report_link = $prehistory->report;
        }
        if (!empty($report_file)) {
            $path = Prehistory::$uploadlocation;
            $report_link = uploadImage($report_file, $path, [800, 600], $report_link);
            $data = $request->merge(['user_id' => $request->user()->id])->except(['_token', 'report', 'page']);
            $data['report'] = $report_link;
        }
        return Prehistory::updateOrCreate(['id' => $updateId], $data);
    }

    public function medicalHistory(Request $request)
    {
        $prehistories = $request->user()->prehistories;
        if ($request->has('page') && in_array($request->get('page'), ['create', 'edit', 'delete'])) {
            $prehistory = collect();
            $updateId = null;
            if ($request->has('id')) {
                $updateId = $request->get('id');
            }
            if ($updateId && $request->get('page') == 'delete') {
                $isDeleted = Prehistory::find($updateId)->delete();
                if ($isDeleted) {
                    return redirect()->route('user.medical_history')->with('message', 'Patient History Deleted');
                }
            }

            if ($request->isMethod('post')) {
                $ifUpdated = $this->handleAddEdit($request, $updateId);
                if ($ifUpdated) {
                    $message = $updateId ? 'Patient History Updated' : 'Patient History created';
                    Session::flash('message', $message);
                    if (empty($updateId)) {
                        return redirect()->route('user.medical_history')->with('message', $message);
                    }
                }
            }
            if ($updateId) {
                $prehistory = Prehistory::find($updateId);
            }
            $page = view('users.medhistory.form', get_defined_vars())->render();
        } else {
            $page = view('users.medhistory.list', get_defined_vars())->render();
        }
        return view('users.medical-history', get_defined_vars());
    }

    public function healthPrediction(Request $request)
    {
        $lifecycles = $request->user()->lifecycles;
        if ($request->has('page') && in_array($request->get('page'), ['create', 'edit', 'delete'])) {
            $lifecycle = collect();
            $updateId = null;
            if ($request->has('id')) {
                $updateId = $request->get('id');
            }
            if ($updateId && $request->get('page') == 'delete') {
                $isDeleted = Lifecycle::find($updateId)->delete();
                if ($isDeleted) {
                    return redirect()->route('user.health_prediction')->with('message', 'Lifecycle Deleted');
                }
            }

            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'age' => ['required', 'numeric', 'max:255'],
                    'gender' => ['required', 'numeric', 'max:255'],
                    'cp' => ['numeric', 'between:0,3'],
                    'thalachh' => ['numeric', 'between:0,320'],
                    'oldpeak' => ['numeric', 'between:0,10'],
                    'pressurehight' => ['numeric', 'max:255'],
                    'pressurelow' => ['numeric', 'max:255'],
                    'glucose' => ['numeric', 'between:0,99.99'],
                    'troponin' => ['numeric', 'between:0,99.99'],
                    'thall' => ['numeric', 'between:0,3'],
                    'caa' => ['numeric', 'between:0,4'],
                ]);

                $data = $request->merge(['user_id' => $request->user()->id])->except(['_token', 'page']);

                try {
                    $response = $this->getPrediction(
                        $request->age,
                        $request->gender,
                        $request->cp,
                        $request->oldpeak,
                        $request->thalachh,
                        $request->thall,
                        $request->caa
                    );
                    $data['ha_prediction'] = json_encode($response);
                } catch (Exception $e) {
                    return $e->getMessage();
                    Log::error('ERROR_PREDICT:: ', json_encode($e->getMessage()));
                }


                $ifUpdated = Lifecycle::updateOrCreate(['id' => $updateId], $data);

                if ($ifUpdated) {
                    $message = $updateId ? 'Lifecycle Updated' : 'Lifecycle created';
                    Session::flash('message', $message);
                    if (empty($updateId)) {
                        return redirect()->route('user.health_prediction')->with('message', $message);
                    }
                }
            }
            if ($updateId) {
                $lifecycle = Lifecycle::find($updateId);
            }
            $page = view('users.health-prediction.form', get_defined_vars())->render();
        } else {
            $page = view('users.health-prediction.list', get_defined_vars())->render();
        }
        return view('users.health-prediction', get_defined_vars());
    }

    public function appointmentList(Request $request)
    {
        $data['myappointment'] = Order::where('user_id', Auth::user()->id)->get();
        return view('users.appointments.appointment_list', $data);
    }

    public function getPrediction($age, $sex, $cp, $oldpeak, $thalachh, $thall, $caa)
    {
        $client = new Client();
        $data = [
            "age" =>  $age,
            "sex" => $sex,
            "cp" => $cp,
            "trtbps" => 120,
            "chol" => 157,
            "fbs" => 0,
            "restecg" => 0,
            "thalachh" => $thalachh,
            "exng" => 0,
            "oldpeak" => $oldpeak,
            "slp" => 2,
            "caa" => $caa,
            "thall" => $thall
        ];
        
        Log::info('HA_PREDICTION_REQUEST:: ' . json_encode($data));

        $response = $client->post(config('app.ha_api_base') . '/api/predict', [
            RequestOptions::JSON => $data
        ]);

        $result = $response->getBody()->getContents();
        Log::info('HA_PREDICTION_RESPONSE:: ' . $result);

        $response = json_decode($result, true);
        return $response;
    }
}
