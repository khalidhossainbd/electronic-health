<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Docprofile extends Model
{
    use HasFactory;

    protected $table = 'docprofiles';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

        /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
       'gender', 'home_phone', 'nid', 'shot_bio', 'address', 'area', 'state', 'country', 'user_id' , 'social_media', 'time_zone', 'video_url', 'price'
    ];
    

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
