@extends('layouts.doctor-app')

@section('title')
    Doctor Dashboard
@endsection

@section('content')

    <!-- BREADCRUMB-->
    <section class="au-breadcrumb m-t-75">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="au-breadcrumb-content">
                            <div class="au-breadcrumb-left">
                                <h4 class="name">Doctor's Active Appointment List </h4>
                            </div>
                            <a href="{{ route('doctor.appointment') }}" class="au-btn au-btn-icon au-btn--green">
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
	    <section class="statistic">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card p-5">
							<form action="{{ route('doctor.active-appointment') }}" method="get">
								<div class="row">
									<div class="col-md-4 col-12">
										<div class="form-group">
										  <label for="inputState">Appointment Date</label>
										  <select id="inputState" class="form-control" name="date">
											@if(null !== $appointment_data)
												@foreach($appointment_data as $mydate)
													<option value="{{ $mydate->date }}" @if( $mydate->date == $request->date) selected @endif>{{ $mydate->date }}</option>
												@endforeach
											@endif
										  </select>
										</div>
									</div>
									<div class="col-md-4 col-12">
										<div class="form-group" style="margin-top: 33px;">
											<button type="submit" class="form-control btn btn-secondary">Search</button>
										</div>
									</div>
									<div class="col-md-4 col-12">
										
									</div>
								</div>
							</form>
							<div class="row">
								<div class="col-12">
									@if(null !== $appointments)
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>Patient Name</th>
													<th>Start Time</th>
													<th>Duration</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($appointments as $item)
												<tr>
													<td>
														{{ App\Helpers\Phelper::getPatientName($item->id)->f_name }}
														{{ App\Helpers\Phelper::getPatientName($item->id)->l_name }}
													</td>
													<td>{{ $item->start_time }}</td>
													<td>{{ $item->duration }} Minutes</td>
													<td>
														<a class="au-btn au-btn-icon au-btn--green" href="{{ route('doctor.visit', ['id'=>$item->id, 'tab'=>'patient']) }}">Make Visit</a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									@endif
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

	

@endsection