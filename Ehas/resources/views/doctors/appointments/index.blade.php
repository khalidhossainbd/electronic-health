@extends('layouts.doctor-app')

@section('title')
    Doctor Dashboard
@endsection

@section('content')

    <!-- BREADCRUMB-->
    <section class="au-breadcrumb m-t-75">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="au-breadcrumb-content">
                            <div class="au-breadcrumb-left">
                                <h4 class="name">Doctor's Appointmet List </h4>
                            </div>
                            <a href="{{ route('doctor.appointment.create') }}" class="au-btn au-btn-icon au-btn--green">
                                Create New
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->


    <section class="statistic">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card p-5">
                            <div class="col-md-12">
                        <div class="table-responsive m-b-40">
                            <table class="table table-borderless table-data3">
                                <thead>
                                    <tr>
                                        <th>Appointment ID</th>
                                        <th>Date</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>Duration</th>
                                        <th>status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($appointments)>0)
                                    @foreach($appointments as $item)
                                        <tr>
                                            <td>{{ $item->appointmentID }}</td>
                                            <th>{{ date("d-m-Y", strtotime($item->date))  }}</th>
                                            <td>{{ $item->start_time }}</td>
                                            <td>{{ $item->end_time }}</td>
                                            <td>{{ $item->duration }}</td>
                                            <td>{{ $item->status == 1 ? 'Active' : ''}}</td>
                                            <td>
                                                <a class="btn btn-sm btn-primary" href="">Edit</a>
                                            </td>
                                        </tr>                                    
                                    @endforeach
                                    @else

                                    @endif
                                    
                                </tbody>
                            </table>
                        </div> 
                    </div>
                    <div class="col-12">
                        <div class="d-flex">
                            <div class="mx-auto mb-4">
                                {{$appointments->links("pagination::bootstrap-4")}}
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
