<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientMedTest extends Model
{
    use HasFactory;
    
    protected $table = 'patient_med_tests';
    
    protected $primaryKey = 'id';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prescription_id', 'testcategory_id', 'med_test_id', 'patient_id', 'appointment_id', 'status'
    ];

}
