@extends('layouts.doctor-app')

@section('title')
    Doctor Dashboard
@endsection

@section('content')

    <!-- BREADCRUMB-->
    <section class="au-breadcrumb m-t-75">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="au-breadcrumb-content">
                            <div class="au-breadcrumb-left">
                                <h4 class="name">Doctor's Profile Information Add </h4>
                            </div>
                            <a href="{{ route('doctor.profile') }}" class="au-btn au-btn-icon au-btn--green">
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->


    <section class="statistic">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ @$editData ? route('doctor.profile.update', @$editData->id) : route('doctor.profile.store') }}" method="post">
                            @csrf
                            @if(@$editData) @method('patch') @endif
                        <div class="card p-5">
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="gender">Gender</label>
                                        <select class="form-control" id="gender" name="gender">
                                          <option value="" selected>=== Select an Option ===</option>
                                          <option value="1" {{ @$editData->gender == 1 ? 'selected' : '' }}>Male</option>
                                          <option value="2" {{ @$editData->gender == 2 ? 'selected' : '' }}>Female</option>
                                          <option value="3" {{ @$editData->gender == 3 ? 'selected' : '' }}>Other's</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="home_phone">Home Phone Number</label>
                                        <input type="text" name="home_phone" class="form-control @error('home_phone') is-invalid @enderror" id="home_phone" placeholder="Phone number" value="{{ !empty($editData)? @$editData->home_phone : old('home_phone') }}">
                                        @error('home_phone')
                                         <div class="invalid-feedback">
                                            {{ $message }}
                                         </div>
                                        @enderror 
                                    </div>
                                    <div class="form-group">
                                        <label for="shot_bio">Short Bio</label>
                                        <textarea class="form-control @error('shot_bio') is-invalid @enderror" id="short_desc" name="shot_bio" rows="5">{{ !empty($editData)? @$editData->shot_bio : old('shot_bio') }}</textarea>
                                        @error('shot_bio')
                                         <div class="invalid-feedback">
                                            {{ $message }}
                                         </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="price">Price</label>
                                        <input type="text" name="price" class="form-control @error('price') is-invalid @enderror" id="price"  value="{{ !empty($editData)? @$editData->price : old('price') }}">
                                        @error('price')
                                         <div class="invalid-feedback">
                                            {{ $message }}
                                         </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="nid">National Insurance Number</label>
                                        <input type="text" name="nid" class="form-control @error('nid') is-invalid @enderror" id="nid" placeholder="Example: E125834A" value="{{ !empty($editData)? @$editData->nid : old('nid') }}">
                                        @error('nid')
                                         <div class="invalid-feedback">
                                            {{ $message }}
                                         </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="social_media">Social Media Link</label>
                                        <input type="text" name="social_media" class="form-control @error('social_media') is-invalid @enderror" id="social_media" placeholder="Example: www.facebook.com/username" value="{{ !empty($editData)? @$editData->social_media : old('social_media') }}">
                                        @error('social_media')
                                         <div class="invalid-feedback">
                                            {{ $message }}
                                         </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Street Address</label>
                                        <input type="text" name="address" class="form-control @error('address') is-invalid @enderror" id="address" placeholder="Home Address" value="{{ !empty($editData)? @$editData->address : old('address') }}">
                                        @error('address')
                                         <div class="invalid-feedback">
                                            {{ $message }}
                                         </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="area">Area</label>
                                        <input type="text" name="area" class="form-control @error('area') is-invalid @enderror" id="area" placeholder="Living Area" value="{{ !empty($editData)? @$editData->area : old('area') }}">
                                        @error('area')
                                         <div class="invalid-feedback">
                                            {{ $message }}
                                         </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <input type="text" name="state" class="form-control @error('state') is-invalid @enderror" id="state" placeholder="State Name" value="{{ !empty($editData)? @$editData->state : old('state') }}">
                                        @error('state')
                                         <div class="invalid-feedback">
                                            {{ $message }}
                                         </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>  
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection


@section('java_script')
<script>
    const editor = Jodit.make('#short_desc');
</script>
@endsection
