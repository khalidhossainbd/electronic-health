-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 05, 2024 at 05:12 PM
-- Server version: 8.0.35-0ubuntu0.22.04.1
-- PHP Version: 8.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digital_health`
--

-- --------------------------------------------------------

--
-- Table structure for table `lifecycles`
--

CREATE TABLE `lifecycles` (
  `id` bigint UNSIGNED NOT NULL,
  `age` int DEFAULT NULL,
  `gender` int DEFAULT NULL,
  `cp` varchar(222) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `pressurehight` int NOT NULL DEFAULT '0',
  `pressurelow` int NOT NULL DEFAULT '0',
  `glucose` varchar(33) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.00',
  `caa` varchar(33) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thall` varchar(33) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oldpeak` varchar(33) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thalachh` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.00',
  `troponin` varchar(22) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.00',
  `weight` varchar(22) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.00',
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `date` date DEFAULT NULL,
  `ha_prediction` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lifecycles`
--

INSERT INTO `lifecycles` (`id`, `age`, `gender`, `cp`, `pressurehight`, `pressurelow`, `glucose`, `caa`, `thall`, `oldpeak`, `thalachh`, `troponin`, `weight`, `user_id`, `date`, `ha_prediction`, `created_at`, `updated_at`) VALUES
(1, 67, 1, '2', 33, 3, '5.66', '', '', '', '23', '12', '4', 1, '2022-06-15', NULL, '2024-01-05 16:05:16', '2024-01-05 16:05:16'),
(2, 67, 1, '1', 3, 4, '3', '2', '2', '2', '182', '4', '50', 1, '2024-01-26', NULL, '2024-01-05 16:51:54', '2024-01-05 16:51:54'),
(3, 67, 1, '1', 33, 3, '3', '2', '2', '2', '182', '12', '50', 1, '2024-01-19', '[[0.4191106671106671,0.5808893328893329]]', '2024-01-05 17:03:10', '2024-01-05 17:03:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lifecycles`
--
ALTER TABLE `lifecycles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lifecycles`
--
ALTER TABLE `lifecycles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
