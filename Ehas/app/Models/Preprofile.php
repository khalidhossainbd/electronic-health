<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Preprofile extends Model
{
    use HasFactory;

    protected $table = 'preprofiles';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

        /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
       'gender', 'home_phone', 'dob', 'weight', 'disable_history', 'address', 'area', 'state', 'country', 'user_id'
    ];
    

    public function user()
    {
        return $this->hasOne(User::class);
    }
    
    
}
