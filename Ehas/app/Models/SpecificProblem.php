<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecificProblem extends Model
{
    use HasFactory;
    
    protected $table = 'specific_problems';
    
    protected $fillable = ['title', 'content', 'status', 'general_problem_id'];

    public function general()
    {
        return $this->belongsTo(GeneralProblem::class, 'general_problem_id');
    }
}
