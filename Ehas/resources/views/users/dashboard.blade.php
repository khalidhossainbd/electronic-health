@extends('layouts.user-app')

@section('title')
    User dashboard
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="overview-wrap">
                    <h2 class="title-1">Welcome To EHAS {{ Auth::user()->f_name }} {{ Auth::user()->l_name }}</h2>
                    <a href="{{ route('consultants') }}" class="au-btn au-btn-icon au-btn--blue">
                        Get Appointment</a>
                </div>
            </div>
        </div>
        <div class="row m-t-25">
            <div class="col-12">
                <div class="card p-4">
                    <p> 
                        Human Health Prediction based on Electric Health Appointment System (EHAS) by web application.
                    </p>
                    <p>
                        To Get Health prediction Please update your profile.
                    </p>
                    <div class="overview-wrap mt-5">
                    <a href="{{ route('user.profile') }}" class="au-btn au-btn-icon au-btn--blue">
                        Update Profile</a>
                    </div>
                </div>
            </div>


        </div> 

        <div class="row">
            <div class="col-md-12">
                <div class="copyright">
                    <p>Copyright © EHAS. All rights reserved. Develop by <a href="">Md Khalid Hossain</a>.</p>
                </div>
            </div>
        </div>
    </div>

@endsection
