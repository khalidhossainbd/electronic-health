<?php


use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admins\AdminController;
use App\Http\Controllers\Admins\CategoryController;
use App\Http\Controllers\Admins\GeneralProblemController;
use App\Http\Controllers\Admins\MedcomController;
use App\Http\Controllers\Admins\MedestController;
use App\Http\Controllers\Admins\SpecificProblemController;
use App\Http\Controllers\Users\UserHomeController;

use App\Http\Controllers\MainController;
use App\Http\Controllers\ChatBotController;
use App\Models\Medvariant;
use App\Http\Controllers\Admins\MedvarController;
use App\Http\Controllers\Admins\MedController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/dashboard', function () {
//     return view('users.dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');


Route::get('/', [MainController::class, 'index'])->name('home');
Route::get('/contact-us', [MainController::class, 'contact'])->name('contact');
Route::get('/services', [MainController::class, 'service'])->name('service');
Route::get('/about-us', [MainController::class, 'about'])->name('about');
Route::get('/consultants', [MainController::class, 'consultants'])->name('consultants');
Route::get('/consultant/{id}', [MainController::class, 'consultant_profile'])->name('consultant.profile');
Route::get('/ehas-live-chat', [MainController::class, 'liveChat'])->name('live.chat');

Route::get('/privacy-policy', [MainController::class, 'privary_policy'])->name('privacy_policy');
Route::get('/terms-of-use', [MainController::class, 'terms_of_use'])->name('terms_of_use');
Route::get('/support', [MainController::class, 'support'])->name('support');

Route::post('/get-chat-message', [ChatBotController::class, 'getMessage'])->name('chat.message');



require __DIR__.'/auth.php';

require __DIR__.'/user.php';

require __DIR__.'/doctor.php';

require __DIR__.'/support.php';


// Admin user Route List
Route::group(['middleware' => ['auth:admin', 'verified'], 'prefix' => 'kadmin-user'], function () {
    Route::get('/dashboard', [AdminController::class ,'index'])->name('admin.dashboard');
    
    
    Route::get('/admins', [AdminController::class ,'admin_list'])->name('admin.admin_list');
    Route::get('/admins-create', [AdminController::class ,'admin_create'])->name('admin.admin_create');
    
    Route::get('/doctors', [AdminController::class ,'doctor_list'])->name('admin.doctor_list');
    Route::get('/patients', [AdminController::class ,'patient_list'])->name('admin.patient_list');
    Route::get('/supports', [AdminController::class ,'support_list'])->name('admin.support_list');
    
    Route::get('/appointments', [AdminController::class ,'appointments'])->name('admin.appointments');
    Route::get('/appointments/{id}', [AdminController::class ,'appointments_list'])->name('admin.appointments_list');
    Route::get('/appointments-sold', [AdminController::class ,'appointment_sold'])->name('admin.appointments_sold');
    Route::get('/earnings', [AdminController::class ,'earnings'])->name('admin.earnings');
    
    Route::get('/prescriptions', [AdminController::class ,'prescriptions'])->name('admin.prescriptions');
    
    
    Route::any('/categories-delete/{id}', [CategoryController::class ,'delete'])->name('categories.delete');
    Route::any('/medtests-delete/{id}', [MedestController::class ,'delete'])->name('medtests.delete');
    Route::any('/medcoms-delete/{id}', [MedcomController::class ,'delete'])->name('medcoms.delete');
    Route::any('/medvars-delete/{id}', [MedvarController::class ,'delete'])->name('medvars.delete');
    Route::any('/meds-delete/{id}', [MedController::class ,'delete'])->name('meds.delete');
    
    Route::any('/general-delete/{id}', [GeneralProblemController::class ,'delete'])->name('general.delete');
    Route::any('/specific-delete/{id}', [SpecificProblemController::class ,'delete'])->name('specific.delete');
    
    Route::resource('/categories', CategoryController::class);
    Route::resource('/medtests', MedestController::class);
    Route::resource('/medcoms', MedcomController::class);
    Route::resource('/medvars', MedvarController::class);
    Route::resource('/meds', MedController::class);
    
    Route::resource('/general', GeneralProblemController::class);
    Route::resource('/specific', SpecificProblemController::class);
    
    Route::any('/status-change', [AdminController::class ,'statusChange'])->name('status.change');
    
    
});


require __DIR__.'/adminauth.php';

