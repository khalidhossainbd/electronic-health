<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, $role): Response
    {
        if($request->user()->role !== $role)
        {
            // abort(404);
            if($request->user()->role == "doctor")
            {
                return redirect()->route('doctor.dashboard');
            }

            if($request->user()->role == "support")
            {
                return redirect()->route('support.dashboard');
            }
        }

        return $next($request);
    }
}
